Game Name: DisColoured
Team Name: The Resistance
Class-Section Year: GAM100f19-A 2019

Team Members:
David De Souza (d.desouza@digipen.edu) - BSGD
Jolyn Wong Kaiyi (wong.k@digipen.edu) - BSGD
Srikesh Sundaresan (srikesh.s@digipen.edu) - BSGD
Seow Jun Hao Darren (seow.j@digipen.edu) - RTIS

Game Brief:
Inspired by Splatoon.
Cover as much of the game map as possible with the colour of the team
you are on.
Winner is the team that covered a higher percentage of the map.


How to play:

** 1st Player **
'WASD' - move player
Arrow Keys - shoot in the direction of the arrow keys
Space - cast ultimate


** Multiplayer **
Left Joystick - move player
Right Joystick - determine direction of shot ( up, down, left, right )
L2,R2 ( Triggers ) - shoot
A/X Button - cast ultimate


