/*========================================================================
* \file			UltimateAbility.h
* \brief		Handles the Ultimate ability 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef ULTIMATEABILITY_H
#define ULTIMATEABILITY_H
#include "../Header_Files/Globals.h"
#include "../Libraries/GameTime.h"
#include "../Header_Files/Team.h"

//========================================================================
//	author: David De Souza
//	Adds to the team's ultimateCharge value
//	Sets the bUltimateCharged flag to 1 if fully charged (100%)
//========================================================================
void ChargeUltimate(Team *_team);

//========================================================================
//	author: David De Souza
//	Renders the ultimate charge bar to the UI for team A
//========================================================================
void RenderUlitmateBarTeamA(Team *_team);

//========================================================================
//	author: David De Souza
//	Renders the ultimate charge bar to the UI for team B 
//========================================================================
void RenderUlitmateBarTeamB(Team *_team);

//========================================================================
//	author: David De Souza
//	handles ultimate explosion when used by player 
//========================================================================
void UseUltimate(Team *_team, PlayerComponent *_player);

//========================================================================
//	author: David De Souza
//	Renders the ultimate explosion to the console 
//========================================================================
void RenderUltimateEffects(Team *_team, PlayerComponent *_player);

//========================================================================
//	author: David De Souza
//	checks what team the player is on when the RenderUltimateEffects 
//  function is called 
//========================================================================
void RenderUltimateBasedOnTeam();

//========================================================================
//	author: David De Souza
//	handles ultimate explosion when used by AI 
//========================================================================
void AIUseUltimate(Team *_team, AIComponent *_AI);

//========================================================================
//	author: David De Souza
//	sets all enemies within the ultimate blastradius to dead when 
//  ultimate is used by player
//========================================================================
void KillEverythingInRadius_Player(PlayerComponent *_player);

//========================================================================
//	author: David De Souza
//	sets all enemies within the ultimate blastradius to dead when 
//  ultimate is used by AI
//========================================================================
void KillEverythingInRadius_AI(AIComponent *_AI);
#endif // !ULTIMATEABILITY_H