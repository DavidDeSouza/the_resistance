/*========================================================================
* \file			PlayerControlsOptions.h
* \brief		Handles the PlayerControlsOptions state
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef PLAYERCONTROLOPTIONS_H
#define PLAYERCONTROLOPTIONS_H
#include "../Header_Files/Globals.h"

//========================================================================
//	public variables
//========================================================================
enum Side
{
	A,//left - Team A
	B,//right - Team B
};

int side[MAXPLAYER];
int ForeGroundColor[MAXPLAYER];
int BackGroundColor[MAXPLAYER];
char player1String[50];
char player2String[50];
char player3String[50];
char player4String[50];

//========================================================================
//	author: David De Souza
//  handles input events
//========================================================================
void Controls_ProcessInput();

//========================================================================
//	author: David De Souza
//  the update loop of the state
//========================================================================
void Controls_Update();

//========================================================================
//	author: David De Souza
//  handles rendeering in the state
//========================================================================
void Controls_Render();

//========================================================================
//	author: David De Souza
//  runs on entry into state
//========================================================================
void Controls_EnterState();

//========================================================================
//	author: David De Souza
//  runs on exiting the state
//========================================================================
void Controls_ExitState();

//========================================================================
//	Handles Rendering of the player's options screen 
//	Handles Rendering of the player's ready up status 
//========================================================================
void RenderSelections();

//========================================================================
//	Handles Rendering of the player's input control type 
//========================================================================
void RenderPlayerTypes();

#endif