/*========================================================================
* \file			Globals.h
* \brief		holds all global variables 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef GLOBALS_H
#define GLOBALS_H

#define WINDOWSIZEX 81				// using odd numbers so there is a proper midpoint
#define WINDOWSIZEY 51				// using odd numbers so there is a proper midpoint
#define MAPSTARTPOSX 0				/*define where the start of the map in the x axis*/
#define MAPSTARTPOSY 3				/*define where the start of the map in the y axis*/
#define MAPSIZEX WINDOWSIZEX		/*define how big is the map in the x axis*/
#define MAPSIZEY WINDOWSIZEY		/*define how big is the map in the y axis*/ 
#define NUMBEROFWALL 6				/* number of walls*/
#define SPAWNPOINT_OFFSET 3			// offset from side to set spawn points

//========================================================================
//	colours 
//========================================================================
#define FOREGROUND_BLACK	 0x0001 // text color contains black
#define FOREGROUND_WHITE_	 0x0002 // text color contains white.
#define FOREGROUND_ORANGE    0x0004 // text color contains orange.
#define FOREGROUND_BLUE_      0x0005 // text color contains blue.
#define FOREGROUND_GREEN_     0x0006 // text color contains green.
#define FOREGROUND_RED_	 0x0007 // text color contains red.
#define FOREGROUND_TEAL		 0x0008 // text color contains teal.
#define FOREGROUND_PURPLE    0x0009 // text color contains purple
#define FOREGROUND_LIGHTORANGE 0x000A // text color contains light orange
#define FOREGROUND_LIGHTTEAL   0x000B // text color contains light teal

#define BACKGROUND_BLACK	 0x0010 // background color contains black
#define BACKGROUND_WHITE_	 0x0020 // background color contains white
#define BACKGROUND_GREY      0x0030 // background color contains grey.
#define BACKGROUND_ORANGE    0x0040 // background color contains orange.
#define BACKGROUND_BLUE_      0x0050 // background color contains blue.
#define BACKGROUND_GREEN_     0x0060 // background color contains green.
#define BACKGROUND_RED_    0x0070 // background color contains red.
#define BACKGROUND_TEAL		 0x0080 // background color contains teal
#define BACKGROUND_PURPLE    0x0090 // background color contains purple
#define BACKGROUND_LIGHTORANGE 0x00A0 // background color contains light orange
#define BACKGROUND_LIGHTTEAL   0x00B0 // background color contains light teal

//========================================================================
//	controller  
//========================================================================
#define DEADZONE 26000
#define SPEED 0.028f

#define SIZE_X 3					// size of a normal sprite
#define SIZE_Y 3					// size of a normal sprite

//========================================================================
//	shooting and ultimate  
//========================================================================
#define SHOT_RANGE_MIN 22
#define SHOT_RANGE_MAX 30
#define SHOT_RANGE_SPREAD 1
#define ULTIMATE_BLASTRADIUS 30
#define ULTIMATE_CHARGE_RATE 10

//========================================================================
//	keyboard movement  
//========================================================================
#define VK_W 0x57
#define VK_A 0x41
#define VK_S 0x53
#define VK_D 0x44
#define VK_1 0x31

//========================================================================
//	AI and team variables 
//========================================================================
#define MAXNUMBEROFAI 10
#define MAXNUMBERINTEAM 4
#define MAXPLAYER 4

#include "Wall.h"
#include "Map.h"
#include "Player.h"
#include "AI.h"
#include "Team.h"

//========================================================================
//	public variables 
//========================================================================
extern Map map[MAPSIZEX][MAPSIZEY];
extern Wall wall[NUMBEROFWALL];
extern int bGameIsRunning;
extern PlayerComponent player1;
extern PlayerComponent player2;
extern PlayerComponent player3;
extern PlayerComponent player4;
extern int numberOfControllers;

extern AIComponent AI[MAXNUMBEROFAI];
extern Team teamA;
extern Team teamB;

extern char stringBuffer[100]; // this is for printing text
#endif // !GLOBALS_H