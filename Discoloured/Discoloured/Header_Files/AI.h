/**********************************************************************************
* \file			AI.h
* \brief		AI logic functions belongs here/AI.c
* \author		Seow Jun Hao Darren
* \version		1.2
* \date			2019
*
*	The AI Library that contains the AI mechanics such as render/movement/spawning/
*	initialization

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.2 - Upgrade AI to learn how to shoot in a direction where it 
					  contain the least piant
*				1.1 - Upgrade AI movement logic so that they can move from one 
					  point to another.
*
*				1.0 - Initial commit
**********************************************************************************/
#ifndef _AI_
#define _AI_
#include "../Libraries/Point.h"
#include "../Libraries/Object.h"
#include "../Libraries/Collider.h"

typedef struct AIComponent 
{
	Point bodyPosition;
	Point directionHeadPosition;
	Point prevPos;
	Point deltaPosition;
	Point waypoint;
	AIDirection direction;
	FaceDirection shootDirection;
	BoxCollider collider;
	double euler;
	double velocity;
	float respawnTimer;
	float shootTimer;
	float invulnerableBlinkTimer;
	int bodyColor;
	int headColor;
	int teamColor;
	int teamShotColor;
	int shotColor;
	int b_isActive;
	int isRendering;
	int b_canUseUltimate;
	int bIsDead;
	int bIsInvulnerable;
	int bSetWayPoint;
	int bGoToWayPoint;
	int sizeX;
	int sizeY;
	char body;
	char directionHead;
	char respawnTag[5];
}AIComponent;

/*
 * \brief	Initializes the AI, needs to be called first
 * \param	takes in AIComponent and team color
*/
void InitializeAI(AIComponent *AI,int _color);
/*
 * \brief	Spawn the AI
 * \param	takes in AIComponent  
*/
void SpawnAI(AIComponent *_AI);

/*
 * \brief	Function that Control AI Movement
 * \param	takes in AIComponent
*/
void AIMovement(AIComponent *_AI);

/*
 * \brief	Function that allows AI to Shoot
 * \param	takes in AIComponent
*/
void AIShoot(AIComponent *_AI);

/*
 * \brief	Function that allows AI to Cast Ultimate
 * \param	takes in AIComponent
*/
void AICastUltimate(AIComponent *_AI);

/*
 * \brief	Update the AI collider
 * \param	takes in AIComponent
 * \note	done by Jolyn
*/
void UpdateAICollider(AIComponent *_AI);

/*
 * \brief	Function that Render AI trails
 * \param	takes in AIComponent
 * \note	Done by David
*/
void RenderAITrails(AIComponent *_AI);

/*
 * \brief	Function that Render AI tag
 * \param	takes in AIComponent
 * \note	Done by David
 
*/
void RenderAITag(AIComponent *_AI);

/*
 * \brief	Function that Render AI 
 * \param	takes in AIComponent
 * \note	Done by Darren, Edited by David
*/
void RenderAI(AIComponent *_AI);
#endif
