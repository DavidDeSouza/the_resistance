/*========================================================================
* \file			DeltaTime.h
* \brief		for implementing the euler check functionality
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef DELTATIME_H
#define DELTATIME_H

//========================================================================
//	author: David De Souza
//	for smoother movement in the console
//	checks if the euler value is greater than 1,
//	if true returns 1, if false returns 0
//========================================================================
int CheckEuler(double *euler, double velocity);

#endif