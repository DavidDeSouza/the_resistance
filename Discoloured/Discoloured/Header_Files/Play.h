/*========================================================================
* \file			Play.h
* \brief		Handles the Play state of the game 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef PLAY_H
#define PLAY_H

//========================================================================
//	global variables
//========================================================================
int preGamePause;

//========================================================================
//	author: David De Souza
//  runs on entry into state
//========================================================================
void Game_EnterState();

//========================================================================
//	author: David De Souza
//  runs on exiting the state
//========================================================================
void Game_ExitState();

//========================================================================
//	author: David De Souza
//  handles input events
//========================================================================
void Game_ProcessInput();

//========================================================================
//	author: David De Souza
//  The update loop of the state
//========================================================================
void Game_Update();

//========================================================================
//	author: David De Souza
//  handles rendering in the state
//========================================================================
void Game_Render();

#endif