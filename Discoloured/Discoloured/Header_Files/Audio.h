/*========================================================================
* \file			Audio.h
* \brief		Handles Audio engine using fMod
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef AUDIO_H
#define AUDIO_H
#include <fmod.h>

//========================================================================
//	global variables
//========================================================================
FMOD_SYSTEM *audioSystem;
FMOD_CHANNELGROUP *channelGroup;

struct Channel
{
	FMOD_CHANNEL *sfxBullets;
	FMOD_CHANNEL *sfx;
	FMOD_CHANNEL *bgm;
}channel;

struct Audio
{
	FMOD_SOUND *shot;
	FMOD_SOUND *splash;
	FMOD_SOUND *drumRoll;
	FMOD_SOUND *whistle;
	FMOD_SOUND *menuBgm;
	FMOD_SOUND *gameBgm;
}audio;


//========================================================================
//	author: David De Souza
//  Initialises audio system and loads audio files 
//========================================================================
void InitialiseAudio();

//========================================================================
//	author: David De Souza
//  handles playing of audio
//========================================================================
void PlayAudio(FMOD_SOUND *_sound);

//========================================================================
//	author: David De Souza
//  pauses an audio channel 
//========================================================================
void PauseAudio(FMOD_CHANNEL *_channel);

//========================================================================
//	author: David De Souza
//  handles the cleanup of the audio system 
//========================================================================
void CloseAudio();

#endif