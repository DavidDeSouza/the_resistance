/*========================================================================
* \file			Credits.c
* \brief		File that Loops frames of credits
* \author		Srikesh Sundaresan
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/

#ifndef CREDITS_H
#define CREDITS_H


//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Calls individual frames of the credit sequence 
//  (CreditsLogo && CreditsData functions) based on the timer reached. 
//	Inputs: NIL
//========================================================================
void Credits_Render();

//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Checks if SPACE/ESC/ENTER is pressed. Runns counters that
//	are called in Credits_Render.
//	Inputs: NIL
//========================================================================

void Credits_Update();

//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Does nothing. Clears buffer when Machine Changes state
//	Inputs: NIL
//========================================================================

void Credits_ExitState();

#endif