/*========================================================================
* \file			Readfile.h
* \brief		File that was made to read coordinate values and draw 
                walls for the map. Later updates included the ability to
				draw these walls based on ratios of the console size, 
				instead of specific coordinates.  Final update added mirror
				functionality to the walls.

* \author		Srikesh Sundaresan, David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/

#ifndef READFILE_H
#define READFILE_H

//========================================================================
//	Author: David De souza, Srikesh Sundaresan
//  
//	Description: Reads notepadfiles for 2 coordinates based on console size.
//               Mirrors said coord and draws rectangle with Darren's code.
 
//	Inputs: NIL
//========================================================================
void MapRandomiser();

//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Finds midpoint of The console no matter what side it is
//	Inputs: NIL
//========================================================================

int Calc_ConsoleMid(int, int);

#endif // ! READFILE_H