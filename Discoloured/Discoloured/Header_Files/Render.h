/*========================================================================
* \file			Render.h
* \brief		Renders all graphics to the console
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef RENDER_H
#define RENDER_H

//========================================================================
//	author: David De Souza
//	Handles all rendering to be done in the main game state 
//========================================================================
void RenderToConsole();

//========================================================================
//	author: David De Souza
//	Renders the controller and keyboard controls 
//========================================================================
void RenderControlsGraphic();

//========================================================================
//	author: David De Souza
//	Renders the Pre-Game countdown graphics 
//========================================================================
void PreGameRender(int _timer);

//========================================================================
//	author: David De Souza
//	Renders the Frames Per Second
//========================================================================
void RenderFPS();

//========================================================================
//	author: David De Souza
//	Renders the number of kills each team has
//========================================================================
void RenderKillCounters();

//========================================================================
//	author: Jolyn Wong Kaiyi
//	Render Debugging Information
//========================================================================
void RenderDebugInfo(int _show);

//========================================================================
//	author: David De Souza
//  Renders the Winner Team A graphic 
//========================================================================
void RenderTeamAWinner();

//========================================================================
//	author: David De Souza
//  Renders the Winner Team B graphic 
//========================================================================
void RenderTeamBWinner();

//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Renders frames for the splashscreen
//	Inputs: NIL
//========================================================================
void DigipenLogoFrame_1();
void DigipenLogoFrame_2();
void DigipenLogoFrame_3();
void DigipenLogoFrame_4();
void DigipenLogoFrame_5();
void DigipenLogoFrame_6();
void DigipenLogoFrame_7();
void DigipenLogoFrame_8();
void DigipenLogoFrame_9();
void DigipenLogoFrame_10();
void DigipenLogoFrame_11();
void DigipenLogoFrame_12();
void DigipenLogoFrame_13();
void DigipenLogoFrame_14();
void DigipenLogoFrame_15();
void DigipenLogoFrame_16();
void DigipenLogoFrame_17();
void DigipenLogoFrame_18();
void DigipenLogoFrame_19();
void DigipenLogoFrame_20();
void DigipenLogoFrame_21();
void DigipenLogoFrame_22();
void DigipenLogoFrame_23();
void DigipenLogoFrame_24();
void DigipenLogoFrame_25();
void DigipenLogoFrame_26();
void DigipenLogoFrame_27();
void DigipenLogoFrame_28();
void DigipenLogoFrame_29();
void DigipenLogoFrame_30();
void DigipenLogoFrame_31();
void DigipenLogoFrame_32();
void DigipenLogoFrame_33();
void DigipenLogoFrame_34();
void DigipenLogoFrame_35();
void DigipenLogoFrame_36();
void DigipenLogoFrame_37();
void DigipenLogoFrame_38();
void DigipenLogoFrame_39();
void DigipenLogoFrame_40();

//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Renders frames of team logo for the Credits
//	Inputs: NIL
//========================================================================

void Render_CreditsLogo_Frame0();
void Render_CreditsLogo_Frame1();
void Render_CreditsLogo_Frame2();
void Render_CreditsLogo_Frame3();
void Render_CreditsLogo_Frame4();
void Render_CreditsLogo_Frame5();
void Render_CreditsLogo_Frame6();
void Render_CreditsLogo_Frame7();
void Render_CreditsLogo_Frame8();
void Render_CreditsLogo_Frame9();
void Render_CreditsLogo_Frame10();
void Render_CreditsLogo_Frame11();
void Render_CreditsLogo_Frame12();
void Render_CreditsLogo_Frame13();
void Render_CreditsLogo_Frame14();
void Render_CreditsLogo_Frame15();


//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Renders frames of Data (all involved ppl) for the Credits
//	Inputs: NIL
//========================================================================

void Render_CreditsData_Frame0();
void Render_CreditsData_Frame1();
void Render_CreditsData_Frame2();
void Render_CreditsData_Frame3();
void Render_CreditsData_Frame4();
void Render_CreditsData_Frame5();
void Render_CreditsData_Frame6();
void Render_CreditsData_Frame7();
void Render_CreditsData_Frame8();
void Render_CreditsData_Frame9();
void Render_CreditsData_Frame10();
void Render_CreditsData_Frame11();
void Render_CreditsData_Frame12();
void Render_CreditsData_Frame13();
void Render_CreditsData_Frame14();
void Render_CreditsData_Frame15();
void Render_CreditsData_Frame16();
void Render_CreditsData_Frame17();
void Render_CreditsData_Frame18();
void Render_CreditsData_Frame19();
void Render_CreditsData_Frame20();
void Render_CreditsData_Frame21();
void Render_CreditsData_Frame22();
void Render_CreditsData_Frame23();
void Render_CreditsData_Frame24();
void Render_CreditsData_Frame25();
void Render_CreditsData_Frame26();
void Render_CreditsData_Frame27();
void Render_CreditsData_Frame28();
void Render_CreditsData_Frame29();
void Render_CreditsData_Frame30();
void Render_CreditsData_Frame31();
void Render_CreditsData_Frame32();
void Render_CreditsData_Frame33();
void Render_CreditsData_Frame34();
void Render_CreditsData_Frame35();
void Render_CreditsData_Frame36();
void Render_CreditsData_Frame37();
void Render_CreditsData_Frame38();
void Render_CreditsData_Frame39();
void Render_CreditsData_Frame40();

#endif