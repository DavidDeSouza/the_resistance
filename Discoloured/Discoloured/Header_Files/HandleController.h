/*========================================================================
* \file			HandleController.h
* \brief		handles initialisation and input of controllers 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. 
				Reproduction or disclosure of this file or its contents 
				without the prior written consent of DigiPen Institute of 
				Technology is prohibited.
========================================================================*/
#ifndef HANDLECONTROLLER_H
#define HANDLECONTROLLER_H
#include "../Header_Files/Player.h"
#include <SDL.h>


// joystick instances
SDL_GameController *joystick1;
SDL_GameController *joystick2;
SDL_GameController *joystick3;

//========================================================================
//	author: David De Souza
//  Initialises all controllers connected to the PC
//	returns the number of controllers initialised 
//========================================================================
int InitialiseControllers();

//========================================================================
//	author: David De Souza
//	handles the movement of the player objects controlled by the 
//	controllers
//========================================================================
void MoveControllerPlayers(PlayerComponent *_player);

//========================================================================
//	author: David De Souza
//	Handles joystick inputs from the first controller instance 
//========================================================================
void HandleController1JoyStick();

//========================================================================
//	author: David De Souza
//	Handles joystick inputs from the second controller instance 
//========================================================================
void HandleController2JoyStick();

//========================================================================
//	author: David De Souza
//	Handles joystick inputs from the third controller instance 
//========================================================================
void HandleController3JoyStick();

//========================================================================
//	author: David De Souza
//	Handles button inputs from the first controller instance 
//========================================================================
void HandleController1Buttons();

//========================================================================
//	author: David De Souza
//	Handles button inputs from the second controller instance 
//========================================================================
void HandleController2Buttons();

//========================================================================
//	author: David De Souza
//	Handles button inputs from the third controller instance 
//========================================================================
void HandleController3Buttons();

//========================================================================
//	author: David De Souza
//	The main controller update loop 
//	Calls all the Handle Controller Input functions 
//========================================================================
void HandleAllControllerInputs();

//========================================================================
//	author: David De Souza
//	sets all controller instances to null
//  quits SDL
//========================================================================
void UnInitialiseControllers();
#endif // !HANDLECONTROLLER_H