/**********************************************************************************
* \file			Score.h
* \brief		Handles the calculation of map covered by Team colour and determine
				the winner based on that calculation
* \author		Jolyn Wong Kaiyi
* \date			2019
*
*	This script stores Score structure that is used for the calculation of the score,
*   which is detemined by how much of the map is covered in percentage. It also 
*   determines based on the calculations, the winner for the match.
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _SCORE_H_
#define _SCORE_H_

#include "../Header_Files/Team.h"

/* Score structure that contains information for scoring */
typedef struct
{
	Team team;
	float percentageCovered;
	float pixelsCovered;
	float pixelsPossibleToCover;

} Score;

/*
	author  Jolyn Wong

	brief	 Initialises a score object to set which team is this score going to represent
	         as well as the total area that can be covered to set that to be the 100%

	param	_scoreToInit - pointer to score object that is to be initialised
	        _team - the team that the score object should represent
			_possibleArea - the total possible area that can be covered in the map

	output  return the percentage(%) of area covered by _color
*/
void InitialiseScore(Score* _scoreToInit, Team _team, float _possibleArea);

/*
	author  Jolyn Wong

	brief	 Calculates the percentage of map covered by a specified color and 
	         the total amount of possible area to be covered over a few frames

	param	 _score - score object to check for which team to calculate for and store the score
			 _mapRows - row that the map is currently on
			 _mapCols - column that the map is currently on
			 
	output   return the percentage(%) of area covered by _color
*/
void CalculateColorPercentage(Score* _score, int _mapRow, int _mapCol);

/*
	author  Jolyn Wong

	brief	 Calculates the percentage of map covered by a specified color and
			 the total amount of possible area to be covered in a single frame

	param	 _score - score object to check for which team to calculate for and store the score
			 _mapRows - row that the map is currently on
			 _mapCols - column that the map is currently on

	output   return the percentage(%) of area covered by _color
*/
void CalculateTotalColorPercentage(Score* _score, int _mapRows, int _mapCols);

/*
	author  Jolyn Wong

	brief	 Reset the current calculation for percentage of map covered by specified color 
             back to default
*/
void ResetScore(Score* _score);

/*
	author  Jolyn Wong

	brief	 Sets the winning team based on which score that is winning

	param	 _winningScore - the score object that stores the winning team
*/
void SetCurrentWinner(Score _winningScore);

/*
	author  Jolyn Wong

	brief	 Gets the winning team based on the current score that is winning

	output   return the Team that the winner is on
*/
Team GetCurrentWinner();

#endif