/*========================================================================
* \file			Instructions.h
* \brief		Hanldes the state "Instructions" 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

//========================================================================
//	author: David De Souza
//  Handles Input events 
//========================================================================
void Instructions_ProcessInput();

//========================================================================
//	author: David De Souza
//  Handles rendering in the Instructions state
//========================================================================
void Instructions_Render();

#endif