/*========================================================================
* \file			GameOver.h
* \brief		Handles the Game Over state
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef GAMEOVER_H
#define GAMEOVER_H

//========================================================================
//	author: David De Souza
//  runs on the first frame upon entry into state 
//========================================================================
void GameOver_EnterState();

//========================================================================
//	author: David De Souza
//  runs on the exiting the state
//========================================================================
void GameOver_ExitState();

//========================================================================
//	author: David De Souza
//  handles input events 
//========================================================================
void GameOver_ProcessInput();

//========================================================================
//	author: David De Souza
//  the update loop of the state
//========================================================================
void GameOver_Update();

//========================================================================
//	author: David De Souza
//  handles rendering in the state
//========================================================================
void GameOver_Render();

#endif 