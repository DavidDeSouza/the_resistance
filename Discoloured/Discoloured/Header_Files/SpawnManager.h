/**********************************************************************************
* \file			SpawnManager.h
* \brief		Handles spawn related interactions such as respawn and invulnerability
* \author		Jolyn Wong Kaiyi
* \date			2019
*
*	This script stores the structure SpawnPoint that is used for creating a point 
*	where players and AI will be spawned at. Interactions with the players and AIs
*   such as respawning after death and invulnerability is also handled by this script.
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _SPAWN_MANAGER_H_
#define _SPAWN_MANAGER_H_

#include "../Header_Files/Player.h"
#include "../Header_Files/AI.h"
#include "../Header_Files/Team.h"

typedef struct
{
	Team team;
	Point position;

	float resetTimer;
	int isTaken;
	char charRepresentation;

} SpawnPoint;

/*
	author  Jolyn Wong

	brief	Initialises the spawn points, which will be used to be the location
	        where the players and AIs will spawn at when they die/ initialise

	param	_team - the team that these spawn points belong to
	        _posX - the position in the col/X value
			_posY - the position in the row/Y value
*/
void InitialiseSpawnPoints(Team _team, int _offsetX, int _offsetY);

/*
	author  Jolyn Wong

	brief	Initialises the spawn points, which will be used to be the location
	        where the players and AIs will spawn at when they die/ initialise

	param	_team - the team that these spawn points belong to
	        _startingPtX - starting point x of area to spawn in
			_startingPtY - starting point y of area to spawn in
			_endingPtX - ending point x of area to spawn in
			_endingPtY - ending point y of area to spawn in
*/
void InitialiseSpawnPointsInArea(Team _team, int _startingPtX, int _startingPtY, int _endingPtX, int _endingPtY);

/*
	author  Jolyn Wong

	brief	Render Spawn Points in the map
*/
void RenderSpawnPoints();

/*
	author  Jolyn Wong

	brief	Checks if there is any players that has the death flag set to true,
			hence calling the respawn function to start respawning them
*/
void CheckRespawnPlayers();

/*
	author  Jolyn Wong

	brief	Checks if there is any players that has the invulnerability flag set to true,
			hence calling the invulnerability function to start make them invulnerable
*/
void CheckVulnerabilityPlayers();

/*
	author  Jolyn Wong

	brief	Checks if there is any AIs that has the death flag set to true,
			hence calling the respawn function to start respawning them
*/
void CheckRespawnAI();

/*
	author  Jolyn Wong

	brief	Checks if there is any AIs that has the invulnerability flag set to true,
			hence calling the invulnerability function to start make them invulnerable
*/
void CheckVulnerabilityAI();

/*
	author  Jolyn Wong

	brief	Sets the respawn points back to not taken, so that the players or AIs are able 
			to respawn at the spawn points randomly
*/
void ResetSpawnPoint();

/*
	author  Jolyn Wong

	brief	Handles the respawning of player, by choosing which spawn point to spawn at and
			resetting some values of the player back to default

	param	*_player - pointer that points to player object that is to be updated
			_spawnInterval - the time interval to pass before player is respawned

	Output  Player object that is pointed by the pointer is set to new spawn point and resetted
*/
void RespawnPlayer(PlayerComponent *_player, float _spawnInterval);

/*
	author  Jolyn Wong

	brief	Handles the respawning of AI, by choosing which spawn point to spawn at and
			resetting some values of the AI back to default

	param	*_AI - pointer that points to AI object that is to be updated
			_spawnInterval - the time interval to pass before AI is respawned

	Output  AI object that is pointed by the pointer is set to new spawn point and resetted
*/
void RespawnAI(AIComponent *_AI, float _spawnInterval);

/*
	author  Jolyn Wong

	brief	Handles the invulnerability of player, by checking if there is still time 
			left before player is able to be killed again

	param	*_player - pointer that points to player object that is to be updated
			_elapsedTime - the time that has passed since the player object is respawned
			_duration - how long the invulnerability will last

	Output  Player object that is pointed by the pointer is set invulnerable until duration is reached
*/
void SetInvulnerabilityPlayer(PlayerComponent *_player, float* _elapsedTime, float _duration);

/*
	author  Jolyn Wong

	brief	Handles the invulnerability of AI, by checking if there is still time
			left before AI is able to be killed again

	param	*_player - pointer that points to AI object that is to be updated
			_elapsedTime - the time that has passed since the AI object is respawned
			_duration - how long the invulnerability will last

	Output  AI object that is pointed by the pointer is set invulnerable until duration is reached
*/
void SetInvulnerabilityAI(AIComponent *_AI, float* _elapsedTime, float _duration);

#endif