/*========================================================================
* \file			PreGame.h
* \brief		Handles the PreGame state, just before game starts 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef PREGAME_H
#define PREGAME_H

//========================================================================
//	author: David De Souza
//  runs on entry into state
//========================================================================
void PreGame_EnterState();

//========================================================================
//	author: David De Souza
//  runs on exiting the state
//========================================================================
void PreGame_ExitState();

//========================================================================
//	author: David De Souza
//  handles input events
//========================================================================
void PreGame_ProcessInput();

//========================================================================
//	author: David De Souza
//  the update loop of the state 
//========================================================================
void PreGame_Update();

//========================================================================
//	author: David De Souza
//  handles rendering in the state
//========================================================================
void PreGame_Render();

#endif