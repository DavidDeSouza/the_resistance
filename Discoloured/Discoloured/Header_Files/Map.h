/*========================================================================
* \file			Map.h
* \brief		handles Rendering of the game Map
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef _MAP_H
#define _MAP_H

#include "../Libraries/Point.h"

typedef struct Map
{
	Point position;
	char c;
	int color;
}Map;

//========================================================================
//	author: 
//========================================================================
void InitializeMapColor();

//========================================================================
//	author: David De Souza
//  Renders the map
//========================================================================
void RenderMapColour();

//========================================================================
//	author: David De Souza
//  Renders the background white
//========================================================================
void RenderBackGroundWhite();

//========================================================================
//	author: David De Souza
//  Renders the UI 
//========================================================================
void RenderUIArea();

#endif
