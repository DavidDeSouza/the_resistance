/*========================================================================
* \file			Player.h
* \brief		File that contains all player functions
* \author		Srikesh Sundaresan
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/

#ifndef _PLAYER_
#define _PLAYER_

#include "../Libraries/Point.h"
#include "../Libraries/Object.h"
#include "../Libraries/Collider.h"

//========================================================================
//	global variables
//========================================================================
typedef enum
{
	Ready_True,
	Ready_False
}Ready;

typedef struct PlayerComponent
{
	Point bodyPosition;
	Direction direction;
	Direction shootingDirection;
	BoxCollider collider;

	double Euler;
	float respawnTimer;
	float invulnerableBlinkTimer;
	float shootTimer;
	float speed;
	int bodyColor;
	int headColor;
	int teamColor;
	int shotColor;
	int xDir;
	int yDir;
	int sizeX;
	int sizeY;
	int bUsedUltimate;
	int bIsDead;
	int bIsInvulnerable;
	int bIsActive;
	int isRendering;
	Ready isReady;
	char body;
	char respawnTag[5];
}PlayerComponent;

//========================================================================
//	author: Srikesh Sundaresan, David De Souza, Jolyn Wong
//  Description: Initialize above struct
//  Inputs: Player pointer & char
//========================================================================
void InitializePlayer(PlayerComponent *_player, char _body);

//========================================================================
//	author: Srikesh Sundaresan, Jolyn Wong
//  Description: Sets the box around the player
//  Inputs: Player pointer
//========================================================================
void UpdatePlayerCollider(PlayerComponent *_player);

//========================================================================
//	author: Srikesh Sundaresan, David De Souza, Jolyn Wong
//  Description: reads input based on player keystrokes. Contains keys for
//				 basic player movement, shooting and ultimate ability
//  Inputs: NIL
//========================================================================
void ReadInput();

//========================================================================
//	author: Jolyn Wong
//  Description: Renders Player according to team colour
//  Inputs: NIL
//========================================================================
void RenderPlayer(PlayerComponent *_player);

//========================================================================
//	author: Srikesh Sundaresan
//  Description: moves player according to keystrokes & deltaTime
//  Inputs: NIL
//========================================================================
void Move(PlayerComponent *_player, double _velocity);

//========================================================================
//	author: David De Souza
//  Description: Renders player trails accoding to player movement
//  Inputs: NIL
//========================================================================
void RenderPlayerTrails(PlayerComponent *_player);


//========================================================================
//	author: David De Souza
//  Description: Renders player tag right before game starts
//  Inputs: NIL
//========================================================================
void RenderPlayerTag(PlayerComponent *_player);

//========================================================================
//	author: Srikesh Sundaresan
//  Description: Resets playertag
//  Inputs: NIL
//========================================================================
void ResetPlayer(PlayerComponent *_player);

#endif 