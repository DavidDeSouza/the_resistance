/*========================================================================
* \file			GameStateMachine.h
* \brief		handles the finite state machine of the game 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef GAMESTATEMACHINE_H
#define GAMESTATEMACHINE_H

//========================================================================
//	state machine global variables
//========================================================================
typedef enum GameState
{
	State_Default,
	State_Team_Options,
	State_Pre_Game,
	State_SplashScreen,
	State_Credits,
	State_MainMenu,
	State_Game,
	State_Game_Stats,
	State_GameOver,
	State_Instructions
}GameState;

GameState CurrentState;

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  runs on the first frame upon entry into state
//========================================================================
void StateMachine_StartFrame();

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  handles changing from one state to another
//========================================================================
void StateMachine_ChangeState(GameState newState);

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  handles input events
//========================================================================
void StateMachine_ProcessInput();

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  the state's update loop 
//========================================================================
void StateMachine_Update();

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  handles rendering in state
//========================================================================
void StateMachine_Render();

#endif