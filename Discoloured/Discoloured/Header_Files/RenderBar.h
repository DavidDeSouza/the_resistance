/**********************************************************************************
* \file			GameStatistics.h
* \brief		Handles render bar objects
* \author		Jolyn Wong Kaiyi
* \date			2019
*
*	This script stores the structure RenderBar that is used for indicating a bar
*	object. The render bar is used for showing the percentage of the map covered
*   by the respective teams. This script also contains functions to initialise,
*   update and render the render bar objects.
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _RENDERBAR_H_
#define _RENDERBAR_H_

#include "../Libraries/Point.h"
#include "../Header_Files/Team.h"
#include "../Header_Files/Score.h"

typedef struct
{
	Team team;
	Point minPoint;
	Point maxPoint;
	Point currPoint;
	int color;
	int toIncrease;

} RenderBar;

/*
	author  Jolyn Wong

	brief	Initialises an render bar object to have the respective minPoints,
			maxPoints and color according to inputs

	param	*_renderBar - a pointer to a render bar object, that is to be initialised
			minPointX - the x value for the minimium point of this render bar
			minPointY - the y value for the minimium point of this render bar
			maxPointX - the x value for the maximium point of this render bar
			maxPointY - the y value for the maximium point of this render bar
			color - the color to be used for the render bar

	Output	The render bar object pointed by the pointer is initialised to the default
			starting values
*/
void InitialiseRenderBar(RenderBar	*_renderBar, int minPointX, int minPointY, int maxPointX, int maxPointY, int color);

/*
	author  Jolyn Wong

	brief	Updates an existing render bar object

	param	*_renderBar - a pointer to a render bar object, that is to be updated
			_score - _score object to be read for comparision to determine how much to increase the bar
			_maxPercentage - the percentage that is the 100%/max of which this bar can go to

	Output	The render bar object pointed by the pointer is updated accordingly
*/
void Updating_RenderBar(RenderBar *_renderBar, const Score _score, const float _maxPercentage);

/*
	author  Jolyn Wong

	brief	Renders an existing render bar object

	param	_renderBar - the render bar object to be read, which will be used to indicate 
						 the parts of the map to render the bar
*/
void Rendering_RenderBar(RenderBar _renderBar);

#endif