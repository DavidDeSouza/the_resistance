/**********************************************************************************
* \file			Team.h
* \brief		base team mechanic that allows team assignment
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* The base team mechanic that allows team assignment  

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#ifndef _TEAM_H_
#define _TEAM_H_

#include "../Header_Files/Player.h"
#include "../Header_Files/AI.h"

typedef struct Team
{
	int color;
	int shotColor;
	int numOfPlayers;
	int maxNumOfPlayer;
	int bUltimateCharged;
	int ultimateBarPosition;
	int blastDistance;
	int numberOfKills;
	int ultimateCharge;
}Team;

/*
*\brief Initailize The Team variables
*\note	to be called first
*/
void InitializeTeam();

/*
*\brief Assign player to their assigned team on the PlayControlOption screen
*\param which Player and which team they have choosen to be on
*/
void AssignPlayerToTeam(PlayerComponent *_player, enum Side _team);

/*
*\brief Assign the AI to be fillers in the team of 4v4
*/
void SetAIIntoTeams();
#endif
