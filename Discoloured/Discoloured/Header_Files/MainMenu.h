/*========================================================================
* \file			MainMenu.h
* \brief		Handles the MainMenu state
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef MAINMENU_H
#define MAINMENU_H

//========================================================================
//	author: David De Souza
//  runs when entering into state 
//========================================================================
void MainMenu_EnterState();

//========================================================================
//	author: David De Souza
//  Runs when exiting the state
//========================================================================
void MainMenu_ExitState();

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  Handles Input events 
//========================================================================
void MainMenu_ProcessInput();

//========================================================================
//	author: David De Souza
//  The update loop of the state
//========================================================================
void MainMenu_Update();

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  Handles Rendering in the state 
//========================================================================
void MainMenu_Render();

#endif // MAINMENU_H