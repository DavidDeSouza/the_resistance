/*========================================================================
* \file			SplashScreen.h
* \brief		File that calls frames of splashscreen and switches game
				state to main menu when done
* \author		Srikesh Sundaresan
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/


#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Calls individual frames of the logo render sequence 
//  (DigipenLogoFrame) based on the timer reached. 
//	Inputs: NIL
//========================================================================
void SplashScreen_Render();

//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: if the animations ends || ESC / ENTER / SPACE pressed, to 
//  switch gamestate to MainMenu
//	Inputs: NIL
//========================================================================
void SplashScreen_Update();

//========================================================================
//	Author: David De Souza
//  
//	Description: Plays audio when splash screen exits
//	Inputs: NIL
//========================================================================
void SplashScreen_ExitState();


#endif // SplashScreen