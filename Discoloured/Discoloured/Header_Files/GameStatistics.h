/**********************************************************************************
* \file			GameStatistics.h
* \brief		Handles the game state GAME_STATS
* \author		Jolyn Wong Kaiyi
* \date			2019
*
*	This script handles the game state GAME_STATS, where the statistics of the match
*   played by the players are displayed here. There will be calculation of the 
*   percentage of the map covered by the respective teams and the winner is displayed.
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _GAME_STATISTICS_H_
#define _GAME_STATISTICS_H_

/*
	author  Jolyn Wong

	brief	 Called at the first frame when the state is entered, initialises any 
			 variables and game objects that are to be initialised before use
*/
void GameStatistics_EnterState();

/*
	author  Jolyn Wong

	brief	 Called at the last frame before leaving the state, clearing any
			 variables and game objects that is to be freed and reset
*/
void GameStatistics_ExitState();

/*
	author  Jolyn Wong

	brief	 Called every frame in this state, processes any inputs from the
	         players such as keypress etc
*/
void GameStatistics_ProcessInput();

/*
	author  Jolyn Wong

	brief	 Called every frame in this state, handling any game logic that is
			 required to be updated every frame
*/
void GameStatistics_Update();

/*
	author  Jolyn Wong

	brief	 Called every frame in this state, handling any objects that is to
	         rendered
*/
void GameStatistics_Render();

#endif