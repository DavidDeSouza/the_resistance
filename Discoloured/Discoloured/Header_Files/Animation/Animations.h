/**********************************************************************************
* \file			Animation.h
* \brief		Contains animation functions
* \author		Jolyn Wong Kaiyi
* \date			2019
*
*	This script contains functions that are used for animations
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _ANIMATION_H_
#define _ANIMATION_H_

/*
	author  Jolyn Wong 

	brief	Based on pointer to a boolean, set the boolean to be true/ false to create 
	        a blinking effect ( render in a frame, unrender in the next frame )

	param	_activeness - the boolean flag used to determine if to render or not
			_elapsedTime - the time to check if reached interval
*/
void BlinkingAnimation(int* _activeness, float* _elapsedTime);

#endif