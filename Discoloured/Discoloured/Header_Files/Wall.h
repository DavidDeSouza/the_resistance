/**********************************************************************************
* \file			Wall.h
* \brief		Contain the function for Walls
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* contains function like initializeWall and RenderWall

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#ifndef _WALL_
#define _WALL_
#include "../Libraries/Point.h"

typedef struct Wall {
	Point startPosition;
	Point endPosition;
	char c;
	int color;
}Wall;

/*\brief Initialization of Wall
 * must be called first
 */
void InitializeWall(int startPosX, int startPosY, int endPosX, int endPosY, int arrayIndex);

/*\brief rendering of wall*/
void RenderWall();
#endif
