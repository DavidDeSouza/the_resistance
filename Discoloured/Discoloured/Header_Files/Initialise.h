/*========================================================================
* \file			Initialise.h
* \brief		handles initialisation of the game 
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#ifndef INITIALISE_H
#define INITIALISE_H

//========================================================================
//	author: David De Souza
//  creates console window and initialises the game 
//========================================================================
void InitialiseGame();

#endif 