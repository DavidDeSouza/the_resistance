/**********************************************************************************
* \file			Bullet.h
* \brief		Handles bullet objects, and their interaction with other objects
				and scene
* \author		Jolyn Wong Kaiyi
* \date			2019
*
*	This script stores the structure Bullets that is used for creating a bullet and
*	pools them at the start of the game, which they will then be used for shooting
*	by the player and AI. It also handles the update and render of bullets, as well 
*	as interaction of the bullets with other objects.
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _BULLETS_H_
#define _BULLETS_H_

#include "../../Libraries/Vector2.h"
#include "../../Libraries/Object.h"

typedef struct Bullet
{
	Point pos;
	Point maxDisplacedPos;
	Vector2 dir;
	Direction directionInEnum;

	double euler;
	int isActive;
	int teamColor;
	int color;
	float speed;
	float deltaTime;
	char charRepresentation;
} Bullet;

/* 
	author  Jolyn Wong

	brief	Creates max number of bullets when game state starts, and pools them off screen
			Initialises bullets to be at default state

	param	_rep - ascii character representation
*/
void InitialiseBullets(char _rep);

/*
	author  Jolyn Wong

	brief	Determines which direction bullet should travel in terms of x and y coordinates

	param	_direction - Direction represented in enum

	Output	return a Vector2 that determines that direction of travel in terms of x and y coordinates
*/
Vector2 DetermineShootingDirection(Direction _direction);

/*
	author  Jolyn Wong

	brief	Checks if bullet specified has reached its maximium travel displacement

	param	_bullet - bullet to be checked
			_displacedPos - displaced position to check with

	Output	returns an int that represents a boolean to determine is bullet has reached its max displacement
*/
int HasReachedMaxDisplacement(Point _bulletPosition, Point _displacedPos);

/*
	author  Jolyn Wong

	brief	Creates max number of bullets when game state starts, and pools them off screen
			Initialises bullets to be at default state

	param	_startingShootingPos - where the bullet will start at
			_shotDir - direction of travel by bullet
			_maxDisplacement - maximium number of pixels bullet can travel along
			_teamColor - the color of the team the player that shot the bullet is in
			_color - color to cover the map when bullet is travelling
			_timeInterval - the interval at which bullet will be shot
			*_shotTime - pointer to an float object that is used to be a timer for 
						 when to shoot

	Output	return an int that represents a boolean to determine if shot is successful
*/
void ShootingBullets(Point _startShootingPos, Direction _shotDir, int _maxDisplacement, 
	                 int _teamColor, int _color, float _timeInterval, float *_shotTime);

/*
	author  Jolyn Wong

	brief	Checks, for each bullet, if the bullet hits 

	param	_bullet - pointer to the bullet object that is to be checked
			_targetPos - the next position to check for in regards to the bullet

	Output	return an int that represents a boolean to determine if bullet collided
			with any objects
*/
int CheckCollisionWithBullets(Bullet *_bullet, Point _targetPos);

/*
	author  David De Souza

	brief	Creates a splatter paint kind of effect when bullet is shot

	param	_bullet - the bullet that was shot
*/
void SplatterShot(Bullet *_bullet);

/*
	author  Jolyn Wong

	brief	Updates the bullets movement and interaction with items in the map
*/
void UpdateBulletsMovement();

/*
	author  Jolyn Wong

	brief	Render the bullets onto the console
*/
void RenderBullets();

/*
	author  Jolyn Wong

	brief	Resets the bullet specified in the param, by re-initialising it

	param	_bullet - the bullet that is to be re-initailised
*/
void ResetBullets(Bullet *_bullet);

#endif