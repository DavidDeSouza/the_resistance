/*========================================================================
* \file			Discoloured.c
* \brief		main driver file for the game, contains main() function
* \author		David De Souza
* \date			2019
*
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology.
				Reproduction or disclosure of this file or its contents
				without the prior written consent of DigiPen Institute of
				Technology is prohibited.
========================================================================*/
#include "Header_Files/Globals.h"
#include "Header_Files/Initialise.h"
#include "Header_Files/Render.h"
#include "Clock/Clock.h"
#include "Header_Files/AI.h"
#include "Header_Files/GameStateMachine.h"
#include <SDL.h>

//========================================================================
//	author: David De Souza, Srikesh Sundaresan
//  main function - code starts here
//  required the put parameters in the main function because SDL library 
//  defines the main function - SDL_main
//========================================================================
int main(int argc, char *argv[])
{
	InitialiseGame();
	StateMachine_ChangeState(State_SplashScreen);
	
	while (bGameIsRunning)
	{
 		Clock_GameLoopStart();
		StateMachine_StartFrame();
		StateMachine_ProcessInput();
		StateMachine_Update();
		StateMachine_Render();
	}
	return 0;
}