#include "../Header_Files/GameStateMachine.h"
#include"../Header_Files/Credits.h"
#include "../Header_Files/SplashScreen.h"
#include "../Header_Files/MainMenu.h"
#include "../Header_Files/Play.h"
#include "../Header_Files/GameOver.h"
#include "../Header_Files/PlayerControlOptions.h"
#include "../Header_Files/GameStatistics.h"
#include "../Header_Files/PreGame.h"
#include "../Header_Files/Instructions.h"
#include "../Console/Console.h"

CurrentState = State_Default;
static GameState RequestedState = State_Default;

void StateMachine_StartFrame()
{
	if (CurrentState != RequestedState)
	{
		switch (CurrentState)
		{
		case State_MainMenu:	
			MainMenu_ExitState();	
			break;
		case State_Game:		
			Game_ExitState();		
			break;
		case State_GameOver:	
			GameOver_ExitState();	
			break;
		case State_Team_Options:
			Controls_ExitState();
			break;
		case State_Pre_Game:
			PreGame_ExitState();
			break;
		case State_Game_Stats:
			GameStatistics_ExitState();
			break;
		case State_Credits:
			Credits_ExitState();
			break;
		case State_SplashScreen:
			SplashScreen_ExitState();
			break;
		

		default:				
			break;
		}
		
		CurrentState = RequestedState;

		switch (CurrentState)
		{
		case State_MainMenu:	
			MainMenu_EnterState();	
			break;
		case State_Game:		
			Game_EnterState();		
			break;
		case State_GameOver:	
			GameOver_EnterState();	
			break;
		case State_Team_Options:
			Controls_EnterState();
			break;
		case State_Pre_Game:
			PreGame_EnterState();
			break;
		case State_Game_Stats:
			GameStatistics_EnterState();
			break;
		case State_SplashScreen:
			SplashScreen_Render();
			break;
		case State_Credits:
			Credits_Render();
			break;

		default:				
			break;
		}
	}
}

void StateMachine_ChangeState(GameState newState)
{
	RequestedState = newState;
}

void StateMachine_ProcessInput()
{
	switch (CurrentState)
	{
	case State_MainMenu:	
		MainMenu_ProcessInput();	
		break;
	case State_Game:		
		Game_ProcessInput();		
		break;
	case State_GameOver:	
		GameOver_ProcessInput();	
		break;
	case State_Team_Options:
		Controls_ProcessInput();
		break;
	case State_Pre_Game:
		PreGame_ProcessInput();
		break;
	case State_Game_Stats:
		GameStatistics_ProcessInput();
		break;
	case State_Instructions:
		Instructions_ProcessInput();
		
		break;


	default: break;
	}
}

void StateMachine_Update()
{
	switch (CurrentState)
	{
	case State_MainMenu:	
		MainMenu_Update();			
		break;
	case State_Game:		
		Game_Update();				
		break;
	case State_GameOver:	
		GameOver_Update();			
		break;
	case State_Team_Options:
		Controls_Update();
		break;
	case State_Pre_Game:
		PreGame_Update();
		break;
	case State_Game_Stats:
		GameStatistics_Update();
		break;
	case State_SplashScreen:
		SplashScreen_Update();
		break;
	case State_Credits:
		Credits_Update();
		break;

	default: break;
	}
}

void StateMachine_Render()
{
	// Clear the Rendering Buffer
	Console_ClearRenderBuffer();

	// Render the scenes in the Buffer
	switch (CurrentState)
	{
	case State_MainMenu:	
		MainMenu_Render();			
		break;
	case State_Game:		
		Game_Render();				
		break;
	case State_GameOver:	
		GameOver_Render();			
		break;
	case State_Team_Options:
		Controls_Render();
		break;
	case State_Pre_Game:
		PreGame_Render();
		break;
	case State_Game_Stats:
		GameStatistics_Render();
		break;
	case State_SplashScreen:
		SplashScreen_Render();
		break;
	case State_Instructions:
		Instructions_Render();
		break;
	case State_Credits:
		Credits_Render();
		break;

	default: break;
	}

	// Copy the Render Buffer to the Screen
	Console_SwapRenderBuffer();
}