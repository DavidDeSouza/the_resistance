#include "../Header_Files/PreGame.h"
#include "../Header_Files/GameStateMachine.h"
#include "../Console/Console.h"
#include "../Header_Files/Render.h"
#include "../Libraries/GameTime.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/Globals.h"
#include <stdio.h>

int preGameTimer;
char timerStringBuffer[30];
float timerDeltaTime = 0.0f;

void PreGame_ProcessInput()
{

}

void PreGame_Update()
{
	if (CheckTimerReached(&timerDeltaTime, 1000.0f))
	{
		sprintf_s(timerStringBuffer, sizeof(timerStringBuffer) / sizeof(timerStringBuffer[0]),
			"Loading... %d", preGameTimer);
		preGameTimer--;
	}
	if (preGameTimer < 0)
	{
		StateMachine_ChangeState(State_Game);
	}
}

void PreGame_Render()
{
	RenderBackGroundWhite();
	RenderControlsGraphic();
	Console_SetRenderBuffer_Colour_String(33, 40, timerStringBuffer, FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void PreGame_EnterState()
{
	preGameTimer = 7;
}

void PreGame_ExitState()
{

}