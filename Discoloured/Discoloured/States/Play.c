#include <Windows.h>
#include <stdio.h>
#include "../Header_Files/Play.h"
#include "../Header_Files/GameStateMachine.h"
#include "../Header_Files/Paint/Bullets.h"
#include "../Header_Files/Player.h"
#include "../Header_Files/Globals.h"
#include "../Header_Files/AI.h"
#include "../Console/Console.h"
#include "../Header_Files/ReadFile.h"
#include "../Header_Files/Render.h"
#include "../Header_Files/HandleController.h"  
#include "../Header_Files/PlayerControlOptions.h"
#include "../Libraries/GameTime.h"
#include "../Header_Files/Team.h"
#include "../Header_Files/UltimateAbility.h"
#include "../Random/Random.h"
#include "../Header_Files/SpawnManager.h"
#include "../Header_Files/Audio.h"

//========================================================================
//	variables
//========================================================================
const float g_gameDuration = 2.0f; // this is in minutes
float g_gameDurationInMilliseconds; // this is in milliseconds
float g_gameDeltaTime;
float g_remainingDuration_Millis, g_remainingDuration_Secs;
int g_remainingDuration_Mins;
float g_ultimateDeltaTime = 0.0f;
int preGamePause = 7;
float preGamePauseDeltaTime = 0.0f;
int g_showDebugInfo;
int whistlePlayed;



void Game_ProcessInput()
{
	// Set timer to zero to end game
	/*if (GetAsyncKeyState(VK_HOME) & 1)
	{
		g_gameDeltaTime = g_gameDurationInMilliseconds;
	}*/

	// Set player to die
	/*if (GetAsyncKeyState(VK_END) & 1)
	{
		player1.bIsDead = 1;
	}*/

	// Set debugging info to render or not
	if (GetAsyncKeyState(VK_1) & 1)
		g_showDebugInfo = !g_showDebugInfo;

	if (preGamePause)
		return;
	ReadInput();
	HandleAllControllerInputs();
}

void Game_Update()
{
	if (preGamePause) 
	{
		if (CheckTimerReached(&preGamePauseDeltaTime, 1000.0f))
		{
			preGamePause--;
		}
		if (preGamePause == 1 && !whistlePlayed)
		{
			PlayAudio(audio.whistle);
			whistlePlayed = 1;
		}
	}

	if (!preGamePause)
	{
		for (int i = 0; i < MAXNUMBEROFAI; i++)
		{
			if (AI[i].b_isActive == 0)
			{
				continue;
			}
			if(!AI[i].bIsDead)
			{
				AIMovement(&AI[i]);
				AIShoot(&AI[i]);
				AICastUltimate(&AI[i]);
			}
		}

		UpdateBulletsMovement();

		// Changes the state when game timer is finished 
		if (CheckTimerReached(&g_gameDeltaTime, g_gameDurationInMilliseconds))
		{
			PlayAudio(audio.whistle);
			StateMachine_ChangeState(State_Game_Stats);
		}


		if (numberOfControllers == 1)
		{
			MoveControllerPlayers(&player2);
		}
		else if (numberOfControllers == 2)
		{
			MoveControllerPlayers(&player2);
			MoveControllerPlayers(&player3);
		}
		else if (numberOfControllers == 3)
		{
			MoveControllerPlayers(&player2);
			MoveControllerPlayers(&player3);
			MoveControllerPlayers(&player4);
		}

		// Calculate the remaining duration of the Game Timer
		g_remainingDuration_Millis = g_gameDurationInMilliseconds - g_gameDeltaTime;

		// every 10 seconds charge ultimate ability

		if (CheckTimerReached(&g_ultimateDeltaTime, 1000.0f))
		{
			ChargeUltimate(&teamA);
			ChargeUltimate(&teamB);
		}

		// Check for any player or AI that is to be respawned
		CheckRespawnPlayers();
		CheckVulnerabilityPlayers();

		CheckRespawnAI();
		CheckVulnerabilityAI();

		ResetSpawnPoint();
	}
	FMOD_System_Update(audioSystem);
}

void Game_Render()
{
	RenderToConsole();
	RenderDebugInfo(g_showDebugInfo);

	// Render Game Timer
	if (!preGamePause)
	{
		DisplayTimer_MinutesSeconds(g_remainingDuration_Millis, &g_remainingDuration_Mins, &g_remainingDuration_Secs);
		sprintf_s(stringBuffer, sizeof(stringBuffer), "%d mins %.0f seconds", g_remainingDuration_Mins, g_remainingDuration_Secs);
		Console_SetRenderBuffer_Colour_String(WINDOWSIZEX / 2 - 7, MAPSTARTPOSY - 1, stringBuffer, FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}

	if (preGamePause > 0)
		PreGameRender(preGamePause);
}

void Game_EnterState()
{
	AssignPlayerToTeam(&player1, side[0]);
	if (numberOfControllers >= 1)
	{
		AssignPlayerToTeam(&player2, side[1]);
	}
	if (numberOfControllers >= 2)
	{
		AssignPlayerToTeam(&player3, side[2]);
	}
	if (numberOfControllers >= 3)
	{
		AssignPlayerToTeam(&player4, side[3]);
	}
	preGamePause = 7;
	whistlePlayed = 0;
	MapRandomiser();

	PlayAudio(audio.gameBgm);

	// Initialise SpawnPoints
	InitialiseSpawnPointsInArea(teamA, MAPSTARTPOSX + SPAWNPOINT_OFFSET, MAPSTARTPOSY + SPAWNPOINT_OFFSET,
									   MAPSTARTPOSX + SPAWNPOINT_OFFSET, MAPSIZEY - SPAWNPOINT_OFFSET);
	InitialiseSpawnPointsInArea(teamB, MAPSIZEX - SPAWNPOINT_OFFSET, MAPSTARTPOSY + SPAWNPOINT_OFFSET,
									   MAPSIZEX - SPAWNPOINT_OFFSET, MAPSIZEY - SPAWNPOINT_OFFSET);

	SetAIIntoTeams();
	InitializeWall(MAPSTARTPOSX, MAPSTARTPOSY, MAPSIZEX - 1, MAPSIZEY - 1, 0);

	// Initialise and Spawn Player
	InitializePlayer(&player1, '1');
	RespawnPlayer(&player1, 0.0f);
	UpdatePlayerCollider(&player1);

	if (numberOfControllers == 1)
	{
		InitializePlayer(&player2, '2');
		RespawnPlayer(&player2, 0.0f);
		UpdatePlayerCollider(&player2);
	}
	else if (numberOfControllers == 2)
	{
		InitializePlayer(&player2, '2');
		RespawnPlayer(&player2, 0.0f);
		UpdatePlayerCollider(&player2);
		InitializePlayer(&player3, '3');
		RespawnPlayer(&player3, 0.0f);
		UpdatePlayerCollider(&player3);
	}
	else if (numberOfControllers == 3)
	{
		InitializePlayer(&player2, '2');
		RespawnPlayer(&player2, 0.0f);
		UpdatePlayerCollider(&player2);
		InitializePlayer(&player3, '3');
		RespawnPlayer(&player3, 0.0f);
		UpdatePlayerCollider(&player3);
		InitializePlayer(&player4, '4');
		RespawnPlayer(&player4, 0.0f);
		UpdatePlayerCollider(&player4);
	}

	
	//InitializeAI(&AI[0],teamA.color);
	for (int i = 0; i < MAXNUMBEROFAI; i++)
	{
		if (AI[i].b_isActive)
		{
			RespawnAI(&AI[i], 0.0f);
			//SpawnAI(&AI[i]);
		}
	}

	InitializeMapColor();
	InitialiseBullets((char)254);

	// Initialising Variables 
	g_gameDeltaTime = 0.0f;
	g_gameDurationInMilliseconds = ConvertFromMinutes(g_gameDuration);
	g_showDebugInfo = 0;
}

void Game_ExitState()
{
	for (int i = 0; i < MAXNUMBEROFAI; i++)
	{
		AI[i].b_isActive = 0;
	}
	ResetSpawnPoint();
	teamA.numOfPlayers = 0;
	teamB.numOfPlayers = 0;
	ResetSpawnPoint();
	PauseAudio(channel.bgm);
	player1.bIsActive = 0;
	player2.bIsActive = 0;
	player3.bIsActive = 0;
	player4.bIsActive = 0;
}