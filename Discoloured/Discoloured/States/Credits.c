#include "../Header_files/Credits.h"
#include "../Libraries/GameTime.h"
#include <Windows.h>
#include "../Header_Files/GameStateMachine.h"
#include "../Console/Console.h"
#include "../Header_Files/Globals.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/Render.h"

float ChangestateTimer = 0.0f;
float LogoAnimationTimer = 0.0f;
int LogoFrameCounter = 0;
float DataAnimationTimer = 0.0f;
int DataFrameCounter = 0;

void Credits_Render()
{
	switch (LogoFrameCounter)
	{
	case 0: Render_CreditsLogo_Frame0();
		break;
	case 1: Render_CreditsLogo_Frame1();
		break;
	case 2: Render_CreditsLogo_Frame2();
		break;
	case 3: Render_CreditsLogo_Frame3();
		break;
	case 4: Render_CreditsLogo_Frame4();
		break;
	case 5: Render_CreditsLogo_Frame5();
		break;
	case 6: Render_CreditsLogo_Frame6();
		break;
	case 7: Render_CreditsLogo_Frame7();
		break;
	case 8: Render_CreditsLogo_Frame8();
		break;
	case 9: Render_CreditsLogo_Frame9();
		break;
	case 10: Render_CreditsLogo_Frame10();
		break;
	case 11: Render_CreditsLogo_Frame11();
		break;
	case 12: Render_CreditsLogo_Frame12();
		break;
	case 13: Render_CreditsLogo_Frame13();
		break;
	case 14: Render_CreditsLogo_Frame14();
		break;
	case 15: Render_CreditsLogo_Frame15();
		break;
	case 16: Render_CreditsLogo_Frame15();
		break;
	case 17: Render_CreditsLogo_Frame14();
		break;
	case 18: Render_CreditsLogo_Frame13();
		break;
	case 19: Render_CreditsLogo_Frame12();
		break;
	case 20: Render_CreditsLogo_Frame11();
		break;
	case 21: Render_CreditsLogo_Frame10();
		break;
	case 22: Render_CreditsLogo_Frame9();
		break;
	case 23: Render_CreditsLogo_Frame8();
		break;
	case 24: Render_CreditsLogo_Frame7();
		break;
	case 25: Render_CreditsLogo_Frame6();
		break;
	case 26: Render_CreditsLogo_Frame5();
		break;
	case 27: Render_CreditsLogo_Frame4();
		break;
	case 28: Render_CreditsLogo_Frame3();
		break;
	case 29: Render_CreditsLogo_Frame2();
		break;
	case 30: Render_CreditsLogo_Frame1();
		break;
	case 31: Render_CreditsLogo_Frame0();
		break;
	}

	switch (DataFrameCounter)
	{
	case 0: Render_CreditsData_Frame0();
		break;
	case 1: Render_CreditsData_Frame1();
		break;
	case 2: Render_CreditsData_Frame2();
		break;
	case 3: Render_CreditsData_Frame3();
		break;
	case 4: Render_CreditsData_Frame4();
		break;
	case 5: Render_CreditsData_Frame5();
		break;
	case 6: Render_CreditsData_Frame6();
		break;
	case 7: Render_CreditsData_Frame7();
		break;
	case 8: Render_CreditsData_Frame8();
		break;
	case 9: Render_CreditsData_Frame9();
		break;
	case 10: Render_CreditsData_Frame10();
		break;
	case 11: Render_CreditsData_Frame11();
		break;
	case 12: Render_CreditsData_Frame12();
		break;
	case 13: Render_CreditsData_Frame13();
		break;
	case 14: Render_CreditsData_Frame14();
		break;
	case 15: Render_CreditsData_Frame15();
		break;
	case 16: Render_CreditsData_Frame16();
		break;
	case 17: Render_CreditsData_Frame17();
		break;
	case 18: Render_CreditsData_Frame18();
		break;
	case 19: Render_CreditsData_Frame19();
		break;
	case 20: Render_CreditsData_Frame20();
		break;
	case 21: Render_CreditsData_Frame21();
		break;
	case 22: Render_CreditsData_Frame22();
		break;
	case 23: Render_CreditsData_Frame23();
		break;
	case 24: Render_CreditsData_Frame24();
		break;
	case 25: Render_CreditsData_Frame25();
		break;
	case 26: Render_CreditsData_Frame26();
		break;
	case 27: Render_CreditsData_Frame27();
		break;
	case 28: Render_CreditsData_Frame28();
		break;
	case 29: Render_CreditsData_Frame29();
		break;
	case 30: Render_CreditsData_Frame30();
		break;
	case 31: Render_CreditsData_Frame31();
		break;
	case 32: Render_CreditsData_Frame32();
		break;
	case 33: Render_CreditsData_Frame33();
		break;
	case 34: Render_CreditsData_Frame34();
		break;
	case 35: Render_CreditsData_Frame35();
		break;
	case 36: Render_CreditsData_Frame36();
		break;
	case 37: Render_CreditsData_Frame37();
		break;
	case 38: Render_CreditsData_Frame38();
		break;
	case 39: Render_CreditsData_Frame39();
		break;
	case 40: Render_CreditsData_Frame40();
		break;

	default: break;

	}


}

void Credits_Update()
{
	if (CheckTimerReached(&ChangestateTimer, 200.0f) && (GetAsyncKeyState(VK_RETURN) || GetAsyncKeyState(VK_SPACE) || GetAsyncKeyState(VK_ESCAPE)))
	{
		StateMachine_ChangeState(State_MainMenu);
	}

	if (CheckTimerReached(&LogoAnimationTimer, 600.0f))
	{
		LogoFrameCounter++;
		if (LogoFrameCounter > 31)
		{
			LogoFrameCounter = 0;
		}
	}


	if (CheckTimerReached(&DataAnimationTimer, 250.0f))
	{ 
		DataFrameCounter++;
		if (DataFrameCounter > 40)
		{
			DataFrameCounter = 0;
		}
	}

}

void Credits_ExitState()
{

}
