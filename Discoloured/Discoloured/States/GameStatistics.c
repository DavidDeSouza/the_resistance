#include <stdio.h>
#include "../Header_Files/GameStateMachine.h"
#include "../Header_Files/Globals.h"
#include "../Console/Console.h"
#include "../Header_Files/AI.h"
#include "../Libraries/GameTime.h"
#include "../Header_Files/Score.h"
#include "../Header_Files/Map.h"
#include "../Clock/Clock.h"
#include "../Header_Files/RenderBar.h"
#include "../Header_Files/Audio.h"

/***************************************************************************************
									Local Variables
***************************************************************************************/

/* Render Bar variables */
RenderBar teamA_renderBar, teamB_renderBar;

/* Display variables */
typedef enum
{
	DISPLAY_GAMEOVER,
	DISPLAY_CALCULATION,
	DISPLAY_WINNER

} DisplayState;
DisplayState g_displayState;

float g_displayDeltaTime;
const float g_displayDuration = 2.5f;

/* Score variables */
Score teamA_score, teamB_score;
float g_totalPixelsPossibleToColor;
float g_lowerPercentage, g_higherPercentage;
int g_step;

/* Audio Variables */
int g_playSFX;

/***************************************************************************************
								 Encapsulated Functions
***************************************************************************************/

/*
	brief	 Handles the render of Game Over words
*/
void RenderGameOverText()
{
	sprintf_s(stringBuffer, sizeof(stringBuffer), "Game Over");
	Console_SetRenderBuffer_Colour_String(WINDOWSIZEX / 2 - 5, MAPSTARTPOSY - 1, stringBuffer, FOREGROUND_RED_ | BACKGROUND_WHITE_);
}

/*
	brief	 Handles what to update according to the current display state

	param    _displayState - the current display state
*/
void Update_DisplayState(const DisplayState _displayState)
{
	switch (_displayState)
	{
			/* First display: Show Game Over on screen */
		case DISPLAY_GAMEOVER:
			if (CheckTimerReached(&g_displayDeltaTime, ConvertFromSeconds(g_displayDuration)))
			{
				g_displayDeltaTime = 0.0f;
				++g_displayState;
			}
			break;

			/* Second display: Show the calculation of the score */
		case DISPLAY_CALCULATION:
			// Play audio once
			if (!g_playSFX)
			{
				PlayAudio(audio.drumRoll);
				g_playSFX = 1;
			}

			switch (g_step)
			{
				case 0:
					// Increase each score by the amount till timing reached
					if (!CheckTimerReached(&g_displayDeltaTime, ConvertFromSeconds(g_displayDuration)))
					{
						// Calculate the amount to increase for the duration to get to lower percentage
						float increasePercentage = g_lowerPercentage / ConvertFromSeconds(g_displayDuration);

						if (teamA_score.percentageCovered < g_lowerPercentage)
							teamA_score.percentageCovered += increasePercentage * Clock_GetDeltaTime(); // * dt - to get %
						if (teamB_score.percentageCovered < g_lowerPercentage)
							teamB_score.percentageCovered += increasePercentage * Clock_GetDeltaTime();

						Updating_RenderBar(&teamA_renderBar, teamA_score, g_lowerPercentage);
						Updating_RenderBar(&teamB_renderBar, teamB_score, g_lowerPercentage);
					}
					else
						++g_step;

					break;
				case 1:
					/* Wait for a few seconds before changing */
					if (!CheckTimerReached(&g_displayDeltaTime, ConvertFromSeconds(g_displayDuration)))
					{
						teamA_score.percentageCovered = g_lowerPercentage;
						teamB_score.percentageCovered = g_lowerPercentage;
					}
					else
						++g_step;

					break;
				case 2:
					// Increase each score by the amount till timing reached
					if (!CheckTimerReached(&g_displayDeltaTime, ConvertFromSeconds(g_displayDuration)))
					{
						// Calculate the amount to increase for the duration to get to the winner score
						float increasePercentage;

						if (GetCurrentWinner().color == teamA.color)
						{
							increasePercentage = (g_higherPercentage - teamA_score.percentageCovered) / ConvertFromSeconds(g_displayDuration);
							teamA_score.percentageCovered += increasePercentage * Clock_GetDeltaTime();
							Updating_RenderBar(&teamA_renderBar, teamA_score, g_higherPercentage);
						}
						else
						{
							increasePercentage = (g_higherPercentage - teamB_score.percentageCovered) / ConvertFromSeconds(g_displayDuration);
							teamB_score.percentageCovered += increasePercentage * Clock_GetDeltaTime();
							Updating_RenderBar(&teamB_renderBar, teamB_score, g_higherPercentage);
						}
					}
					else
						++g_step;

					break;
				default:
					++g_displayState;
					break;
			}
			break;

			/* Third display: Show the winner for the match */
		case DISPLAY_WINNER:
			/* When calculation is done, display winner for a while before moving to next screen */
			if (CheckTimerReached(&g_displayDeltaTime, ConvertFromSeconds(g_displayDuration)))
			{
				ResetScore(&teamA_score);
				ResetScore(&teamB_score);

				StateMachine_ChangeState(State_GameOver);
			}
			break;
		default:
			break;
	}
}

/*
	brief	 Handles what to render according to the current display state

	param    _displayState - the current display state
*/
void Render_DisplayState(const DisplayState _displayState)
{
	switch (_displayState)
	{
		case DISPLAY_GAMEOVER: // Show a game over on screen
			RenderMapColour();
			RenderGameOverText();
			break;

		case DISPLAY_WINNER:
		case DISPLAY_CALCULATION:
			Rendering_RenderBar(teamA_renderBar);
			Rendering_RenderBar(teamB_renderBar);

			sprintf_s(stringBuffer, sizeof(stringBuffer), "Team A");
			Console_SetRenderBuffer_String(teamA_renderBar.currPoint.x, teamA_renderBar.currPoint.y - 2, stringBuffer);

			sprintf_s(stringBuffer, sizeof(stringBuffer), "Team B");
			Console_SetRenderBuffer_String(teamB_renderBar.currPoint.x, teamB_renderBar.currPoint.y - 2, stringBuffer);

			sprintf_s(stringBuffer, sizeof(stringBuffer), "%0.2f%%", teamA_score.percentageCovered);
			Console_SetRenderBuffer_String(teamA_renderBar.currPoint.x, teamA_renderBar.currPoint.y - 1, stringBuffer);

			sprintf_s(stringBuffer, sizeof(stringBuffer), "%0.2f%%", teamB_score.percentageCovered);
			Console_SetRenderBuffer_String(teamB_renderBar.currPoint.x, teamB_renderBar.currPoint.y - 1, stringBuffer);
			break;
	}
}

/***************************************************************************************
									   Functions
***************************************************************************************/

void GameStatistics_ProcessInput()
{
	if (GetAsyncKeyState(VK_RETURN)) {}
}

void GameStatistics_Update()
{
	Update_DisplayState(g_displayState);
}

void GameStatistics_Render()
{
	RenderBackGroundWhite();
	Render_DisplayState(g_displayState);
}

void GameStatistics_EnterState()
{
	g_displayState = DISPLAY_GAMEOVER;
	g_displayDeltaTime = 0.0f;
	g_totalPixelsPossibleToColor = 0.0f;
	g_lowerPercentage = 0.0f;
	g_step = 0;
	g_playSFX = 0;

	/* Copy map data to storeMap and determine area that can be colored */
	for (int i = MAPSTARTPOSX + 1; i < MAPSIZEX - 1; ++i)
	{
		for (int j = MAPSTARTPOSY + 1; j < MAPSIZEY - 1; ++j)
		{
			if (map[i][j].c != 'W')
				++g_totalPixelsPossibleToColor;
		}
	}

	/* Initialise the score to be displayed and team for each score */
	InitialiseScore(&teamA_score, teamA, g_totalPixelsPossibleToColor);
	InitialiseScore(&teamB_score, teamB, g_totalPixelsPossibleToColor);

	/* Initialise Render Bars for the teams */
	InitialiseRenderBar(&teamA_renderBar, 20, MAPSIZEY - 3, 30, MAPSTARTPOSY + 3, teamA.color);
	InitialiseRenderBar(&teamB_renderBar, WINDOWSIZEX - 37, MAPSIZEY - 3, WINDOWSIZEX - 27, MAPSTARTPOSY + 3, teamB.color);

	CalculateTotalColorPercentage(&teamA_score, MAPSIZEX, MAPSIZEY);
	CalculateTotalColorPercentage(&teamB_score, MAPSIZEX, MAPSIZEY);

	/* Determine winner */
	if (teamA_score.percentageCovered < teamB_score.percentageCovered)
	{
		g_lowerPercentage = teamA_score.percentageCovered;
		g_higherPercentage = teamB_score.percentageCovered;
		SetCurrentWinner(teamB_score);
	}
	else
	{
		g_lowerPercentage = teamB_score.percentageCovered;
		g_higherPercentage = teamA_score.percentageCovered;
		SetCurrentWinner(teamA_score);
	}

	/* Initialise the score to be displayed and team for each score again */
	InitialiseScore(&teamA_score, teamA, g_totalPixelsPossibleToColor);
	InitialiseScore(&teamB_score, teamB, g_totalPixelsPossibleToColor);
}

void GameStatistics_ExitState()
{

}