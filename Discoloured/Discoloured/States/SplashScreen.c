
#include "../Header_Files/SplashScreen.h"
#include "../Libraries/GameTime.h"
#include <Windows.h>
#include "../Header_Files/GameStateMachine.h"
#include "../Console/Console.h"
#include "../Header_Files/Globals.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/Audio.h"
#include "../Header_Files/Render.h"

float DeltaTime_SplashAnimation = 0.0f;      /* millisec difference per frame*/
int FrameCounter = 0;                        /* counter that calls frames */



void SplashScreen_Render()
{
	if (CheckTimerReached(&DeltaTime_SplashAnimation, 85.0f))
	{
		FrameCounter++;   /* adds one everytime specified time is called */
	}

	switch (FrameCounter)  /* calls frames according to counter */
	{	
		case 1: DigipenLogoFrame_1();
			break;
		case 2: DigipenLogoFrame_2();
			break;
		case 3: DigipenLogoFrame_3();
			break;
		case 4: DigipenLogoFrame_4();
			break;
		case 5: DigipenLogoFrame_5();
			break;
		case 6: DigipenLogoFrame_6();
			break;
		case 7: DigipenLogoFrame_7();
			break;
		case 8: DigipenLogoFrame_8();
			break;
		case 9: DigipenLogoFrame_9();
			break;
		case 10: DigipenLogoFrame_10();
			break;
		case 11: DigipenLogoFrame_11();
			break;
		case 12: DigipenLogoFrame_12();
			break;
		case 13: DigipenLogoFrame_13();
			break;
		case 14: DigipenLogoFrame_14();
			break;
		case 15: DigipenLogoFrame_15();
			break;
		case 16: DigipenLogoFrame_16();
			break;
		case 17: DigipenLogoFrame_17();
			break;
		case 18: DigipenLogoFrame_18();
			break;
		case 19: DigipenLogoFrame_19();
			break;
		case 20: DigipenLogoFrame_20();
			break;
		case 21: DigipenLogoFrame_21();
			break;
		case 22: DigipenLogoFrame_22();
			break;
		case 23: DigipenLogoFrame_23();
			break;
		case 24: DigipenLogoFrame_22();
			break;
		case 25: DigipenLogoFrame_25();
			break;
		case 26: DigipenLogoFrame_26();
			break;
		case 27: DigipenLogoFrame_27();
			break;
		case 28: DigipenLogoFrame_28();
			break;
		case 29: DigipenLogoFrame_29();
			break;
		case 30: DigipenLogoFrame_30();
			break;
		case 31: DigipenLogoFrame_31();
			break;
		case 32: DigipenLogoFrame_32();
			break;
		case 33: DigipenLogoFrame_33();
			break;
		case 34: DigipenLogoFrame_34();
			break;
		case 35: DigipenLogoFrame_35();
			break;
		case 36: DigipenLogoFrame_36();
			break;
		case 37: DigipenLogoFrame_37();
			break;
		case 38: DigipenLogoFrame_38();
			break;
		case 39: DigipenLogoFrame_39();
			break;
		case 40: DigipenLogoFrame_40();
			break;

	}
		


}


void SplashScreen_Update()
{
	if (FrameCounter > 40)
	{
		StateMachine_ChangeState(State_MainMenu);
	}
}



void SplashScreen_ExitState()
{
	DigipenLogoFrame_40(); /* added the frame here, else, console flickers to black */
	PlayAudio(audio.menuBgm);
}

