#define _CRT_SECURE_NO_WARNINGS
#include "../Header_Files/PlayerControlOptions.h"
#include <Windows.h>
#include "../Header_Files/GameStateMachine.h"
#include "../Header_Files/HandleController.h"
#include <stdio.h>
#include "../Header_Files/Globals.h"
#include "../Header_Files/HandleController.h"
#include "../Console/Console.h"
#include "../Header_Files/Player.h"
#include "../Header_Files/Render.h"
#include "../Libraries/GameTime.h"
#include <SDL.h>
#include "../Header_Files/Audio.h"

int numberOfControllers = 0;
int countDown = 5;
float countdownDeltaTime = 0.0f;
char countdownStringBuffer[40];

float currentTimeElapsed = 0.0f;


void InitializeSideAndColor()
{
	for (int i = 0; i < MAXPLAYER; i++)
	{
		side[i] = A;
		BackGroundColor[i] = BACKGROUND_ORANGE;
	}
	ForeGroundColor[0] = FOREGROUND_PURPLE;
	ForeGroundColor[1] = FOREGROUND_BLUE_;
	ForeGroundColor[2] = FOREGROUND_GREEN_;
	ForeGroundColor[3] = FOREGROUND_RED_;
}
void Controls_ProcessInput()
{
	if (GetAsyncKeyState(VK_ESCAPE) & 1)
	{
		StateMachine_ChangeState(State_MainMenu);
	}
	if (GetAsyncKeyState(VK_RETURN) & 1)
	{
		if (player1.isReady == Ready_True)
		{
			player1.isReady = Ready_False;
		}
		else
		{
			player1.isReady = Ready_True;
		}
	}
	if (GetAsyncKeyState(VK_SPACE) & 1)
	{
		numberOfControllers = InitialiseControllers();
		InitializeSideAndColor();

		strncpy(player1String, "       Player 1      >", sizeof(player1String) / sizeof(player1String[0]));
		if (numberOfControllers == 0)
		{
			strncpy(player2String, "Player 2 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player3String, "Player 3 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player4String, "Player 4 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
		}
		if (numberOfControllers == 1)
		{
			strncpy(player2String, "       Player 2      >", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player3String, "Player 3 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player4String, "Player 4 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
		}
		else if (numberOfControllers == 2)
		{
			strncpy(player2String, "       Player 2      >", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player3String, "       Player 3      >", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player4String, "Player 4 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
		}
		else if (numberOfControllers == 3)
		{
			strncpy(player2String, "       Player 2      >", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player3String, "       Player 3      >", sizeof(player1String) / sizeof(player1String[0]));
			strncpy(player4String, "       Player 4      >", sizeof(player1String) / sizeof(player1String[0]));
		}
	}
	if (player1.isReady == Ready_False)
	{
		if (GetAsyncKeyState(VK_RIGHT) & 1)
		{
			if (side[0] == A)
			{
				side[0] = B;
				ForeGroundColor[0] = FOREGROUND_PURPLE;
				BackGroundColor[0] = BACKGROUND_TEAL;
				strncpy(player1String, "<      Player 1       ", sizeof(player1String) / sizeof(player1String[0]));
			}
		}
		if (GetAsyncKeyState(VK_LEFT) & 1)
		{
			if (side[0] == B)
			{
				side[0] = A;
				ForeGroundColor[0] = FOREGROUND_PURPLE;
				BackGroundColor[0] = BACKGROUND_ORANGE;
				strncpy(player1String, "       Player 1      >", sizeof(player1String) / sizeof(player1String[0]));
			}
		}
	}
	/* Empty the input buffer incase the input is used for player movement */
	/* Quick fix */
	if (GetAsyncKeyState(VK_W)) { }
	if (GetAsyncKeyState(VK_A)) { }
	if (GetAsyncKeyState(VK_S)) { }
	if (GetAsyncKeyState(VK_D)) { }
}

void Controls_Update()
{
	//checking if all players are ready
	if (numberOfControllers == 0)
	{
		if (player1.isReady == Ready_True)
		{
			if (CheckTimerReached(&countdownDeltaTime, 1000.0f))
			{
				sprintf_s(countdownStringBuffer, sizeof(countdownStringBuffer) / sizeof(countdownStringBuffer[0]),
					"       Game Starting in %d...      ", countDown);
				countDown--;
			}
		}
		else
		{
			strcpy(countdownStringBuffer, "All Players Must Be Ready To Start");
			countDown = 5;
		}
	}
	if (numberOfControllers == 1)
	{
		if (player1.isReady == Ready_True && player2.isReady == Ready_True)
		{
			if (CheckTimerReached(&countdownDeltaTime, 1000.0f))
			{
				sprintf_s(countdownStringBuffer, sizeof(countdownStringBuffer) / sizeof(countdownStringBuffer[0]),
					"       Game Starting in %d...      ", countDown);
				countDown--;
			}
		}
		else
		{
			strcpy(countdownStringBuffer, "All Players Must Be Ready To Start");
			countDown = 5;
		}
	}
	if (numberOfControllers == 2)
	{
		if (player1.isReady == Ready_True && player2.isReady == Ready_True &&
			player3.isReady == Ready_True)
		{
			if (CheckTimerReached(&countdownDeltaTime, 1000.0f))
			{
				sprintf_s(countdownStringBuffer, sizeof(countdownStringBuffer) / sizeof(countdownStringBuffer[0]),
					"       Game Starting in %d...      ", countDown);
				countDown--;
			}
		}
		else
		{
			strcpy(countdownStringBuffer, "All Players Must Be Ready To Start");
			countDown = 5;
		}
	}
	if (numberOfControllers == 3)
	{
		if (player1.isReady == Ready_True && player2.isReady == Ready_True &&
			player3.isReady == Ready_True && player4.isReady == Ready_True)
		{
			if (CheckTimerReached(&countdownDeltaTime, 1000.0f))
			{
				sprintf_s(countdownStringBuffer, sizeof(countdownStringBuffer) / sizeof(countdownStringBuffer[0]),
					"       Game Starting in %d...      ", countDown);
				countDown--;
			}
		}
		else
		{
			strcpy(countdownStringBuffer, "All Players Must Be Ready To Start");
			countDown = 5;
		}
	}

	if (countDown < 0)
	{
		PauseAudio(channel.bgm);
		StateMachine_ChangeState(State_Pre_Game);
	}

	
	HandleAllControllerInputs();
}

void Controls_Render()
{
	RenderBackGroundWhite();
	RenderSelections();
	RenderPlayerTypes();
	Console_SetRenderBuffer_Colour_String(24, 36, "                                  ", BACKGROUND_TEAL);
	Console_SetRenderBuffer_Colour_String(24, 37, countdownStringBuffer, FOREGROUND_WHITE_ | BACKGROUND_TEAL);
	Console_SetRenderBuffer_Colour_String(24, 38, "                                  ", BACKGROUND_TEAL);
}

void Controls_EnterState()
{
	InitializeSideAndColor();

	InitializeTeam();
	player1.isReady = Ready_False;
	player2.isReady = Ready_False;
	player3.isReady = Ready_False;
	player4.isReady = Ready_False;

	strncpy(player1String, "       Player 1      >", sizeof(player1String) / sizeof(player1String[0]));
	strncpy(player2String, "Player 2 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
	strncpy(player3String, "Player 3 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
	strncpy(player4String, "Player 4 Not Connected", sizeof(player1String) / sizeof(player1String[0]));
}

void Controls_ExitState()
{
	
}

void RenderSelections()
{
	Console_SetRenderBuffer_Colour_String(30, 13, "                      ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
	Console_SetRenderBuffer_Colour_String(30, 14, "  Detect Controllers  ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
	Console_SetRenderBuffer_Colour_String(30, 15, "      <SPACEBAR>      ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
	Console_SetRenderBuffer_Colour_String(30, 16, "                      ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
	Console_SetRenderBuffer_Colour_String(30, 19, "                      ", BackGroundColor[0]);
	Console_SetRenderBuffer_Colour_String(30, 20, player1String, ForeGroundColor[0] | BackGroundColor[0]);
	Console_SetRenderBuffer_Colour_String(30, 21, "                      ", BackGroundColor[0]);
	Console_SetRenderBuffer_Colour_String(30, 22, "                      ", BackGroundColor[1]);
	Console_SetRenderBuffer_Colour_String(30, 23, player2String, ForeGroundColor[1] | BackGroundColor[1]);
	Console_SetRenderBuffer_Colour_String(30, 24, "                      ", BackGroundColor[1]);
	Console_SetRenderBuffer_Colour_String(30, 25, "                      ", BackGroundColor[2]);
	Console_SetRenderBuffer_Colour_String(30, 26, player3String, ForeGroundColor[2] | BackGroundColor[2]);
	Console_SetRenderBuffer_Colour_String(30, 27, "                      ", BackGroundColor[2]);
	Console_SetRenderBuffer_Colour_String(30, 28, "                      ", BackGroundColor[3]);
	Console_SetRenderBuffer_Colour_String(30, 29, player4String, ForeGroundColor[3] | BackGroundColor[3]);
	Console_SetRenderBuffer_Colour_String(30, 30, "                      ", BackGroundColor[3]);
	Console_SetRenderBuffer_Colour_String(30, 32, "   <ESC> to go back   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);

	if (player1.isReady == Ready_True)
	{
		Console_SetRenderBuffer_Colour_String(15, 20, "Ready", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	}
	else if (player1.isReady == Ready_False)
	{
		Console_SetRenderBuffer_Colour_String(15, 20, "EntertoReady", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}
	if (player2.isReady == Ready_True)
	{
		Console_SetRenderBuffer_Colour_String(15, 23, "Ready", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	}
	else if (player2.isReady == Ready_False)
	{
		Console_SetRenderBuffer_Colour_String(15, 23, "A/XtoReady", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}
	if (player3.isReady == Ready_True)
	{
		Console_SetRenderBuffer_Colour_String(15, 26, "Ready", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	}
	else if (player3.isReady == Ready_False)
	{
		Console_SetRenderBuffer_Colour_String(15, 26, "A/XtoReady", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}
	if (player4.isReady == Ready_True)
	{
		Console_SetRenderBuffer_Colour_String(15, 29, "Ready", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	}
	else if (player4.isReady == Ready_False)
	{
		Console_SetRenderBuffer_Colour_String(15, 29, "A/XtoReady", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}
}

void RenderPlayerTypes()
{
	Console_SetRenderBuffer_Colour_String(60, 20, "Keyboard", FOREGROUND_TEAL | BACKGROUND_WHITE_);

	if (numberOfControllers >= 1)
	{
		Console_SetRenderBuffer_Colour_String(60, 23, SDL_GameControllerName(joystick1), FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}
	if (numberOfControllers >= 2)
	{
		Console_SetRenderBuffer_Colour_String(60, 26, SDL_GameControllerName(joystick2), FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}
	if (numberOfControllers >= 3)
	{
		Console_SetRenderBuffer_Colour_String(60, 29, SDL_GameControllerName(joystick3), FOREGROUND_TEAL | BACKGROUND_WHITE_);
	}
}