#include "../Header_Files/Instructions.h"
#include "../Console/Console.h"
#include "../Header_Files/Render.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/GameStateMachine.h"
#include "../Header_Files/Globals.h"

void Instructions_ProcessInput()
{
	if (GetAsyncKeyState(VK_ESCAPE) & 1)
	{
		StateMachine_ChangeState(State_MainMenu);
	}
}

//========================================================================
//	author: David De Souza
//  Renders sample UI 
//========================================================================
void RenderUIExample()
{
	Console_SetRenderBuffer_Colour_String(56, 1, "100%            Ultimate", BACKGROUND_TEAL | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(1, 1, "Ultimate            100%", BACKGROUND_RED_ | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(1, 2, "Team A Kills : 23", FOREGROUND_BLACK | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(64, 2, "Team B Kills: 34", FOREGROUND_BLACK | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(12, 3, "|                          |                           |", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(12, 4, "|                [Game countdown timer]                |", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(12, 5, "|                                                      |", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(12, 6, "|______[    get kills to charge up ultimate   ]________|", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(12, 7, "       [Kills all enemies in a 30 pixel radius]         ", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
}

void Instructions_Render()
{
	RenderBackGroundWhite();
	RenderControlsGraphic();
	RenderUIExample();
	Console_SetRenderBuffer_Colour_String(WINDOWSIZEX / 2 - 7, MAPSTARTPOSY - 1, "0 mins 00 seconds", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5,40, "The aim of the game is to paint                 Players will respawn ", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(5,41, "as much of the map as possible                  after 2 seconds from ", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
	Console_SetRenderBuffer_Colour_String(5,42, "with your team's colours                        getting killed", BACKGROUND_WHITE_ | FOREGROUND_BLACK);
}