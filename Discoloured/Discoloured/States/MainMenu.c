﻿#include "../Header_Files/MainMenu.h"
#include "../Libraries/GameTime.h"
#include <Windows.h>
#include "../Header_Files/GameStateMachine.h"
#include "../Console/Console.h"
#include "../Header_Files/Globals.h"
#include "../Header_Files/PlayerControlOptions.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/HandleController.h"
#include "../Header_Files/Audio.h"

// play=0, instructions=1, exit=2
int menu = 0;

void MainMenu_ProcessInput()
{
	if (GetAsyncKeyState(VK_UP) & 1)
	{
		if (menu == 0)
		{
			menu = 3;
		}
		else
		{
			menu--;
		}
		PlayAudio(audio.shot);
	}
	if (GetAsyncKeyState(VK_DOWN) & 1)
	{
		if (menu == 3)
		{
			menu = 0;
		}
		else
		{
			menu++;
		}
		PlayAudio(audio.shot);
	}

	if (GetAsyncKeyState(VK_RETURN) & 1)
	{
		if (menu == 0)
		{
			if (numberOfControllers)
			{
				UnInitialiseControllers();
			}
			StateMachine_ChangeState(State_Team_Options);
		}
		else if (menu == 1)
		{
			StateMachine_ChangeState(State_Instructions);
		}
		else if (menu == 2)
		{
			{
				StateMachine_ChangeState(State_Credits);
			}
		}
		else if (menu == 3)
		{

			if (numberOfControllers)
			{
				UnInitialiseControllers();
			}
			Console_CleanUp();
			CloseAudio();
			bGameIsRunning = 0;
		}
	}

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		/* do nothing = clears buffer if user spams escape key */
	}
}

void MainMenu_Update()
{
}

void MainMenu_Render()
{
	RenderBackGroundWhite();

	switch (menu)
	{
	case 0:
		Console_SetRenderBuffer_Colour_String(1, 18, "8888888b. d8b        ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 19, "888  'Y88bY8P        ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 20, "888    888           ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 21, "888    888888.d8888b ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 22, "888    88888888K     ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 23, "888    888888'Y8888b.", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 24, "888  .d88P888     X88", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 25, "8888888P' 888 88888P'", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

		Console_SetRenderBuffer_Colour_String(22, 18, " .d8888b.         888                                  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 19, "d88P  Y88b        888                                  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 20, "888    888        888                                  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 21, "888        .d88b. 888.d88b. 888  88.88d888 .d88b.  .d88888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 22, "888       d88''88b88888''88b888  88.88P'  d8P  Y8bd88' 888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 23, "888    888888  88888888  888888  88.88    88888888888  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 24, "Y88b  d88PY88..88P88888..88PY88b 88.88    Y8b.    Y88b 888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 25, " 'Y8888P'  'Y88P' 888'Y88P'  'Y8888.88     'Y8888  'Y88888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

		Console_SetRenderBuffer_Colour_String(30, 27, "     Play Game     ", FOREGROUND_BLUE | BACKGROUND_ORANGE);
		Console_SetRenderBuffer_Colour_String(30, 28, "    How To Play    ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 29, "      Credits      ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 30, "     Quit Game     ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);

		break;

	case 1:
		Console_SetRenderBuffer_Colour_String(1, 18, "8888888b. d8b        ", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 19, "888  'Y88bY8P        ", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 20, "888    888           ", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 21, "888    888888.d8888b ", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 22, "888    88888888K     ", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 23, "888    888888'Y8888b.", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 24, "888  .d88P888     X88", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 25, "8888888P' 888 88888P'", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

		Console_SetRenderBuffer_Colour_String(22, 18, " .d8888b.         888                                  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 19, "d88P  Y88b        888                                  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 20, "888    888        888                                  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 21, "888        .d88b. 888.d88b. 888  88.88d888 .d88b.  .d88888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 22, "888       d88''88b88888''88b888  88.88P'  d8P  Y8bd88' 888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 23, "888    888888  88888888  888888  88.88    88888888888  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 24, "Y88b  d88PY88..88P88888..88PY88b 88.88    Y8b.    Y88b 888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(22, 25, " 'Y8888P'  'Y88P' 888'Y88P'  'Y8888.88     'Y8888  'Y88888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

		Console_SetRenderBuffer_Colour_String(30, 27, "     Play Game     ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 28, "    How To Play    ", FOREGROUND_BLUE | BACKGROUND_ORANGE);
		Console_SetRenderBuffer_Colour_String(30, 29, "      Credits      ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 30, "     Quit Game     ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);

		break;

	case 2:
		Console_SetRenderBuffer_Colour_String(1, 18, "8888888b. d8b         .d8888b.         888                                  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 19, "888  'Y88bY8P        d88P  Y88b        888                                  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 20, "888    888           888    888        888                                  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 21, "888    888888.d8888b 888        .d88b. 888.d88b. 888  88.88d888 .d88b.  .d88888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 22, "888    88888888K     888       d88''88b88888''88b888  88.88P'  d8P  Y8bd88' 888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 23, "888    888888'Y8888b.888    888888  88888888  888888  88.88    88888888888  888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 24, "888  .d88P888     X88Y88b  d88PY88..88P88888..88PY88b 88.88    Y8b.    Y88b 888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 25, "8888888P' 888 88888P' 'Y8888P'  'Y88P' 888'Y88P'  'Y8888.88     'Y8888  'Y88888", FOREGROUND_LIGHTTEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

		Console_SetRenderBuffer_Colour_String(30, 27, "     Play Game     ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 28, "    How To Play    ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 29, "      Credits      ", FOREGROUND_BLUE | BACKGROUND_ORANGE);
		Console_SetRenderBuffer_Colour_String(30, 30, "     Quit Game     ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		break;


	case 3:
		Console_SetRenderBuffer_Colour_String(1, 18, "8888888b. d8b         .d8888b.         888                                  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 19, "888  'Y88bY8P        d88P  Y88b        888                                  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 20, "888    888           888    888        888                                  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 21, "888    888888.d8888b 888        .d88b. 888.d88b. 888  88.88d888 .d88b.  .d88888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 22, "888    88888888K     888       d88''88b88888''88b888  88.88P'  d8P  Y8bd88' 888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 23, "888    888888'Y8888b.888    888888  88888888  888888  88.88    88888888888  888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 24, "888  .d88P888     X88Y88b  d88PY88..88P88888..88PY88b 88.88    Y8b.    Y88b 888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(1, 25, "8888888P' 888 88888P' 'Y8888P'  'Y88P' 888'Y88P'  'Y8888.88     'Y8888  'Y88888", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

		Console_SetRenderBuffer_Colour_String(30, 27, "     Play Game     ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 28, "    How To Play    ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 29, "      Credits      ", FOREGROUND_WHITE_ | BACKGROUND_TEAL);
		Console_SetRenderBuffer_Colour_String(30, 30, "     Quit Game     ", FOREGROUND_BLUE | BACKGROUND_ORANGE);

	default:
		break;
	}
}

void MainMenu_EnterState()
{
	if (numberOfControllers)
	{
		UnInitialiseControllers();
	}
}

void MainMenu_ExitState()
{
}