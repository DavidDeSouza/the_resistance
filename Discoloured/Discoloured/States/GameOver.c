#include <Windows.h>
#include "../Header_Files/Render.h"
#include "../Header_Files/MainMenu.h"
#include "../Header_Files/GameStateMachine.h"
#include "../Console/Console.h"
#include "../Header_Files/Globals.h"
#include "../Header_Files/AI.h"
#include "../Libraries/GameTime.h"
#include "../Header_Files/Score.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/Audio.h"

void GameOver_ProcessInput()
{
	if (GetAsyncKeyState(VK_RETURN) & 1)
		StateMachine_ChangeState(State_MainMenu);
}

void GameOver_Update()
{
	
}

void GameOver_Render()
{
	RenderBackGroundWhite();

	Console_SetRenderBuffer_Colour_String(2, 17, "oooooo   oooooo     oooo  o8o                                             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 18, " `888.    `888.     .8'   `''                                             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 19, "  `888.   .8888.   .8'   oooo  ooo. .oo.   ooo. .oo.    .ooooo.  oooo d8b ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 20, "   `888  .8'`888. .8'    `888  `888P'Y88b  `888P'Y88b  d88' `88b `888''8P ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "    `888.8'  `888.8'      888   888   88U   ARE   AWE  SOooooOME  888     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "     `888'    `888'       888   888   888   888   888  888    .o  888     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "      `8'      `8'       o888o o888o o888o o888o o888o `Y8bod8P' d888b    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);

	if (GetCurrentWinner().color == teamA.color)
	{
		RenderTeamAWinner();
	}
	else if (GetCurrentWinner().color == teamB.color)
	{
		RenderTeamBWinner();
	}

	Console_SetRenderBuffer_Colour_String(WINDOWSIZEX / 2 - 9, WINDOWSIZEY / 2 + 13, "                   ", FOREGROUND_BLACK | BACKGROUND_ORANGE);
	Console_SetRenderBuffer_Colour_String(WINDOWSIZEX / 2 - 9, WINDOWSIZEY / 2 + 13, "  < Press Enter >  ", FOREGROUND_BLACK | BACKGROUND_ORANGE);
	Console_SetRenderBuffer_Colour_String(WINDOWSIZEX / 2 - 9, WINDOWSIZEY / 2 + 14, "     Main Menu     ", FOREGROUND_BLACK | BACKGROUND_ORANGE);
	Console_SetRenderBuffer_Colour_String(WINDOWSIZEX / 2 - 9, WINDOWSIZEY / 2 + 15, "                   ", FOREGROUND_BLACK | BACKGROUND_ORANGE);
}

void GameOver_EnterState()
{
}

void GameOver_ExitState()
{
	PlayAudio(audio.menuBgm);
}