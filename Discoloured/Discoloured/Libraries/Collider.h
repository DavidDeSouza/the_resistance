/**********************************************************************************
* \file			Collider.h
* \brief		Contains collider structure and some helper functions
* \author		Jolyn Wong
* \date			2019
*
*	This script stores the structure BoxCollider, which is used to create box
*	collider objects. The box colliders determine the area of collision available
*   for the object it is attached to. This sript also contains functions to create
*   and render the colliders of objects.
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _COLLIDER_H_
#define _COLLIDER_H_

#include "Point.h"

/* Structure containing data relating to a box collider */
typedef struct
{
	Point minPoint;
	Point maxPoint;
	Point midPoint;

	int color;
	char c;

} BoxCollider;

/*
	author  Jolyn Wong

	brief	Creates a temporary box collider object and initialises it

	param	_minPoint - the minimum point of the box
	        _maxPoint - the maximum point of the box

	Output	returns a box collider object that is to be assigned
*/
BoxCollider CreateBoxCollider(Point _midPoint, Point _maxPoint);

/*
	author  Jolyn Wong

	brief	Renders the box collider to see where it is, mainly for debugging

	param	_collider - the collider to render
*/
void RenderCollider(BoxCollider _collider);

#endif