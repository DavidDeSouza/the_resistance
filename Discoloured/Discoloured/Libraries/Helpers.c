/**********************************************************************************
* \file			Helpers.c
* \brief		Helpers library which does extra math stuff
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* Helpers library which does extra math stuff

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#include "Point.h"
#include <math.h>

double GetDistanceBetweenPoints(Point a, Point b)
{
	int deltaX = a.x - b.x;
	int deltaY = a.y - b.y;
	return sqrt((double)deltaX*(double)deltaX + (double)deltaY*(double)deltaY);
}

int GetDistance(int a, int b)
{
	return abs(a - b);
}