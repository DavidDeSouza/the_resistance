/**********************************************************************************
* \file			Draw.h
* \brief		Drawing library which draws a rectangle
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* Drawing library which draws a rectangle

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#include "../Console/Console.h"

void DrawFilledRect(int startPosX, int startPosY, 
	int endPosX, int endPosY,char c)
{
	for  (int x = startPosX; x < endPosX; x++)
	{
		for (int y = startPosY; y < endPosY; y++)
		{
			Console_SetRenderBuffer_Char(x, y, c);
		}
	}
}

void DrawEmptyRect(int startPosX, int startPosY,
	int endPosX, int endPosY, char c)
{
	for (int x = startPosX; x <= endPosX; ++x)
	{
		for (int y = startPosY; y <= endPosY; ++y)
		{
			if (x == startPosX || y == startPosY || x == endPosX || y == endPosY)
			{
				Console_SetRenderBuffer_Char(x, y, c);
			}
		}
	}
}
void DrawEmptyRectWithColor(int startPosX, int startPosY,
	int endPosX, int endPosY, char c, int color)
{
	for (int x = startPosX; x <= endPosX; ++x)
	{
		for (int y = startPosY; y <= endPosY; ++y)
		{
			if (x == startPosX || y == startPosY || x == endPosX || y == endPosY)
			{				
				Console_SetRenderBuffer_Colour_Char(x, y, c, color);
			}
		}
	}
}
void DrawFilledRectWithColor(int startPosX, int startPosY,
	int endPosX, int endPosY, char c,int color)
{
	for (int x = startPosX; x < endPosX; x++)
	{
		for (int y = startPosY; y < endPosY; y++)
		{
			Console_SetRenderBuffer_Colour_Char(x, y, c,color);
		}
	}
}
