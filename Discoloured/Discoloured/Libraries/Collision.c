/**********************************************************************************
* \file			Collision.h
* \brief		Contains collision detection functions
* \author		Seow Jun Hao Darren
* \version		1.2
* \date			2019
*
* a library that does different type of collision detection

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.2 - Added CheckCheckCollisionAheadBoxCollider and
					  CheckCollisionAheadBoxColliderWithPoint
				1.1 - Added CheckPointToBoxCollider and CheckAABB by Jolyn
				1.0 - Initial commit
**********************************************************************************/
#include "Collision.h"
#include "../Header_Files/Globals.h"

int CheckCollision(Point currPosition, Point targetPosition)
{
	if (currPosition.x == targetPosition.x&&currPosition.y == targetPosition.y)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int CheckCollisionAhead(Point currPosition,Direction currDirection, Point targetPosition)
{
	int deltaPositionX,deltaPositionY;
	switch (currDirection)
	{
	case LEFT:
		deltaPositionX = currPosition.x - 1;
		deltaPositionY = currPosition.y;
		break;
	case RIGHT:
		deltaPositionX = currPosition.x + 1;
		deltaPositionY = currPosition.y;
		break;
	case UP:
		deltaPositionX = currPosition.x;
		deltaPositionY = currPosition.y - 1;
		break;
	case DOWN:
		deltaPositionX = currPosition.x;
		deltaPositionY = currPosition.y + 1;
		break;
	case TOPLEFT:
		deltaPositionX = currPosition.x - 1;
		deltaPositionY = currPosition.y - 1;
		break;
	case TOPRIGHT:
		deltaPositionX = currPosition.x + 1;
		deltaPositionY = currPosition.y - 1;
		break;
	case BOTTOMLEFT:
		deltaPositionX = currPosition.x - 1;
		deltaPositionY = currPosition.y + 1;
		break;
	case BOTTOMRIGHT:
		deltaPositionX = currPosition.x + 1;
		deltaPositionY = currPosition.y + 1;
		break;
	default:
		deltaPositionX = currPosition.x;
		deltaPositionY = currPosition.y;
		break;
	}
	if (deltaPositionY == targetPosition.y && deltaPositionX == targetPosition.x)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int CheckPointToBoxCollider(Point _collider, BoxCollider _collided)
{
	if (_collider.x >= _collided.minPoint.x && _collider.x <= _collided.maxPoint.x
		&& _collider.y >= _collided.minPoint.y && _collider.y <= _collided.maxPoint.y)
	{
		return 1;
	}

	return 0;
}

int CheckAABB(BoxCollider _collider, BoxCollider _collided)
{
	return 0;
}

int CheckCollisionAheadBoxCollider(BoxCollider _currBox, Direction direction, BoxCollider _target)
{
	Point deltaMin,deltaMax;
	switch (direction)
	{
	case LEFT:
		deltaMin.x = _currBox.minPoint.x - 1;
		deltaMin.y = _currBox.minPoint.y;
		deltaMax = _currBox.maxPoint;
		break;
	case RIGHT:
		deltaMin = _currBox.minPoint;
		deltaMax.x = _currBox.maxPoint.x + 1;
		deltaMax.y = _currBox.maxPoint.y;
		break;
	case UP:
		deltaMin.x = _currBox.minPoint.x;
		deltaMin.y = _currBox.minPoint.y - 1;
		deltaMax = _currBox.maxPoint;
		break;
	case DOWN:
		deltaMin = _currBox.minPoint;
		deltaMax.x = _currBox.maxPoint.x;
		deltaMax.y = _currBox.maxPoint.y;
		break;
	default:
		deltaMin = _currBox.minPoint;
		deltaMax = _currBox.maxPoint;
		break;
	}
	if ((deltaMax.x >= _target.minPoint.x && deltaMax.y >= _target.minPoint.y) &&
		(deltaMin.x <= _target.maxPoint.x && deltaMin.y <= _target.maxPoint.y))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int CheckCollisionAheadBoxColliderWithPoint(BoxCollider _currBox, Direction direction, Point _target)
{
	Point deltaMin, deltaMax;
	switch (direction)
	{
	case LEFT:
		deltaMin.x = _currBox.minPoint.x - 1;
		deltaMin.y = _currBox.minPoint.y;
		deltaMax = _currBox.maxPoint;
		break;
	case RIGHT:
		deltaMin = _currBox.minPoint;
		deltaMax.x = _currBox.maxPoint.x + 1;
		deltaMax.y = _currBox.maxPoint.y;
		break;
	case UP:
		deltaMin.x = _currBox.minPoint.x;
		deltaMin.y = _currBox.minPoint.y - 1;
		deltaMax = _currBox.maxPoint;
		break;
	case DOWN:
		deltaMin = _currBox.minPoint;
		deltaMax.x = _currBox.maxPoint.x;
		deltaMax.y = _currBox.maxPoint.y;
		break;
	default:
		deltaMin = _currBox.minPoint;
		deltaMax = _currBox.maxPoint;
		break;
	}
	if ((deltaMin.x == _target.x && deltaMin.y == _target.y)&&
		(deltaMax.x == _target.x && deltaMax.y == _target.y))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int CheckIfAtBoundary(int _x, int _y)
{
	if (_x >= MAPSIZEX - 1 || _x <= MAPSTARTPOSX ||
		_y >= MAPSIZEY - 1 || _y <= MAPSTARTPOSY)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}