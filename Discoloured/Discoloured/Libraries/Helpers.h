/**********************************************************************************
* \file			Helpers.h
* \brief		Helpers library which does extra math stuff
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* Helpers library which does extra math stuff

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#ifndef _Helpers_
#define _Helpers_

#include "Point.h"
/* brief: Get the distance between two points 
params: a - point A in the coordinate system
		b - Point B in the coordinate system
		output: float value between two points
*/
double GetDistanceBetweenPoints(Point a, Point b);

/* brief: Get the distance between two points in the same axis
params: int a -  takes in a value in one axis
		int b - takes in another value in the same axis
		output: int value between two points in the same axis
*/
int GetDistance(int a, int b);
#endif