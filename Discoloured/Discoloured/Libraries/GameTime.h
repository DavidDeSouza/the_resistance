/**********************************************************************************
* \file			GameTime.h
* \brief		handles timer for the game
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* An time related extension library made from Yannick Clock.h to check for time

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.1 - Added new function ConvertFromMinutes and convertFromSeconds
					  by Jolyn

				1.0 - Initial commit 
**********************************************************************************/
#ifndef _GAMETIME_
#define _GAMETIME_

/*brief Check Timer using delta Time 
param: *deltaTime - a pointer to the delatime 
		maxTime - how long the timer is in milliseconds
*/
int CheckTimerReached(float *deltaTime, float maxTime);

/*
	author  Jolyn Wong
	brief	Convert an input float value that is in terms of minutes into milliseconds
	param	_minutesToConvert - the float value in terms of minutes
	output	returns the minutes input converted into milliseconds
*/
float ConvertFromMinutes(float _minutesToConvert);

/*
	author  Jolyn Wong
	brief	Convert an input float value that is in terms of seconds into milliseconds
	param	_secondsToConvert - the float value in terms of seconds
	output	returns the seconds input converted into milliseconds
*/
float ConvertFromSeconds(float _secondsToConvert);

/* 
	author  Jolyn Wong
	brief	Displays the specified Game Timer in terms of minutes and seconds
	param	_timeToDisplay - the float to be displayed in minutes and seconds
	output	returns the max number of minutes possible from _timeToDisplay
*/
void DisplayTimer_MinutesSeconds(float _milliseconds, int* _minutesToDisplay, float* _secondsToDisplay);

#endif // !_GAMETIME_ b 