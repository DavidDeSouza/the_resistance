/**********************************************************************************
* \file			Draw.h
* \brief		Drawing library which draws a rectangle 
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* Drawing library which draws a rectangle 

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#ifndef _DRAW_H
#define _DRAW_H

/*brief Draw a Filled Rectangle
param: startPosX - the start position in the X coord of the rect
		startPosY - The start position in the Y coord of the rect
		endPosX - The End position in the X coord of the rect
		endPosY - The End Position in the Y coord of the rect
		c - the char used to draw the rect
*/
void DrawFilledRect(int startPosX, int startPosY,
	int endPosX, int endPosY, char c);

/*brief Draw a Empty Rectangle
param: startPosX - the start position in the X coord of the rect
		startPosY - The start position in the Y coord of the rect
		endPosX - The End position in the X coord of the rect
		endPosY - The End Position in the Y coord of the rect
		c - the char used to draw the rect
*/
void DrawEmptyRect(int startPosX, int startPosY,
	int endPosX, int endPosY, char c);

/*brief Draw a Filled Rectangle With Color
param: startPosX - the start position in the X coord of the rect
		startPosY - The start position in the Y coord of the rect
		endPosX - The End position in the X coord of the rect
		endPosY - The End Position in the Y coord of the rect
		c - the char used to draw the rect
		color - the color of the rectangle
*/
void DrawFilledRectWithColor(int startPosX, int startPosY,
	int endPosX, int endPosY, char c, int color);

/*brief Draw a Empty Rectangle and color the border of the rectangle
param: startPosX - the start position in the X coord of the rect
		startPosY - The start position in the Y coord of the rect
		endPosX - The End position in the X coord of the rect
		endPosY - The End Position in the Y coord of the rect
		c - the char used to draw the rect
		color - the color of the rectangle 
*/
void DrawEmptyRectWithColor(int startPosX, int startPosY,
	int endPosX, int endPosY, char c,int color);

#endif