#include "Draw.h"
#include "Collider.h"
#include "../Header_Files/Globals.h"

/***************************************************************************************
									   Functions
***************************************************************************************/

BoxCollider CreateBoxCollider(Point _minPoint, Point _maxPoint)
{
	BoxCollider temp;

	temp.minPoint = _minPoint;
	temp.maxPoint = _maxPoint;
	temp.midPoint.x = (_maxPoint.x - _minPoint.x) / 2;
	temp.midPoint.y = (_maxPoint.y - _minPoint.y) / 2;
	temp.color = FOREGROUND_GREEN_;
	temp.c = '*';

	return temp;
}

void RenderCollider(BoxCollider _collider)
{
	DrawEmptyRectWithColor(_collider.minPoint.x, _collider.minPoint.y,
						   _collider.maxPoint.x, _collider.maxPoint.y,
						   _collider.c, _collider.color);
}