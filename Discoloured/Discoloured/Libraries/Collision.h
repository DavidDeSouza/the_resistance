/**********************************************************************************
* \file			Collision.h
* \brief		Contains collision detection functions
* \author		Seow Jun Hao Darren
* \version		1.2
* \date			2019
*
* a library that does different type of collision detection

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.2 - Added CheckCheckCollisionAheadBoxCollider and
					  CheckCollisionAheadBoxColliderWithPoint
				1.1 - Added CheckPointToBoxCollider and CheckAABB by Jolyn
				1.0 - Initial commit
**********************************************************************************/
#ifndef _COLLISION_
#define _COLLISION_

#include "Point.h"
#include "Object.h"
#include "Collider.h"

/*Return a true/false statement when checking if one object is on top of another object
param:	currPosition - Takes in a Point struct of the object of the position 
		targetPosition - takes in a Point struct of the target position you want to check
*/
int CheckCollision(Point currPosition, Point targetPosition);

/*Return a true/false statement when checking if there is an object in front 
param:	currPosition - Takes in a Point struct of the object of the position
		currDirection - Enum of the direction of the object
		targetPosition - takes in a Point struct of the target position you want to check
*/
int CheckCollisionAhead(Point currPosition, Direction currDirection, Point targetPosition);

/*
	brief	Handles the checking of collision between a point and a box collider

	param	_collider - the point on the map to check with
			_collided - the box collider to check with the point

	Output	returns an int value 1 if the point is inside the box collider
*/
int CheckPointToBoxCollider(Point _collider, BoxCollider _collided);

/*
	brief	Handles the checking of collision between two box colliders

	param	_collider - the box collider to check with
			_collided - the box collider that the _collider collided with

	Output	returns an int value 1 if the two box collider overlaps with one another
*/
int CheckAABB(BoxCollider _collider, BoxCollider _collided);

/*Return a true/false statement when checking if one boxcollider is on top of another 
  boxcollider in the next frame in certain direction
param:	currPosition - Takes in a Point struct of the object of the position
		targetPosition - takes in a Point struct of the target position you want to check
*/
int CheckCollisionAheadBoxCollider(BoxCollider _currBox, Direction direction, BoxCollider _target);

/*Return a true/false statement when checking if one boxcollider is on top of another
  object in the next frame in certain direction
param:	currPosition - Takes in a Point struct of the object of the position
		targetPosition - takes in a Point struct of the target position you want to check
*/
int CheckCollisionAheadBoxColliderWithPoint(BoxCollider _currBox, Direction direction, Point _target);



/*
	brief	Checks if any bullet's position has reached the boundaries

	param	_x - the position of the bullet in the x axis
			_y - the position of the bullet in the y axis

	Output	return an int that represents a boolean to determine if bullet is at boundaries
*/
int CheckIfAtBoundary(int _x, int _y);

#endif