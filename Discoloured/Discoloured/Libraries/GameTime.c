/**********************************************************************************
* \file			GameTime.h
* \brief		handles timer for the game
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* An time related extension library made from Yannick Clock.h to check for time

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.1 - Added new function ConvertFromMinutes and convertFromSeconds
					  by Jolyn
				1.0 - Initial commit
**********************************************************************************/
#include "../Clock/Clock.h"

int CheckTimerReached(float *deltaTime, float maxTime)
{
	*deltaTime += Clock_GetDeltaTime();
	if (*deltaTime < maxTime)
	{
		return 0;
	}
	else
	{
		*deltaTime -= maxTime;
  		return 1;
	}
}

float ConvertFromMinutes(float _minutesToConvert)
{
	return _minutesToConvert * 60.0f * 1000.0f;
}

float ConvertFromSeconds(float _secondsToConvert)
{
	return _secondsToConvert * 1000.0f;
}

/* TODO: Clean up the codes */
void DisplayTimer_MinutesSeconds(float _milliseconds, int* _minutesToDisplay, float* _secondsToDisplay)
{
	*_minutesToDisplay = (int)(_milliseconds / (60.0f * 1000.0f));
	*_secondsToDisplay = (_milliseconds - (*_minutesToDisplay * 60.0f * 1000.0f)) / 1000.0f;
}


