/**********************************************************************************
* \file			Object.h
* \brief		Contains the enum of direction for objects used in the process of
				making the game
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* Contains the enum of direction for objects used in the process of
  making the game

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#ifndef _OBJECT_
#define _OBJECT_
#include "Point.h"
#define NUMOFDIRECTION 4
/*enum thats contains all the 8 directions*/
typedef enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN,
	TOPLEFT,
	TOPRIGHT,
	BOTTOMLEFT,
	BOTTOMRIGHT,
	DEFAULT,
}AIDirection,PlayerDirection,Direction,FaceDirection;

#endif