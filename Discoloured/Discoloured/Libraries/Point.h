/**********************************************************************************
* \file			Point.h
* \brief		Contains the Point struct to containt the x and y position of an 
				object
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* Contains the Point struct to containt the x and y position of an object

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#ifndef POINT_H
#define POINT_H

typedef struct Point{
	int x;
	int y;
}Point;

#endif