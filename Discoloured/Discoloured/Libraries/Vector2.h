/**********************************************************************************
* \file			Vector2.h
* \brief		Contains a Vector2 struct that is a redefination of Point struct
* \author		Jolyn Wong Kaiyi
* \date			2019
*
*	This script stores structure Vector2, which is just a re-defination of the 
*   structure Point defined in Point.h.
*
* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
**********************************************************************************/
#ifndef _VECTOR_2_H_
#define _VECTOR_2_H_

#include "Point.h"

typedef Point Vector2;

/*
	author  Jolyn Wong

	brief	Sets a Vector2 variables to new values specified in parameters

	param	_x - new x value
			_y - new y value

	Output	return a Vector2 with updated values in x and y
*/
Vector2 SetVector(int _x, int _y);

#endif