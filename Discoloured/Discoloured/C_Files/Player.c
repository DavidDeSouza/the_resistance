﻿#include "../Libraries/Point.h"
#include "../Header_Files/Globals.h"
#include "../Libraries/GameTime.h"
#include "../Console/Console.h"
#include "../Random/Random.h"
#include "../Libraries/Object.h"
#include "../Libraries/Collision.h"
#include "../Libraries/Helpers.h"
#include "../Header_Files/DeltaTime.h"
#include "../Header_Files/Player.h"
#include "../Header_Files/Paint/Bullets.h"
#include "../Header_Files/PlayerControlOptions.h"
#include "../Header_Files/UltimateAbility.h"
#include "../Header_Files/Play.h"
#include "../Header_Files/Audio.h"
#include <limits.h>
#include <Windows.h>
#include <stdio.h>
#include <string.h>

int isShooting = 1;

void Move(PlayerComponent *_player, double _velocity)
{
	if (_player->bIsDead || preGamePause)
		return;

	if (CheckEuler(&_player->Euler, _velocity))
	{
		if (_player->direction == DOWN)
		{
			if (map[_player->bodyPosition.x][_player->bodyPosition.y + 1].c != 'W')
			{
				_player->bodyPosition.y++;
			}
		}
		if (_player->direction == UP)
		{
			if (map[_player->bodyPosition.x][_player->bodyPosition.y - 1].c != 'W')
			{
				_player->bodyPosition.y--;
			}
		}
		if (_player->direction == LEFT)
		{
			if (map[_player->bodyPosition.x - 1][_player->bodyPosition.y].c != 'W')
			{
				_player->bodyPosition.x--;
			}
		}
		if (_player->direction == RIGHT)
		{
			if (map[_player->bodyPosition.x + 1][_player->bodyPosition.y].c != 'W')
			{
				_player->bodyPosition.x++;
			}
		}
		if (_player->direction == DEFAULT)
		{
			return;
		}

		RenderPlayerTrails(_player);
		UpdatePlayerCollider(_player);
	}
}



void InitializePlayer(PlayerComponent *_player, char _body)
{
	_player->direction = DEFAULT;
	_player->shootingDirection = DEFAULT;
	_player->sizeX = SIZE_X / 2;
	_player->sizeY = SIZE_Y / 2;
	_player->Euler = 0.0f;
	_player->respawnTimer = 0.0f;
	_player->shootTimer = 0.0f;
	_player->invulnerableBlinkTimer = 0.0f;
	_player->speed = SPEED;
	_player->bodyColor = _player->teamColor;
	_player->bUsedUltimate = 0;
	_player->bIsDead = 0;
	_player->body = _body;
	_player->isRendering = 1;
	_player->bIsActive = 1;
}

void UpdatePlayerCollider(PlayerComponent *_player)
{
	Point minPoint, maxPoint;
	minPoint = SetVector(_player->bodyPosition.x - _player->sizeX, _player->bodyPosition.y - _player->sizeY);
	maxPoint = SetVector(_player->bodyPosition.x + _player->sizeX, _player->bodyPosition.y + _player->sizeY);

	_player->collider = CreateBoxCollider(minPoint, maxPoint);
}

void ReadInput()
{
	//Register Direction
	if (GetAsyncKeyState(VK_D))
	{
		player1.direction = RIGHT;
		Move(&player1, player1.speed);
	}
	if (GetAsyncKeyState(VK_A))
	{
		player1.direction = LEFT;
		Move(&player1, player1.speed);
	}
	if (GetAsyncKeyState(VK_W))
	{
		player1.direction = UP;
		Move(&player1, player1.speed);
	}
	if (GetAsyncKeyState(VK_S))
	{
		player1.direction = DOWN;
		Move(&player1, player1.speed);
	}
	if (GetAsyncKeyState(VK_LEFT) && !isShooting)
	{
		player1.shootingDirection = LEFT;
		if (!player1.bIsDead)
		{
			ShootingBullets(player1.bodyPosition, player1.shootingDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX),
				player1.teamColor, player1.shotColor, 0.0f, &player1.shootTimer);
			isShooting = 1;
			PlayAudio(audio.shot);
		}
	}
	else if (GetAsyncKeyState(VK_RIGHT) && !isShooting)
	{
		player1.shootingDirection = RIGHT;
		if (!player1.bIsDead)
		{
			ShootingBullets(player1.bodyPosition, player1.shootingDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX),
				player1.teamColor, player1.shotColor, 0.0f, &player1.shootTimer);
			isShooting = 1;
			PlayAudio(audio.shot);
		}
	}
	else if (GetAsyncKeyState(VK_UP) && !isShooting)
	{
		player1.shootingDirection = UP;
		if (!player1.bIsDead)
		{
			ShootingBullets(player1.bodyPosition, player1.shootingDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX),
				player1.teamColor, player1.shotColor, 0.0f, &player1.shootTimer);
			isShooting = 1;
			PlayAudio(audio.shot);
		}
	}
	else if (GetAsyncKeyState(VK_DOWN) && !isShooting)
	{
		player1.shootingDirection = DOWN;
		if (!player1.bIsDead)
		{
			ShootingBullets(player1.bodyPosition, player1.shootingDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX),
				player1.teamColor, player1.shotColor, 0.0f, &player1.shootTimer);
			isShooting = 1;
			PlayAudio(audio.shot);
		}
	}
	else if (!GetAsyncKeyState(VK_LEFT) && !GetAsyncKeyState(VK_RIGHT) &&
		!GetAsyncKeyState(VK_UP) && !GetAsyncKeyState(VK_DOWN))
	{
		isShooting = 0;
	}

	if (GetAsyncKeyState(VK_SPACE))
	{
		if (player1.teamColor == teamA.color)
		{
			UseUltimate(&teamA, &player1);
		}
		else
		{
			UseUltimate(&teamB, &player1);
		}
	}
}

void RenderPlayer(PlayerComponent *_player)
{
	if (!_player->isRendering)
		return;

	for (int x = _player->bodyPosition.x - _player->sizeX; x <= _player->bodyPosition.x + _player->sizeX; x++)
	{
		for (int y = _player->bodyPosition.y - _player->sizeY; y <= _player->bodyPosition.y + _player->sizeY; y++)
		{
			if (x == _player->bodyPosition.x && y == _player->bodyPosition.y)
			{
				if (_player->teamColor == teamA.color)
				{
					Console_SetRenderBuffer_Colour_Char(x, y, _player->body, teamA.shotColor);
				}
				else
				{
					Console_SetRenderBuffer_Colour_Char(x, y, _player->body, teamB.shotColor);
				}
			}
			else
			{
				if (_player == &player1)
				{
					Console_SetRenderBuffer_Colour_Char(x, y, ' ', BACKGROUND_PURPLE | FOREGROUND_WHITE_);
				}
				else if (_player == &player2)
				{
					Console_SetRenderBuffer_Colour_Char(x, y, ' ', BACKGROUND_BLUE_ | FOREGROUND_WHITE_);
				}
				else if (_player == &player3)
				{
					Console_SetRenderBuffer_Colour_Char(x, y, ' ', BACKGROUND_GREEN_ | FOREGROUND_WHITE_);
				}
				else if (_player == &player4)
				{
					Console_SetRenderBuffer_Colour_Char(x, y, ' ', BACKGROUND_RED_ | FOREGROUND_WHITE_);
				}
				
			}
		}
	}
}

void RenderPlayerTrails(PlayerComponent *_player)
{
	for (int x = _player->bodyPosition.x - 1; x <= _player->bodyPosition.x + 1; x++)
	{
		for (int y = _player->bodyPosition.y - 1; y <= _player->bodyPosition.y + 1; y++)
		{
			if (map[x][y].c != 'W')
				map[x][y].color = _player->teamColor;
		}
	}
}

void RenderPlayerTag(PlayerComponent *_player)
{
	if (_player->bodyPosition.y - 2 > MAPSTARTPOSY)
	{
		sprintf_s(_player->respawnTag, sizeof(_player->respawnTag), "%.0f", 2.0 - (_player->respawnTimer / 1000.0f));
		Console_SetRenderBuffer_Colour_String(_player->bodyPosition.x, _player->bodyPosition.y - 2, _player->respawnTag, FOREGROUND_WHITE_ | BACKGROUND_BLACK);
	}
}

void ResetPlayer(PlayerComponent *_player)
{
	_player->direction = DEFAULT;
	_player->shootingDirection = DEFAULT;
	_player->Euler = 0.0f;
	_player->respawnTimer = 0.0f;
	_player->shootTimer = 0.0f;
	_player->bIsDead = 0;
	_player->bIsInvulnerable = 1;
	_player->isRendering = 1;
	_player->invulnerableBlinkTimer = 0.0f;

	UpdatePlayerCollider(_player);
}