#include "../Header_Files/DeltaTime.h"
#include "../Clock/Clock.h"

int CheckEuler(double *euler, double velocity)
{
	*euler += velocity * Clock_GetDeltaTime();
	if (*euler >= 1.0)
	{
		*euler -= 1.0;
		return 1;
	}
	else
	{
		return 0;
	}
}