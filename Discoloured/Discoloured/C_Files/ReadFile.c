#include "../Header_Files/ReadFile.h"
#include <stdio.h>
#include "../Header_Files/Wall.h"
#include "../Random/Random.h"
#include "../Header_Files/Globals.h"

void MapRandomiser()
{
	int currentLine = 0;
	char string[50] = { 0 };
	int _topx = 0, _topy = 0, _botx = 0, _boty = 0;
	FILE *filePointer = NULL;
	int MapType;

	switch (Random_Range(1, 5))
	{
		case 1: fopen_s(&filePointer, "Data/Walls_1.txt", "r"); break;
		case 2: fopen_s(&filePointer, "Data/Walls_2.txt", "r"); break;
		case 3: fopen_s(&filePointer, "Data/Walls_3.txt", "r"); break;
		case 4: fopen_s(&filePointer, "Data/Walls_4.txt", "r"); break;
		case 5: fopen_s(&filePointer, "Data/Walls_5.txt", "r"); break;

	}
	
	if (!filePointer)
	{
		return;
	}

/************************************************************************************
	Trying to read in ratios of the walls instead of just actual point data
************************************************************************************/

	fgets(string, 50, filePointer);
	sscanf_s(string, "%d", &MapType);
	currentLine++;

	while (fgets(string, 50, filePointer))
	{
		if (MapType == 0)
		{

			sscanf_s(string, "%d %d %d %d", &_topx, &_topy, &_botx, &_boty);
			_topx = (MAPSIZEX * 10) / _topx;
			_topy = (MAPSIZEY * 10) / _topy;
			_botx = (MAPSIZEX * 10) / _botx;
			_boty = (MAPSIZEY * 10) / _boty;
			InitializeWall(_topx, _topy, _botx, _boty, currentLine);
			currentLine++;

		}
		else if (MapType == 1)
		{
			sscanf_s(string, "%d %d %d %d", &_topx, &_topy, &_botx, &_boty);
			_topx = (MAPSIZEX * 10) / _topx;
			_topy = (MAPSIZEY * 10) / _topy;
			_botx = (MAPSIZEX * 10) / _botx;
			_boty = (MAPSIZEY * 10) / _boty;

			InitializeWall(_topx, _topy, _botx, _boty, currentLine);
			currentLine++;


			InitializeWall((MAPSIZEX - _botx), (MAPSIZEY - _boty), (MAPSIZEX - _topx), (MAPSIZEY - _topy), currentLine);
			currentLine++;
		}
	}


	fclose(filePointer);
}


/************************************************************************************
   Calculates Midpoint without floats
************************************************************************************/

int Calc_ConsoleMid(int a, int b)
{
	int Result = 0;
	if (b == 0)
	{
		Result = 0;
	}
	else
	{
		Result = (a / b) + (a % b);
	}

	return Result;
}

