/**********************************************************************************
* \file			AI.h
* \brief		AI logic functions belongs here/AI.c
* \author		Seow Jun Hao Darren
* \version		1.2
* \date			2019
*
*	The AI Library that contains the AI mechanics such as render/movement/spawning/
*	initialization

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.2 - Upgrade AI to learn how to shoot in a direction where it
					  contain the least piant
*				1.1 - Upgrade AI movement logic so that they can move from one
					  point to another.
*
*				1.0 - Initial commit
**********************************************************************************/
#include "../Libraries/Point.h"
#include "../Header_Files/Globals.h"
#include "../Libraries/GameTime.h"
#include "../Console/Console.h"
#include "../Random/Random.h"
#include "../Libraries/Object.h"
#include "../Libraries/Collision.h"
#include "../Libraries/Helpers.h"
#include <float.h>
#include "../Header_Files/DeltaTime.h"
#include "../Header_Files/Paint/Bullets.h"
#include "../Header_Files/UltimateAbility.h"
#include <stdio.h>

#define VELOCITY 0.028
#define SHOOTINTERVAL 100 /*in millisecs*/


void InitializeAI(AIComponent *_AI,int _color)
{
	_AI->body = 'A';	
	_AI->b_isActive = 0;
	_AI->teamColor = _color;
	_AI->b_canUseUltimate = 0;
	_AI->direction = Random_Range(0,NUMOFDIRECTION);
	_AI->velocity = VELOCITY;
	_AI->euler = 0.0;
	_AI->sizeX = SIZE_X / 2;
	_AI->sizeY = SIZE_Y / 2;
}

void SpawnAI(AIComponent *_AI)
{

	_AI->body = 'A';
	_AI->bodyColor = _AI->teamColor;
	_AI->shotColor = _AI->teamShotColor;
	_AI->bSetWayPoint = 1;
	_AI->bGoToWayPoint = 0;
	_AI->respawnTimer = 0.0f;
	_AI->shootTimer = 0.0f;
	_AI->bIsDead = 0;
	_AI->bIsInvulnerable = 1;
	_AI->invulnerableBlinkTimer = 0.0f;
	_AI->isRendering = 1;

	UpdateAICollider(_AI);
}

void UpdateAICollider(AIComponent *_AI)
{
	Point minPoint, maxPoint;
	minPoint = SetVector(_AI->bodyPosition.x - _AI->sizeX, _AI->bodyPosition.y - _AI->sizeY);
	maxPoint = SetVector(_AI->bodyPosition.x + _AI->sizeX, _AI->bodyPosition.y + _AI->sizeY);

	_AI->collider = CreateBoxCollider(minPoint, maxPoint);
}
/*\brief choose a valid waypoint to go
 *\param take in AI component 
 */
void SetWayPoint(AIComponent *_AI)
{
	if (_AI->bSetWayPoint==1)
	{
		
		int x = Random_Range(0, MAPSIZEX);
		int y = Random_Range(0, MAPSIZEY);
		
		for (int i = 0; i < NUMBEROFWALL; ++i)
		{
			if (i == 0)
			{
				if (map[x][y].position.x >= wall[i].startPosition.x
					&&map[x][y].position.x <= wall[i].endPosition.x
					&&map[x][y].position.y >= wall[i].startPosition.y
					&&map[x][y].position.y <= wall[i].endPosition.y)
				{
					/*go through the loop again if the waypoint is outside the border*/
					i = 0;
					x = Random_Range(0, MAPSIZEX);
					y = Random_Range(0, MAPSIZEY);
				}
			}
			else
			{
				if (map[x][y].position.x >= wall[i].startPosition.x
					&&map[x][y].position.x <= wall[i].endPosition.x
					&&map[x][y].position.y >= wall[i].startPosition.y
					&&map[x][y].position.y <= wall[i].endPosition.y)
				{
					/*go through the loop again if the waypoint is in the wall*/
					i = 0;
					x = Random_Range(0, MAPSIZEX);
					y = Random_Range(0, MAPSIZEY);
				}
			}
		}
		_AI->waypoint.x = map[x][y].position.x;
		_AI->waypoint.y = map[x][y].position.y;
		_AI->bSetWayPoint = 0;
		_AI->bGoToWayPoint = 1;
	}
}
/*\brief check if the AI reach the way point, then choose a waypoint
 *\brief in the event where the AI doesn't know how to go to the way point/stuck
 *\brief choose a new waypoint instead
 *\param take in AI component
 */
void ReachWayPoint(AIComponent *_AI)
{
	if (CheckCollisionAhead(_AI->bodyPosition, _AI->direction, _AI->prevPos))
	{
		_AI->prevPos.x = -1;
		_AI->prevPos.y = -1;
		_AI->bSetWayPoint = 1;
		_AI->bGoToWayPoint = 0;
	}
	if (_AI->bodyPosition.x == _AI->waypoint.x&&_AI->bodyPosition.y == _AI->waypoint.y)
	{
		_AI->prevPos.x = -1;
		_AI->prevPos.y = -1;
		_AI->bSetWayPoint = 1;
		_AI->bGoToWayPoint = 0;
	}
}
/*\brief do a future stepand update its deltaPosition
 *\param take in AI component and its direction
 */
void DoFutureStep(AIComponent *_AI, Direction direction)
{
	switch (direction)
	{
	case LEFT:
		_AI->deltaPosition.x = _AI->bodyPosition.x - 1;
		_AI->deltaPosition.y = _AI->bodyPosition.y;
		break;
	case RIGHT:
		_AI->deltaPosition.x = _AI->bodyPosition.x + 1;
		_AI->deltaPosition.y = _AI->bodyPosition.y;
		break;
	case UP:
		_AI->deltaPosition.x = _AI->bodyPosition.x;
		_AI->deltaPosition.y = _AI->bodyPosition.y - 1;
		break;
	case DOWN:
		_AI->deltaPosition.x = _AI->bodyPosition.x;
		_AI->deltaPosition.y = _AI->bodyPosition.y + 1;
		break;
	case TOPLEFT:
		_AI->deltaPosition.x = _AI->bodyPosition.x - 1;
		_AI->deltaPosition.y = _AI->bodyPosition.y - 1;
		break;
	case TOPRIGHT:
		_AI->deltaPosition.x = _AI->bodyPosition.x + 1;
		_AI->deltaPosition.y = _AI->bodyPosition.y - 1;
		break;
	case BOTTOMLEFT:
		_AI->deltaPosition.x = _AI->bodyPosition.x - 1;
		_AI->deltaPosition.y = _AI->bodyPosition.y + 1;
		break;
	case BOTTOMRIGHT:
		_AI->deltaPosition.x = _AI->bodyPosition.x + 1;
		_AI->deltaPosition.y = _AI->bodyPosition.y + 1;
		break;
	default:
		break;
	}
}
/*\brief choose a cheapest option to go to the waypoint
 *\param take in AI component
 */
void ChooseGoToWayPointDirection(AIComponent *_AI)
{
	double cost[NUMOFDIRECTION] = { 0 }; 
	double lowestCost = DBL_MAX;
	_AI->direction = DEFAULT;

	for (int direction = 0; direction < NUMOFDIRECTION; ++direction)
	{
		DoFutureStep(_AI, direction);
		int x = _AI->deltaPosition.x;
		int y = _AI->deltaPosition.y;
		int b_collisionWithOtherAI = 0;
		/*check if the future step collide with other AI*/
		for (int i = 0; i < MAXNUMBEROFAI; ++i)
		{
			if (_AI != &AI[i])
			{
				if (CheckCollisionAhead(_AI->bodyPosition, direction, (&AI[i])->bodyPosition))
				{
					b_collisionWithOtherAI = 1;
					break;
				}
			}
		}
		/*check if the future step collide with wall/other wall, if so don't choose the future step direction*/
		if (map[x][y].c == 'W'
			|| CheckCollisionAhead(_AI->bodyPosition, direction, player1.bodyPosition)
			|| CheckCollisionAhead(_AI->bodyPosition, direction, player2.bodyPosition)
			|| CheckCollisionAhead(_AI->bodyPosition, direction, player3.bodyPosition)
			|| CheckCollisionAhead(_AI->bodyPosition, direction, player4.bodyPosition))
		{						
			continue;
		}
		/*give a higher cost if the future step contains its team color so it will less likely choose it*/
		else if (map[x][y].color == _AI->bodyColor || map[x][y].color == _AI->teamShotColor)
		{
			cost[direction] = 2;
		}
		else
		{
			cost[direction] = 1;
		}
		/*give additional cost so that AI will less likely choose this direction*/
		if (b_collisionWithOtherAI)
		{
			cost[direction] += 2;
		}
		
				
	}
	/*Give a extra cost between the distance between the AI and the waypoint it chose*/
	for (int i = 0; i < NUMOFDIRECTION; ++i)
	{
		if (cost[i] != 0)
		{
			DoFutureStep(_AI, i);
			cost[i] += GetDistanceBetweenPoints(_AI->deltaPosition, _AI->waypoint);
		}
		
	}
	for (int i = 0; i < NUMOFDIRECTION; ++i)
	{
		if (cost[i] < lowestCost && cost[i] != 0)
		{
			lowestCost = cost[i];
		}
	}
	/*choose the lowest cost among the direction*/
	for (int i = 0; i < NUMOFDIRECTION; i++)
	{
		if (cost[i] == lowestCost && cost[i] != 0)
		{
			_AI->direction = i;
			break;
		}
	}	
}
/*\brief Allow the AI to go to waypoint
 *\param take in AI component
 */
void GoToWayPoint(AIComponent *_AI)
{	
	ChooseGoToWayPointDirection(_AI);
	ReachWayPoint(_AI);
}
/*\brief Allows the AI to choose its shoot direction,
 *\brief chooses the lowest team paint direction
 *\param take in AI component
 */
void ChooseShootDirection(AIComponent *_AI)
{
	double numOfOtherTeamPaint[NUMOFDIRECTION] = { 0 };
	double highestNumOfOtherTeamPaint = DBL_MAX;
	_AI->shootDirection = DEFAULT;

	for (int direction = 0; direction < NUMOFDIRECTION; direction++)
	{
		for (int j = 0; j < SHOT_RANGE_MAX; j++)
		{
			unsigned int x,y;
			switch (direction)
			{
			case LEFT:
				x = _AI->bodyPosition.x - j;
				y = _AI->bodyPosition.y;
				if (_AI->bodyPosition.x - j < wall[0].startPosition.x)
				{
					x = wall[0].startPosition.x+1;
				}
				if (map[x][y].color == _AI->teamColor|| map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			case RIGHT:
				x = _AI->bodyPosition.x + j;
				y = _AI->bodyPosition.y;
				if (_AI->bodyPosition.x + j > wall[0].endPosition.x)
				{
					x = wall[0].endPosition.x-1;
				}
				if (map[x][y].color == _AI->teamColor || map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			case UP:
				x = _AI->bodyPosition.x;
				y = _AI->bodyPosition.y-j;
				if (_AI->bodyPosition.y - j < wall[0].startPosition.y)
				{
					y = wall[0].startPosition.y - 1;
				}
				if(map[x][y].color == _AI->teamColor || map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			case DOWN:
				x = _AI->bodyPosition.x;
				y = _AI->bodyPosition.y + j;
				if (_AI->bodyPosition.y + j > wall[0].endPosition.y)
				{
					y = wall[0].endPosition.y + 1;
				}
				if (map[x][y].color == _AI->teamColor || map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			case TOPLEFT:
				x = _AI->bodyPosition.x - j;
				y = _AI->bodyPosition.y - j;
				if (_AI->bodyPosition.x - j < wall[0].startPosition.x)
				{
					x = wall[0].startPosition.x + 1;
				}
				if (_AI->bodyPosition.y - j < wall[0].startPosition.y)
				{
					y = wall[0].startPosition.y - 1;
				}

				if (map[x][y].color == _AI->teamColor || map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			case TOPRIGHT:
				x = _AI->bodyPosition.x + j;
				y = _AI->bodyPosition.y - j;
				if (_AI->bodyPosition.x + j > wall[0].endPosition.x)
				{
					x = wall[0].endPosition.x - 1;
				}
				if (_AI->bodyPosition.y - j < wall[0].startPosition.y)
				{
					y = wall[0].startPosition.y - 1;
				}
				if (map[x][y].color == _AI->teamColor || map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			case BOTTOMLEFT:
				x = _AI->bodyPosition.x - j;
				y = _AI->bodyPosition.y + j;
				if (_AI->bodyPosition.x - j < wall[0].startPosition.x)
				{
					x = wall[0].startPosition.x + 1;
				}
				if (_AI->bodyPosition.y + j > wall[0].endPosition.y)
				{
					y = wall[0].endPosition.y + 1;
				}
				if (map[x][y].color == _AI->teamColor || map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			case BOTTOMRIGHT:
				x = _AI->bodyPosition.x + j;
				y = _AI->bodyPosition.y + j;
				if (_AI->bodyPosition.x + j > wall[0].endPosition.x)
				{
					x = wall[0].endPosition.x - 1;
				}
				if (_AI->bodyPosition.y + j > wall[0].endPosition.y)
				{
					y = wall[0].endPosition.y + 1;
				}

				if (map[x][y].color == _AI->teamColor || map[x][y].color == _AI->teamShotColor)
				{
					++numOfOtherTeamPaint[direction];
				}
				break;
			default:
				break;
			}
		}
	}
	
	for (int i = 0; i < NUMOFDIRECTION; i++)
	{
		if (numOfOtherTeamPaint[i] < highestNumOfOtherTeamPaint && numOfOtherTeamPaint != 0)
		{
			highestNumOfOtherTeamPaint = numOfOtherTeamPaint[i];
		}
	}
	for (int i = 0; i < NUMOFDIRECTION; i++)
	{
		if (numOfOtherTeamPaint[i] == highestNumOfOtherTeamPaint && numOfOtherTeamPaint != 0)
		{
			_AI->shootDirection = i;
			break;
		}
	}
}

void AIMovement(AIComponent *_AI)
{
	SetWayPoint(_AI);
	if (_AI->bGoToWayPoint == 1&&CheckEuler(&_AI->euler, _AI->velocity))
	{
		RenderAITrails(_AI);
		GoToWayPoint(_AI);
		_AI->prevPos = _AI->bodyPosition;
		switch (_AI->direction)
		{
		case LEFT:
			_AI->bodyPosition.x -= 1;			
			break;
		case RIGHT:
			_AI->bodyPosition.x += 1;			
			break;
		case UP:
			_AI->bodyPosition.y -= 1;			
			break;
		case DOWN:
			_AI->bodyPosition.y += 1;
			break;
		case TOPLEFT:
			_AI->bodyPosition.x -= 1;
			_AI->bodyPosition.y -= 1;
			break;
		case TOPRIGHT:
			_AI->bodyPosition.x += 1;
			_AI->bodyPosition.y -= 1;
			break;
		case BOTTOMLEFT:
			_AI->bodyPosition.x -= 1;
			_AI->bodyPosition.y += 1;
			break;
		case BOTTOMRIGHT:
			_AI->bodyPosition.x += 1;
			_AI->bodyPosition.y += 1;
		default:
			break;
		}		

		// This updates the collider pos ONLY when the AI moves 
		UpdateAICollider(_AI);
	}
}
void AIShoot(AIComponent *_AI)
{
	if (_AI->shootDirection != DEFAULT)
	{
		ChooseShootDirection(_AI);
		ShootingBullets(_AI->bodyPosition, _AI->shootDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX), _AI->teamColor, _AI->shotColor, 600.0f, &_AI->shootTimer);
	}
}

void AICastUltimate(AIComponent *_AI)
{
	if (_AI->teamColor == teamA.color && (teamA.maxNumOfPlayer - teamA.numOfPlayers) == MAXNUMBERINTEAM)
	{
		AIUseUltimate(&teamA, _AI);
	}
	else if (_AI->teamColor == teamB.color && (teamB.maxNumOfPlayer - teamB.numOfPlayers) == MAXNUMBERINTEAM)
	{
		AIUseUltimate(&teamB, _AI);
	}
}

void RenderAITrails(AIComponent *_AI)
{
	for (int x = _AI->bodyPosition.x-1 >=0 ? _AI->bodyPosition.x - 1: _AI->bodyPosition.x; x <= _AI->bodyPosition.x + 1; x++)
	{
		for (int y = _AI->bodyPosition.y - 1 >= 0 ? _AI->bodyPosition.y - 1 : _AI->bodyPosition.y; y <= _AI->bodyPosition.y + 1; y++)
		{
			if (map[x][y].c != 'W')
				map[x][y].color = _AI->teamColor;
		}
	}
}

void RenderAITag(AIComponent *_AI)
{
	if (_AI->bodyPosition.y - 2 > MAPSTARTPOSY)
	{
		sprintf_s(_AI->respawnTag, sizeof(_AI->respawnTag), "%.0f", 2.0 - (_AI->respawnTimer / 1000.0f));
		Console_SetRenderBuffer_Colour_String(_AI->bodyPosition.x, _AI->bodyPosition.y - 2, _AI->respawnTag, FOREGROUND_WHITE_ | BACKGROUND_BLACK);
	}
}

void RenderAI(AIComponent  *_AI)
{
	if (!_AI->isRendering)
		return;

	for (int x = _AI->bodyPosition.x - _AI->sizeX; x <= _AI->bodyPosition.x + _AI->sizeX; x++)
	{
		for (int y = _AI->bodyPosition.y - _AI->sizeY; y <= _AI->bodyPosition.y + _AI->sizeY; y++)
		{
			if (_AI->bodyPosition.x == x && _AI->bodyPosition.y == y)
			{
				Console_SetRenderBuffer_Colour_Char(x, y, _AI->body, _AI->teamColor);
			}
			else
			{
				
				Console_SetRenderBuffer_Colour_Char(x, y, ' ', _AI->shotColor);
			}
		}
	}
}