#include "../Header_Files/Initialise.h"
#include "../Random/Random.h"
#include "../Console/Console.h"
#include "../Header_Files/AI.h"
#include "../Header_Files/Wall.h"
#include "../Header_Files/Globals.h"
#include "../Libraries/Draw.h"
#include "../Header_Files/ReadFile.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/Paint/Bullets.h"
#include "../Header_Files/Player.h"
#include "../Header_Files/Audio.h"

//========================================================================
//	author: David De Souza
//  Initialises the game's colour palette 
//========================================================================
void InitialisePalette()
{
	ConsoleColorPalette _palette;
	_palette.color[0] = RGB(118, 118, 118); // intensity
	_palette.color[1] = RGB(0, 0, 0); // black
	_palette.color[2] = RGB(255, 255, 255); // white
	_palette.color[3] = RGB(189, 189, 189); // grey
	_palette.color[4] = RGB(253, 223, 121); // orange
	_palette.color[5] = RGB(28, 147, 255); // blue
	_palette.color[6] = RGB(21, 234, 135); // green
	_palette.color[7] = RGB(255, 39, 50); // red
	_palette.color[8] = RGB(72, 97, 148); // teal
	_palette.color[9] = RGB(170, 90, 255); // purple
	_palette.color[10] = RGB(249, 198, 93); // light orange
	_palette.color[11] = RGB(93, 171, 232); // light teal

	Console_SetColorPalette(&_palette);
}

void InitialiseGame()
{
	Console_Init();
	Random_Init();
	Console_SetTitle("Discolored");
	Console_SetSquareFont();
	Console_SetCursorVisibility(0);
	Console_SetWindowedMode(WINDOWSIZEX, WINDOWSIZEY, 0);
	InitialisePalette();
	InitialiseAudio();
}