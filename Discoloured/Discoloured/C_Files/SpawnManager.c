#include "../Header_Files/SpawnManager.h"
#include "../Header_Files/Animation/Animations.h"
#include "../Libraries/Collision.h"
#include "../Header_Files/Globals.h"
#include "../Libraries/GameTime.h"
#include "../Console/Console.h"
#include "../Random/Random.h"
#include "../Libraries/Vector2.h"
#include "../Libraries/Helpers.h"

#define MAX_NUM_OF_SPAWN_POINTS 4

/***************************************************************************************
									Local Variables
***************************************************************************************/
SpawnPoint g_spawnPointsA[MAX_NUM_OF_SPAWN_POINTS];
SpawnPoint g_spawnPointsB[MAX_NUM_OF_SPAWN_POINTS];

const float g_respawnInterval = 2.0f; // in seconds
const float invulnerability_duration = 2.0f; // seconds

/***************************************************************************************
									   Functions
***************************************************************************************/
void InitialiseSpawnPoints(Team _team, int _offsetX, int _offsetY)
{
	for (int i = 0; i < MAX_NUM_OF_SPAWN_POINTS; ++i)
	{
		// Based on the team, set the position according to which side the team belong to
		g_spawnPointsA[i].position.x = MAPSTARTPOSX + _offsetX;
		g_spawnPointsB[i].position.x = MAPSIZEX - _offsetX;

		g_spawnPointsA[i].position.y = MAPSTARTPOSY + _offsetY + i;
		g_spawnPointsB[i].position.y = MAPSTARTPOSY + _offsetY + i;
	}
}

void InitialiseSpawnPointsInArea(Team _team, int _startingPtX, int _startingPtY, int _endingPtX, int _endingPtY)
{
	int index = 0;
	Point startingPt = SetVector(_startingPtX, _startingPtY);
	Point endingPt = SetVector(_endingPtX, _endingPtY);

	// Set first spawn point at top of the spawn area
	if (_team.color == teamA.color)
	{
		g_spawnPointsA[index++].position = startingPt;
		g_spawnPointsA[index].position = endingPt;

		double distance = GetDistanceBetweenPoints(startingPt, endingPt);
		double distBetweenPts = distance / (double)MAX_NUM_OF_SPAWN_POINTS - (double)index++;
		int count = 1;

		// Find the rough offset between spawn points that will make them spaced out
		while (index < MAX_NUM_OF_SPAWN_POINTS)
		{
			g_spawnPointsA[index++].position = SetVector(_startingPtX, _startingPtY + (int)distBetweenPts * count++);
		}
	}
	else
	{
		g_spawnPointsB[index++].position = startingPt;
		g_spawnPointsB[index].position = endingPt;

		double distance = GetDistanceBetweenPoints(startingPt, endingPt);
		double distBetweenPts = distance / (double)MAX_NUM_OF_SPAWN_POINTS - (double)index++;
		int count = 1;

		// Find the rough offset between spawn points that will make them spaced out
		while (index < MAX_NUM_OF_SPAWN_POINTS)
		{
			g_spawnPointsB[index++].position = SetVector(_startingPtX, _startingPtY + (int)distBetweenPts * count++);
		}
	}

	index = 0;
}

void CheckRespawnPlayers()
{
	if (player1.bIsDead)
		RespawnPlayer(&player1, ConvertFromSeconds(g_respawnInterval));
	if (player2.bIsDead)
		RespawnPlayer(&player2, ConvertFromSeconds(g_respawnInterval));
	if (player3.bIsDead)
		RespawnPlayer(&player3, ConvertFromSeconds(g_respawnInterval));
	if (player4.bIsDead)
		RespawnPlayer(&player4, ConvertFromSeconds(g_respawnInterval));
}

void CheckRespawnAI()
{
	for (int i = 0; i < MAXNUMBEROFAI; ++i)
	{
		if (!AI[i].b_isActive)
			continue;
		if (!AI[i].bIsDead)
			continue;

		RespawnAI(&AI[i], ConvertFromSeconds(g_respawnInterval));
	}
}

void CheckVulnerabilityPlayers()
{
	if (player1.bIsInvulnerable)
		SetInvulnerabilityPlayer(&player1, &(player1.respawnTimer), ConvertFromSeconds(invulnerability_duration));
	if (player2.bIsInvulnerable)
		SetInvulnerabilityPlayer(&player2, &(player2.respawnTimer), ConvertFromSeconds(invulnerability_duration));
	if (player3.bIsInvulnerable)
		SetInvulnerabilityPlayer(&player3, &(player3.respawnTimer), ConvertFromSeconds(invulnerability_duration));
	if (player4.bIsInvulnerable)
		SetInvulnerabilityPlayer(&player4, &(player4.respawnTimer), ConvertFromSeconds(invulnerability_duration));
}

void CheckVulnerabilityAI()
{
	for (int i = 0; i < MAXNUMBEROFAI; ++i)
	{
		if (!AI[i].b_isActive)
			continue;
		if (!AI[i].bIsInvulnerable)
			continue;
		SetInvulnerabilityAI(&AI[i], &(AI[i].respawnTimer), ConvertFromSeconds(g_respawnInterval));
	}
}

void RespawnPlayer(PlayerComponent *_player, float _spawnInterval)
{
	// Wait for countdown to respawn player
	if (CheckTimerReached(&(_player->respawnTimer), _spawnInterval))
	{
		int index = Random_Range(0, MAX_NUM_OF_SPAWN_POINTS - 1);

		// Based on which team this player is, spawn back at random spawn points for that team
		if (_player->teamColor == teamA.color)
		{
			while (g_spawnPointsA[index].isTaken)
			{
				index = Random_Range(0, MAX_NUM_OF_SPAWN_POINTS - 1);
			}

			_player->bodyPosition.x = g_spawnPointsA[index].position.x;
			_player->bodyPosition.y = g_spawnPointsA[index].position.y;
			g_spawnPointsA[index].isTaken = 1;
		}
		else
		{
			while (g_spawnPointsB[index].isTaken)
			{
				index = Random_Range(0, MAX_NUM_OF_SPAWN_POINTS - 1);
			}

			_player->bodyPosition.x = g_spawnPointsB[index].position.x;
			_player->bodyPosition.y = g_spawnPointsB[index].position.y;
			g_spawnPointsB[index].isTaken = 1;
		}

		// Reset Player Conditions
		ResetPlayer(_player);
	}
}

void RespawnAI(AIComponent *_AI, float _spawnInterval)
{
	// Wait for countdown to respawn player
	if (CheckTimerReached(&(_AI->respawnTimer), _spawnInterval))
	{
		int index = Random_Range(0, MAX_NUM_OF_SPAWN_POINTS - 1);

		// Based on which team this player is, spawn back at random spawn points for that team
		if (_AI->teamColor == teamA.color)
		{
			while (g_spawnPointsA[index].isTaken)
			{
				index = Random_Range(0, MAX_NUM_OF_SPAWN_POINTS - 1);
			}

			_AI->bodyPosition.x = g_spawnPointsA[index].position.x;
			_AI->bodyPosition.y = g_spawnPointsA[index].position.y;
			g_spawnPointsA[index].isTaken = 1;

		}
		else
		{
			while (g_spawnPointsB[index].isTaken)
			{
				index = Random_Range(0, MAX_NUM_OF_SPAWN_POINTS - 1);
			}

			if (!g_spawnPointsB[index].isTaken)
			{
				_AI->bodyPosition.x = g_spawnPointsB[index].position.x;
				_AI->bodyPosition.y = g_spawnPointsB[index].position.y;
				g_spawnPointsB[index].isTaken = 1;
			}
		}

		// Reset AI Conditions
		SpawnAI(_AI);
	}
}

void ResetSpawnPoint()
{
	for (int i = 0; i < MAX_NUM_OF_SPAWN_POINTS; ++i)
	{
		if (g_spawnPointsA[i].isTaken)
		{
			if (CheckTimerReached(&g_spawnPointsA[i].resetTimer, g_respawnInterval))
				g_spawnPointsA[i].isTaken = 0;
		}

		if (g_spawnPointsB[i].isTaken)
		{
			if (CheckTimerReached(&g_spawnPointsB[i].resetTimer, g_respawnInterval))
				g_spawnPointsB[i].isTaken = 0;
		}
	}
}

void SetInvulnerabilityPlayer(PlayerComponent *_player, float* _elapsedTime, float _duration)
{
	if (!CheckTimerReached(_elapsedTime, _duration))
	{
		// Set player to blink
		BlinkingAnimation(&(_player->isRendering), &(_player->invulnerableBlinkTimer));
	}
	else
	{
		_player->bIsInvulnerable = 0;
		_player->isRendering = 1;
	}
}

void SetInvulnerabilityAI(AIComponent *_AI, float* _elapsedTime, float _duration)
{
	if (!CheckTimerReached(_elapsedTime, _duration))
	{
		// Set ai to blink
		BlinkingAnimation(&(_AI->isRendering), &(_AI->invulnerableBlinkTimer));
	}
	else
	{
		_AI->bIsInvulnerable = 0;
		_AI->isRendering = 1;
	}
}

void RenderSpawnPoints()
{
	for (int i = 0; i < MAX_NUM_OF_SPAWN_POINTS; ++i)
	{
		Console_SetRenderBuffer_Char(g_spawnPointsA[i].position.x,
									 g_spawnPointsA[i].position.y,
									 g_spawnPointsA[i].charRepresentation);

		Console_SetRenderBuffer_Char(g_spawnPointsB[i].position.x,
								     g_spawnPointsB[i].position.y,
								     g_spawnPointsB[i].charRepresentation);
	}
}
