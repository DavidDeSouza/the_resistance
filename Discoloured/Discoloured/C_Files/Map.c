#include "../Header_Files/Globals.h"
#include "../Console/Console.h"

void MorphedWallToMap()
{
	for (int i = 0; i < NUMBEROFWALL; i++)
	{
		for (int x = wall[i].startPosition.x; x <= wall[i].endPosition.x; ++x)
		{
			for (int y = wall[i].startPosition.y; y <= wall[i].endPosition.y; ++y)
			{
				if (x == wall[i].startPosition.x || y == wall[i].startPosition.y
					|| x == wall[i].endPosition.x || y == wall[i].endPosition.y)
				{
					map[x][y].c = wall[i].c;
					map[x][y].color = wall[i].color;
				}
			}
		}
	}
}

void InitializeMapColor()
{	
	for (int x = MAPSTARTPOSX; x < MAPSIZEX ; ++x)
	{
		for (int y = MAPSTARTPOSY; y < MAPSIZEY; ++y)
		{
			map[x][y].position.x = x;
			map[x][y].position.y = y;
			map[x][y].c = ' ';
			map[x][y].color = BACKGROUND_WHITE_;
			
		}
	}
	MorphedWallToMap();
}

void RenderMapColour()
{
	for (int x = MAPSTARTPOSX; x < MAPSIZEX; ++x)
	{
		for (int y = MAPSTARTPOSY; y < MAPSIZEY; ++y)
		{
			Console_SetRenderBuffer_Colour_Char(map[x][y].position.x, map[x][y].position.y, map[x][y].c, map[x][y].color);
		}
	}

}

 

void RenderBackGroundWhite()
{
	for (int x = 0; x < WINDOWSIZEX; x++)
	{
		for (int y = 0; y < WINDOWSIZEY; y++)
		{
			Console_SetRenderBuffer_Colour_Char(x, y, ' ', BACKGROUND_WHITE_);
		}
	}
}

void RenderUIArea()
{
	for (int x = 0; x < WINDOWSIZEX; x++)
	{
		for (int y = 0; y < MAPSTARTPOSY; y++)
		{
			Console_SetRenderBuffer_Colour_Char(x, y, ' ', BACKGROUND_WHITE_);
		}
	}
}