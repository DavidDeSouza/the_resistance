/**********************************************************************************
* \file			Team.h
* \brief		base team mechanic that allows team assignment
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* The base team mechanic that allows team assignment

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.0 - Initial commit
**********************************************************************************/
#include "../Header_Files/Globals.h"
#include "../Header_Files/Player.h"
#include "../Header_Files/Team.h"
#include "../Header_Files/AI.h"
#include <stdio.h>
#include "../Header_Files/PlayerControlOptions.h"

void InitializeTeam()
{

	teamA.color = BACKGROUND_ORANGE | FOREGROUND_BLACK;
	teamA.shotColor = BACKGROUND_LIGHTORANGE | FOREGROUND_BLACK;

	teamA.numOfPlayers = 0;
	teamA.maxNumOfPlayer = MAXNUMBERINTEAM;
	teamA.ultimateCharge = 0;
	teamA.bUltimateCharged = 0;
	teamA.ultimateBarPosition = 1;
	teamA.blastDistance = ULTIMATE_BLASTRADIUS;
	teamA.numberOfKills = 0;

	teamB.color = BACKGROUND_TEAL | FOREGROUND_BLACK;
	teamB.shotColor = BACKGROUND_LIGHTTEAL | FOREGROUND_BLACK;

	teamB.numOfPlayers = 0;
	teamB.maxNumOfPlayer = MAXNUMBERINTEAM;
	teamB.ultimateCharge = 0;
	teamB.bUltimateCharged = 0;
	teamB.ultimateBarPosition = 79;
	teamB.blastDistance = ULTIMATE_BLASTRADIUS;
	teamB.numberOfKills = 0;
}



void InitializeAIIntoTeam(int _numOfAI, int _color,int _shotColor)
{
	int i = 0;
	int n = 0;
	while (i < MAXNUMBEROFAI)
	{

		if (!(&AI[i])->b_isActive)
		{
			(&AI[i])->b_isActive = 1;
			(&AI[i])->teamColor = _color;
			(&AI[i])->teamShotColor = _shotColor;
			++n;
		}		
		if (n >= _numOfAI)
		{
			break;
		}
		++i;
	}
}

void SetAIIntoTeams()
{
	int numOfAIToSpawn;
	for (int i = 0; i < (teamA.maxNumOfPlayer - teamA.numOfPlayers + teamB.maxNumOfPlayer - teamB.numOfPlayers); i++)
	{
		InitializeAI(&AI[i], BACKGROUND_WHITE_);
	}
	if (teamA.numOfPlayers < teamA.maxNumOfPlayer)
	{
		numOfAIToSpawn = teamA.maxNumOfPlayer - teamA.numOfPlayers;

		InitializeAIIntoTeam(numOfAIToSpawn, teamA.color,teamA.shotColor);
	}
	if (teamB.numOfPlayers < teamB.maxNumOfPlayer)
	{
		numOfAIToSpawn = teamB.maxNumOfPlayer - teamB.numOfPlayers;
		InitializeAIIntoTeam(numOfAIToSpawn, teamB.color,teamB.shotColor);
	}
}

void AssignPlayerToTeam(PlayerComponent *_player, enum Side _team)
{
	if (_team == A)
	{
		++teamA.numOfPlayers;

		_player->teamColor = teamA.color;
		_player->shotColor = teamA.shotColor;
	}
	else if (_team == B)
	{
		++teamB.numOfPlayers;
		_player->teamColor = teamB.color;
		_player->shotColor = teamB.shotColor;
	}
}