#include <Windows.h>
#include "../Header_Files/Render.h"
#include "../Header_Files/Globals.h"
#include "../Header_Files/AI.h"
#include "../Header_Files/Wall.h"
#include "../Header_Files/Initialise.h"
#include "../Console/Console.h"
#include "../Header_Files/Map.h"
#include "../Header_Files/Paint/Bullets.h"
#include "../Header_Files/Player.h"
#include "../Header_Files/UltimateAbility.h"
#include "../Header_Files/SpawnManager.h"
#include "../Clock/Clock.h"
#include <stdio.h>

extern char string[20];
char fps[50];
char teamAKills[20];
char teamBKills[20];

void RenderToConsole()
{
	RenderMapColour();
	RenderBullets();
	RenderUIArea();

	for (int i = 0; i < MAXNUMBEROFAI; i++)
	{
		if ((&AI[i])->b_isActive)
		{
			RenderAI(&AI[i]);
		}		
	}

	RenderPlayer(&player1);
	if (numberOfControllers == 1)
	{
		RenderPlayer(&player2);
	}
	else if (numberOfControllers == 2)
	{
		RenderPlayer(&player2);
		RenderPlayer(&player3);
	}
	else if (numberOfControllers == 3)
	{
		RenderPlayer(&player2);
		RenderPlayer(&player3);
		RenderPlayer(&player4);
	}

	RenderUlitmateBarTeamA(&teamA);
	RenderUlitmateBarTeamB(&teamB);
	RenderUltimateBasedOnTeam();

	if (player1.bIsDead)
		RenderPlayerTag(&player1);
	if (player2.bIsDead)
		RenderPlayerTag(&player2);
	if (player3.bIsDead)
		RenderPlayerTag(&player3);
	if (player4.bIsDead)
		RenderPlayerTag(&player4);

	for (int i = 0; i < MAXNUMBEROFAI; i++)
	{
		if (AI[i].b_isActive)
		{
			if (AI[i].bIsDead)
			{
				RenderAITag(&AI[i]);
			}
		}
	}

	RenderKillCounters();
}

void RenderControlsGraphic()
{
	Console_SetRenderBuffer_Colour_String(5, 14, "       -----               -----       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "      |     |             |     |      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "      |     |             |  ^  |      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "      |  W  |             |  |  |      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "      |     |             |     |      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 19, "      |     |             |     |      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 20, " ----- ----- -----   ----- ----- ----- ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 21, "|     |     |     | |     |     |     |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 22, "|     |     |     | |     |     |     |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 23, "|  A  |  S  |  D  | | <-  |  |  |  -> |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 24, "|     |     |     | |     |  v  |     |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 25, "|     |     |     | |     |     |     |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 26, " ----- ----- -----   ----- ----- ----- ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 28, "        MOVE               SHOOT       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 30, "           ----------------- ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 31, "          |                 |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 32, "          |      space      |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 33, "          |                 |", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 34, "           ----------------- ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 36, "                ULTIMATE     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);

	Console_SetRenderBuffer_Colour_String(48, 12, "                 SHOOT", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 13, "                  | ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 14, "   ____          _|__  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 15, "  /    \\--------/    \\", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 16, " /                 Y  \\ ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 17, " |                X B |   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 18, " |                 A  |   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 19, " (      __    __   |  )  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 20, " /     /  \\__/  \\  |  \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 21, "/      \\__/  \\__/  |   \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 22, "|      / |    | \\  |   |    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 23, "|      | |    | |  |   |    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 24, "|      / |    | \\  |   |   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 25, "\\_____/  |    |  \\_|___/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 26, "         |    |    |        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 27, "         |    | ULTIMATE", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 28, "         |    |    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 29, "         |    |_SHOOT DIRECTION ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 30, "         |                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(48, 31, "         |______MOVE          ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void PreGameRender(int _timer)
{
	Console_SetRenderBuffer_Colour_String(23, 23, "PAINT AS MUCH OF THE MAP WITH YOUR", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(23, 24, "TEAM'S COLOUR BEFORE TIME RUNS OUT", FOREGROUND_TEAL | BACKGROUND_WHITE_);

	switch (_timer)
	{
	case 1:
		Console_SetRenderBuffer_Colour_String(34, 11, "  1111111   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 12, " 1::::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 13, " 1::::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 14, "1:::::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 15, "1:::::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 16, "111:::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 17, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 18, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 19, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 20, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 21, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 22, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 25, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 26, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 27, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 28, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 29, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 30, "   1::::1   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 31, "111::::::111", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 32, "1::::::::::1", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 33, "1::::::::::1", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 34, "1::::::::::1", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 35, "1::::::::::1", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(34, 36, "111111111111", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		break;
	case 2:
		Console_SetRenderBuffer_Colour_String(30, 11, " 222222222222222    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 12, "2:::::::::::::::22  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 13, "2:::::::::::::::22  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 14, "2::::::222222:::::2 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 15, "2:::::2     2:::::2 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 16, "2222222     2:::::2 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 17, "            2:::::2 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 18, "            2:::::2 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 19, "            2:::::2 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 20, "            2:::::2 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 21, "         2222::::2  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 22, "     2222::::::22   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 25, "  22::::::::222     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 26, " 2:::::22222        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 27, "2:::::2             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 28, "2:::::2             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 29, "2:::::2             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 30, "2:::::2             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 31, "2:::::2       222222", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 32, "2:::::2       2::::2", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 33, "2::::::2222222:::::2", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 34, "2::::::::::::::::::2", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 35, "2::::::::::::::::::2", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 36, "22222222222222222222", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		break;
	case 3:
		Console_SetRenderBuffer_Colour_String(30, 11, " 333333333333333   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 12, "3:::::::::::::::33 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 13, "3:::::::::::::::33 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 14, "3::::::33333::::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 15, "3:::::3     3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 16, "3333333     3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 17, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 18, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 19, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 20, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 21, "    33333333:::::3 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 22, "    3::::::::::::3 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 25, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 26, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 27, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 28, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 29, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 30, "            3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 31, "3333333     3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 32, "3:::::3     3:::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 33, "3::::::33333::::::3", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 34, "3:::::::::::::::33 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 35, "3:::::::::::::::33 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 36, " 333333333333333   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		break;
	case 4:
		Console_SetRenderBuffer_Colour_String(31, 11, "       444444444  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 12, "      4::::::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 13, "      4::::::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 14, "     4:::::::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 15, "     4:::::::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 16, "    4::::44::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 17, "    4::::44::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 18, "   4::::4 4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 19, "   4::::4 4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 20, "  4::::4  4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 21, "  4::::4  4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 22, " 4::::4   4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 25, "4::::::::::::::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 26, "4::::::::::::::::4", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 27, "4444444444:::::444", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 28, "          4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 29, "          4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 30, "          4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 31, "          4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 32, "          4::::4  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 33, "        44::::::44", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 34, "        4::::::::4", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 35, "        4::::::::4", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(31, 36, "        4444444444", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		break;
	case 5:
		Console_SetRenderBuffer_Colour_String(30, 11, "555555555555555555 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 12, "5::::::::::::::::5 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 13, "5::::::::::::::::5 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 14, "5::::::::::::::::5 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 15, "5::::::::::::::::5 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 16, "5::::::::::::::::5 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 17, "5:::::555555555555 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 18, "5:::::5            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 19, "5:::::5            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 20, "5:::::5            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 21, "5:::::5555555555   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 22, "5:::::::::::::::5   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 25, "555555555555:::::5 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 26, "            5:::::5", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 27, "            5:::::5", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 28, "            5:::::5", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 29, "5555555     5:::::5", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 30, "5:::::5     5:::::5", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 31, "5::::::55555::::::5", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 32, " 55:::::::::::::55 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 33, " 55:::::::::::::55 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 34, "   55:::::::::55   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 35, "   55:::::::::55   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		Console_SetRenderBuffer_Colour_String(30, 36, "     555555555     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
		break;
	default:
		break;
	}

	if (player1.teamColor == teamA.color && player1.bIsActive)
	{
		Console_SetRenderBuffer_Colour_String(player1.bodyPosition.x + 2, player1.bodyPosition.y,
			"< PLAYER 1", FOREGROUND_PURPLE | BACKGROUND_WHITE_);
	}
	else
	{
		Console_SetRenderBuffer_Colour_String(player1.bodyPosition.x - 11, player1.bodyPosition.y,
			"PLAYER 1 >", FOREGROUND_PURPLE | BACKGROUND_WHITE_);
	}
	if (player2.teamColor == teamA.color && player2.bIsActive)
	{
		Console_SetRenderBuffer_Colour_String(player2.bodyPosition.x + 2, player2.bodyPosition.y,
			"< PLAYER 2", FOREGROUND_BLUE_ | BACKGROUND_WHITE_);
	}
	else
	{
		Console_SetRenderBuffer_Colour_String(player2.bodyPosition.x - 11, player2.bodyPosition.y,
			"PLAYER 2 >", FOREGROUND_BLUE_ | BACKGROUND_WHITE_);
	}
	if (player3.teamColor == teamA.color && player3.bIsActive)
	{
		Console_SetRenderBuffer_Colour_String(player3.bodyPosition.x + 2, player3.bodyPosition.y,
			"< PLAYER 3", FOREGROUND_GREEN_ | BACKGROUND_WHITE_);
	}
	else
	{
		Console_SetRenderBuffer_Colour_String(player3.bodyPosition.x - 11, player3.bodyPosition.y,
			"PLAYER 3 >", FOREGROUND_GREEN_ | BACKGROUND_WHITE_);
	}
	if (player4.teamColor == teamA.color && player4.bIsActive)
	{
		Console_SetRenderBuffer_Colour_String(player4.bodyPosition.x + 2, player4.bodyPosition.y,
			"< PLAYER 4", FOREGROUND_RED_ | BACKGROUND_WHITE_);
	}
	else
	{
		Console_SetRenderBuffer_Colour_String(player4.bodyPosition.x - 11, player4.bodyPosition.y,
			"PLAYER 4 >", FOREGROUND_RED_ | BACKGROUND_WHITE_);
	}
}

void RenderFPS()
{
	sprintf_s(fps, sizeof(fps)/sizeof(fps[0]), "Time Between Frames : %f", Clock_GetDeltaTime());
	Console_SetRenderBuffer_Colour_String(0, MAPSIZEY - 1, fps, FOREGROUND_BLACK | BACKGROUND_WHITE_);
}

void RenderKillCounters()
{
	sprintf_s(teamAKills, sizeof(teamAKills) / sizeof(teamAKills[0]), "Team A Kills: %d", teamA.numberOfKills);
	sprintf_s(teamBKills, sizeof(teamBKills) / sizeof(teamBKills[0]), "Team B Kills: %d", teamB.numberOfKills);
	Console_SetRenderBuffer_Colour_String(1, 2, teamAKills, FOREGROUND_BLACK | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(63, 2, teamBKills, FOREGROUND_BLACK | BACKGROUND_WHITE_);
}

void RenderDebugInfo(int _show)
{
	if (!_show) 
		return;

	RenderCollider(player1.collider);
	if (numberOfControllers == 1)
	{
		RenderCollider(player2.collider);
	}
	else if (numberOfControllers == 2)
	{
		RenderCollider(player2.collider);
		RenderCollider(player3.collider);
	}
	else if (numberOfControllers == 3)
	{
		RenderCollider(player2.collider);
		RenderCollider(player3.collider);
		RenderCollider(player4.collider);
	}

	// Render Collider of AI
	for (int i = 0; i < MAXNUMBEROFAI; ++i)
	{
		if (!AI[i].b_isActive)
			continue;

		RenderCollider(AI[i].collider);
	}

	// Render the FPS counter
	RenderFPS();
	// Render Spawn Points
	RenderSpawnPoints();
}

void RenderTeamAWinner()
{
	Console_SetRenderBuffer_Colour_String(6, 26, "88888888888 8888888888        d8888 888b     d888             d8888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(6, 27, "    888     888              d88888 8888b   d8888            d88888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(6, 28, "    888     888             d88P888 88888b.d88888           d88P888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(6, 29, "    888     8888888        d88P 888 888Y88888P888          d88P 888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(6, 30, "    888     888           d88P  888 888 Y888P 888         d88P  888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(6, 31, "    888     888          d88P   888 888  Y8P  888        d88P   888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(6, 32, "    888     888         d8888888888 888   '   888       d8888888888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(6, 33, "    888     8888888888 d88P     888 888       888      d88P     888 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void RenderTeamBWinner()
{
	Console_SetRenderBuffer_Colour_String(7, 26, "88888888888 8888888888        d8888 888b     d888      888888b.   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(7, 27, "    888     888              d88888 8888b   d8888      888  '88b  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(7, 28, "    888     888             d88P888 88888b.d88888      888  .88P  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(7, 29, "    888     8888888        d88P 888 888Y88888P888      8888888K.  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(7, 30, "    888     888           d88P  888 888 Y888P 888      888  'Y88b ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(7, 31, "    888     888          d88P   888 888  Y8P  888      888    888 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(7, 32, "    888     888         d8888888888 888   '   888      888   d88P ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(7, 33, "    888     8888888888 d88P     888 888       888      8888888P'  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}




//========================================================================
//	Author: Srikesh Sundaresan
//  
//	Description: Renders frames for the splashscreen
//	Inputs: NIL
//========================================================================

void DigipenLogoFrame_1()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 88", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 88", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void DigipenLogoFrame_2()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_3()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,88888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_4()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/ ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_5()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888. ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888. ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888. ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   88888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_6()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& .", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8                    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_7()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    ((( ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&. ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                          ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                          ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                          ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_8()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./88888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&88", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_9()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "         ,8#&888    8&&&&888    (((           ((", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_10()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "         ,88888&8,  88&8&&&&& . &88          .&&8 #&888888", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&888888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8    ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_11()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.      ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "         ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&8888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E                         ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_12()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "         ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T                         ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_13()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "         ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_14()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "         ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);

}

void DigipenLogoFrame_15()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, "  8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_16()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, " 8&888888,8                               &88.                                ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_17()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, " 8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_18()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, "  8888888,8             I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_19()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_20()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_21()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_22()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_23()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_24()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_25()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
}


void DigipenLogoFrame_26()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void DigipenLogoFrame_27()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void DigipenLogoFrame_28()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void DigipenLogoFrame_29()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void DigipenLogoFrame_30()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_31()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_32()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_33()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_34()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 22, "        ,88888888  888.  *88&*,&&&. ,8&&&&&*.&&8 #&88   888(,88&8&8  &&&&&8&8", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 23, "        ,88888888  888.   #8&& &88.#8&8/#88*.888 #&8888888&*8&8  #&8 88(88&&&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 24, "        ,8888888   888.  /888*,&88.888. /&8*.888 #&8&&&&8( #8&888888.8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 25, "        ,8#8888/   8888888888 ,&88./8888888*.888 #&88      .8&8/     8&&  88&", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 26, " 8888888,8         8&&&8&&8   ,8&&.  88&8&8*.&&8 #8&8        88888*  8&8  8&8", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 27, "8&888888,8                               &88.                                ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 28, "&&888888,8                           #&8888*                                 ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 29, "8&888888,8                                                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 30, " 8888888,8              I N S T I T U T E    O F   T E C H N O L O G Y       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_35()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 20, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                  ", FOREGROUND_LIGHTTEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,88888&8,  88&8&&&&& . &88          .&&8 #&88888888.                 ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void DigipenLogoFrame_36()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(2, 21, "        ,8#&888    8&&&&888    (((           ((( #&888&&88                   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 20, "                            '                                                ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 19, "'          P        d  '  '  b                                               '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 20, "                                                                           ' '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 21, "'       ' ' '                                            '             ''   ' ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 22, "                                                                    b   ' '   ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 23, "                                                                             '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 24, "'                                    '                '  '                    ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 25, "'  ' ' P'                            '                 ''      ' '  '       ' ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

}

void DigipenLogoFrame_37()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(1, 18, "                            '                                                 ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 19, "'          P        d  '  '  b                                               '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 20, "                             '          '                                  ' '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 21, "'       ' ' '                 '   b  ''  '   b'   '  ' ' '             ''   ' ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 22, " ''    ''                      ' '''      '   'b  '   '''           b   ' '   ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 23, " '        '                ' '       '                '''                    '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 24, "'''  '' '  ''              d    '  ' '                '  '                    ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 25, "'  ' ' P'             'Y '  P'  '    '                 ''      ' '  '       ' ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);

}

void DigipenLogoFrame_38()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(1, 18, "    ' b'   '                 '                                                 ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 19, "''   'Y'  Y'P        d  '  '  b                                               '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 20, " '     '              '       '          '                                  ' '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 21, "'       ' ' '   'b  '         '   b  ''  '   b'   '  ' ' ' ' ' 'd      ''   '  ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 22, " ''    ''    '        '        ' '''      '   'b  '   '''    d' P   b   ' '    ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 23, " '        '     '  ''      ' '       ''      '  ' ''   '''      ' ' '  '   '  '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 24, "'''  '' '  ''     '  Y '   d    '  ' '    '     P   b  '  '    Y b'    Y''b '  ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 25, "'  ' ' P'             'Y '  P'  '    '   '' '  '  'Y '  ''      ' '  '  'Y   ' ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
}

void DigipenLogoFrame_39()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(1, 18, "    8 b'   8                 '                                                 ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 19, "8'   'Y8  Y8P        d  '  '  b                                               8", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 20, " 8     8              8       8          '                                  ' '", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 21, "'       8 8 '   8b  8         '   b  88  '   b'   8  8 ' 8 ' 8 'd      ''   8 ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 22, " 8'    88    8        8        ' ''8      8   8b  8   8'8    d' P   b   ' 8   ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 23, " 8        8     8  '8      ' 8       8'      8  8 '8   8'8      8 8 8  8   '  8", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 24, "8'8  '' 8  8'     '  Y 8   d    8  ' 8    8     P   b  8  8    Y b'    Y''b 8  ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 25, "8  ' 8 P'             'Y 8  P'  '    '   8' 8  '  'Y 8  '8      ' 8  8  'Y   8 ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
}


void DigipenLogoFrame_40()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(1, 18, "8 8 8 b'   8          'd8    '          8                                      ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 19, "88   'Y8  Y8P        d  P  Y  b        8                                      8", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 20, " 8     8 8            8     8 8         88                                  8 8", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 21, "8      8 8 8 'd  8b  8         'd  b  88 8'd  b'   8  8     8 8 'd      'd   8 ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 22, " 88    88 8  8      8 8       d88''8   88  8   8b      8  8   d P  Y  d  '   8", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 23, " 8      8 8 8'Y 88  '8 8    8 8  8    88   8  8  8    88'8     8  8 8  8   8  8", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 24, "888  'd 8  88     X  Y 8b  d  PY8  ' 8  8 8    8P 8 b  8  8    Y b'    Y b  8 8", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(1, 25, "88 8 8 P'             'Y 8  P'  'Y  P' 8 8' 8  '  ' 8   '8      ' 8  8  'Y8  8 ", FOREGROUND_TEAL | FOREGROUND_INTENSITY | BACKGROUND_WHITE_);
}




void Render_CreditsLogo_Frame0()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame1()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame2()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame3()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame4()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame5()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame6()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame7()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame8()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsLogo_Frame9()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void Render_CreditsLogo_Frame10()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void Render_CreditsLogo_Frame11()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void Render_CreditsLogo_Frame12()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void Render_CreditsLogo_Frame13()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void Render_CreditsLogo_Frame14()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void Render_CreditsLogo_Frame15()
{
	RenderBackGroundWhite();
	Console_SetRenderBuffer_Colour_String(5, 13, "    BROUGHT 2 U BY", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 14, "    ___________________ ____ _________________________________________  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 15, "   /_  __/ /__________ / __ \\_________________   ____________________/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 16, "    / / / __ \\/ _ \\   / /_/ / _ \\/ ___/ / ___/  / __' / __ \\/ ___/ _ \\  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 17, "   / / / / / /  __/  / _  _/  __(__  / (__  /  / /_/ / / / / /__/  __/  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(5, 18, "  /_/ /_/ /_/\\___/  /_/ |_|\\___/____/_/____/__/\\__ _/_/ /_/\\___/\\___/   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}








void Render_CreditsData_Frame0()
{
}



void Render_CreditsData_Frame1()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                    __ __ __                                   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void Render_CreditsData_Frame2()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                       / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                     / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);


}


void Render_CreditsData_Frame3()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                     \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                               DAVID DE SOUZA                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}


void Render_CreditsData_Frame4()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                DAVID DE SOUZA                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                              JOLYN WONG KAIYI                           ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void Render_CreditsData_Frame5()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                               JOLYN WONG KAIYI                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                             SRIKESH SUNDARESAN                          ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame6()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                                JOLYN WONG KAIYI                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                              SRIKESH SUNDARESAN                          ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 34, "                             SEOW JUN HAO DARREN                         ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame7()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                                JOLYN WONG KAIYI                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                               SRIKESH SUNDARESAN                          ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 34, "                               SEOW JUN HAO DARREN                         ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame8()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                                JOLYN WONG KAIYI                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                               SRIKESH SUNDARESAN                          ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 34, "                               SEOW JUN HAO DARREN                         ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame9()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                                JOLYN WONG KAIYI                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                               SRIKESH SUNDARESAN                          ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 34, "                               SEOW JUN HAO DARREN                         ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame10()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                                JOLYN WONG KAIYI                           ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                               SRIKESH SUNDARESAN                          ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame11()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                       \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                                JOLYN WONG KAIYI                           ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame12()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                       / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                        \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 DAVID DE SOUZA                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame13()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                         __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                        / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                          \\__/_//_\\__/  \\__/\\__/\\_,_/_/_/_/               ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                                                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void Render_CreditsData_Frame14()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                          __  __        __                                   ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                          / /_/ /____   / /____ ___ ___ _                     ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                            / __/ _ / -_) / __/ -_/ _ `/  ' \\                   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}


void Render_CreditsData_Frame15()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                               __ __   __                                   ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame16()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "          _ ._   _  _  _  ___   __ -  _             ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame17()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "               _ ._   _   _ o  _.    _|_ _   _. ._   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame18()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "              _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame19()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                 _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                  |                                                      ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame20()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                 _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                    |                                                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                            INSTRUCTORS                              ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame21()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                 _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                    |                                                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                               INSTRUCTORS                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                              LOGAM ANDY TAN                             ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame22()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                 _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                    |                                                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                  INSTRUCTORS                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                              LOGAM ANDY TAN                             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                     GERBER YANNICK VINCENT                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame23()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                 _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                    |                                                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                  INSTRUCTORS                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                LOGAM ANDY TAN                             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                          GERBER YANNICK VINCENT                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame24()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                 _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                    |                                                      ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                  INSTRUCTORS                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                LOGAM ANDY TAN                             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                             GERBER YANNICK VINCENT                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame25()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                     _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                  INSTRUCTORS                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                LOGAM ANDY TAN                             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                             GERBER YANNICK VINCENT                        ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void Render_CreditsData_Frame26()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                  _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                     _> |_) (/_ (_ | (_| |    |_ | | (_| | | |< _>             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                        |                                                  ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                  INSTRUCTORS                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                 LOGAM ANDY TAN                             ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                                   GERBER YANNICK VINCENT                        ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame27()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                     _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                        _ |_ ( _  _ | (_  |    |_ | |   _      |< _>             ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 27, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                                                           ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                                       INSTRUCTORS                              ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                                         LOGAM ANDY TAN                             ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame28()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                       _ ._   _   _ o  _. |   _|_ |_   _. ._  |   _             ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}


void Render_CreditsData_Frame29()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame30()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame31()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame32()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                             JASON CHU  |  JOHN BAUER                        ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame33()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                             JASON CHU  |  JOHN BAUER                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 30, "                      SAMIR ABOU SAMRA  |  RAYMOND YAN                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame34()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                             JASON CHU  |  JOHN BAUER                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 30, "                      SAMIR ABOU SAMRA  |  RAYMOND YAN                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                        PRASANNA GHALI  |  MICHELE COMAIR                       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame35()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                             JASON CHU  |  JOHN BAUER                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 30, "                      SAMIR ABOU SAMRA  |  RAYMOND YAN                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                        PRASANNA GHALI  |  MICHELE COMAIR                       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                            XIN LI      |  ANGELA KUGLER                       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame36()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                             JASON CHU  |  JOHN BAUER                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 30, "                      SAMIR ABOU SAMRA  |  RAYMOND YAN                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                        PRASANNA GHALI  |  MICHELE COMAIR                       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                            XIN LI      |  ANGELA KUGLER                       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                      MELVIN GONSALVEZ  |  MEIGHAN MCKELVEY                       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame37()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                             JASON CHU  |  JOHN BAUER                        ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 30, "                      SAMIR ABOU SAMRA  |  RAYMOND YAN                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                        PRASANNA GHALI  |  MICHELE COMAIR                       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                            XIN LI      |  ANGELA KUGLER                       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                      MELVIN GONSALVEZ  |  MEIGHAN MCKELVEY                       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
}


void Render_CreditsData_Frame38()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 28, "                                   EXECUTIVES                              ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 29, "                             JASON CHU  |  JOHN BAUER                        ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 30, "                      SAMIR ABOU SAMRA  |  RAYMOND YAN                            ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                        PRASANNA GHALI  |  MICHELE COMAIR                       ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 32, "                            XIN LI      |  ANGELA KUGLER                       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 33, "                      MELVIN GONSALVEZ  |  MEIGHAN MCKELVEY                       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
}

void Render_CreditsData_Frame39()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_TEAL | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

	Console_SetRenderBuffer_Colour_String(0, 30, "                      SAMIR ABOU SAMRA  |  RAYMOND YAN                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 31, "                        PRASANNA GHALI  |  MICHELE COMAIR                       ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame40()
{
	Console_SetRenderBuffer_Colour_String(0, 25, "                                   PRESIDENT                               ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);
	Console_SetRenderBuffer_Colour_String(0, 26, "                                 CLAUDE  COMAIR                            ", FOREGROUND_ORANGE | BACKGROUND_WHITE_);

}

void Render_CreditsData_Frame48()
{

}

