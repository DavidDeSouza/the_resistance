/**********************************************************************************
* \file			GameTime.h
* \brief		handles timer for the game
* \author		Seow Jun Hao Darren
* \version		1.0
* \date			2019
*
* An time related extension library made from Yannick Clock.h to check for time

* \note			Course: GAM100
* \copyright	Copyright (c) 2019 DigiPen Institute of Technology. Reproduction
				or disclosure of this file or its contents without the prior
				written consent of DigiPen Institute of Technology is prohibited.
*
* \Revisions	1.1 - Added new function ConvertFromMinutes and convertFromSeconds
					  by Jolyn
				1.0 - Initial commit
**********************************************************************************/

#include "../Libraries/Point.h"
#include "../Libraries/Draw.h"
#include "../Header_Files/Globals.h"


void InitializeWall(int startPosX,int startPosY,int endPosX,int endPosY,int arrayIndex)
{
	wall[arrayIndex].startPosition.x = startPosX;
	wall[arrayIndex].startPosition.y = startPosY;
	wall[arrayIndex].endPosition.x = endPosX;
	wall[arrayIndex].endPosition.y = endPosY;
	wall[arrayIndex].color = BACKGROUND_WHITE_| FOREGROUND_WHITE_;
	wall[arrayIndex].c = 'W';
	
}

void RenderWall()
{
	for (int i = 0; i < NUMBEROFWALL; i++)
	{
		DrawEmptyRectWithColor(wall[i].startPosition.x,
			wall[i].startPosition.y,
			wall[i].endPosition.x,
			wall[i].endPosition.y,
			' ',
			wall[i].color);
	}
}
