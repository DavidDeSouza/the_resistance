#define _CRT_SECURE_NO_WARNINGS
#include "../Header_Files/HandleController.h"
#include "../Header_Files/Paint/Bullets.h"
#include "../Console/Console.h"
#include <stdio.h>
#include "../Header_Files/Globals.h"
#include "../Header_Files/UltimateAbility.h"
#include "../Header_Files/DeltaTime.h"
#include "../Header_Files/GameStateMachine.h"
#include "../Random/Random.h"
#include "../Header_Files/PlayerControlOptions.h"
#include "../Header_Files/Play.h"
#include "../Header_Files/Audio.h"
#include <string.h>
SDL_Event ev;

PlayerComponent player2;
PlayerComponent player3;
PlayerComponent player4;

int InitialiseControllers()
{
	int numberOfConnectedControllers = 0;
	SDL_Init(SDL_INIT_GAMECONTROLLER);
	SDL_GameControllerEventState(SDL_ENABLE);

	joystick1 = SDL_GameControllerOpen(0);
	joystick2 = SDL_GameControllerOpen(1);
	joystick3 = SDL_GameControllerOpen(2);

	if (joystick1 != NULL)
	{
		numberOfConnectedControllers++;
	}
	if (joystick2 != NULL)
	{
		numberOfConnectedControllers++;
	}
	if (joystick3 != NULL)
	{
		numberOfConnectedControllers++;
	}
	return numberOfConnectedControllers;
}

void MoveControllerPlayers(PlayerComponent *_player)
{
	if (_player->bIsDead || preGamePause)
		return;

	if (CheckEuler(&_player->Euler, _player->speed))
	{
		if ((_player->direction == LEFT && map[_player->bodyPosition.x - 1][_player->bodyPosition.y].c != 'W')
			|| (_player->direction == RIGHT && map[_player->bodyPosition.x + 1][_player->bodyPosition.y].c != 'W')
			|| (_player->direction == UP && map[_player->bodyPosition.x][_player->bodyPosition.y - 1].c != 'W')
			|| (_player->direction == DOWN && map[_player->bodyPosition.x][_player->bodyPosition.y + 1].c != 'W'))
		{
			_player->bodyPosition.x += _player->xDir;
			_player->bodyPosition.y += _player->yDir;
		}

		
		RenderPlayerTrails(_player);
		UpdatePlayerCollider(_player);
	}
}

void HandleController1JoyStick()
{
	//left stick
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTX)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			if (CurrentState == State_Team_Options)
			{
				if (player2.isReady == Ready_False)
				{
					if (side[1] == B)
					{
						side[1] = A;
						ForeGroundColor[1] = FOREGROUND_BLUE_;
						BackGroundColor[1] = BACKGROUND_ORANGE;
						strncpy(player2String, "       Player 2      >", sizeof(player1String) / sizeof(player1String[0]));
					}
				}
			}
			player2.xDir = -1;
			player2.direction = LEFT;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			if (CurrentState == State_Team_Options)
			{
				if (player2.isReady == Ready_False)
				{
					if (side[1] == A)
					{
						side[1] = B;
						ForeGroundColor[1] = FOREGROUND_BLUE_;
						BackGroundColor[1] = BACKGROUND_TEAL;
						strncpy(player2String, "<      Player 2       ", sizeof(player1String) / sizeof(player1String[0]));
					}
				}
			}
			player2.xDir = 1;
			player2.direction = RIGHT;
		}
		else
		{
			player2.xDir = 0;
		}
	}
	else if (ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTY)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player2.yDir = -1;
			player2.direction = UP;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			player2.yDir = 1;
			player2.direction = DOWN;
		}
		else
		{
			player2.yDir = 0;
		}
	}

	//right stick
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player2.shootingDirection = LEFT;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			player2.shootingDirection = RIGHT;
		}
	}
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTY)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player2.shootingDirection = UP;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			player2.shootingDirection = DOWN;
		}
	}

	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERRIGHT || ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERLEFT)
	{
		if (ev.caxis.value > DEADZONE)
		{
			if (CurrentState == State_Game)
			{
				if (!player2.bIsDead)
				{
					ShootingBullets(player2.bodyPosition, player2.shootingDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX),
						player2.teamColor, player2.shotColor, 10.0f, &player2.shootTimer);
					if (CheckTimerReached(&player2.shootTimer, 10.0f))
						PlayAudio(audio.shot);
				}
			}
		}
	}
	
}

void HandleController2JoyStick()
{
	//left stick
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTX)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			if (CurrentState == State_Team_Options)
			{
				if (player3.isReady == Ready_False)
				{
					if (side[2] == B)
					{
						side[2] = A;
						ForeGroundColor[2] = FOREGROUND_GREEN_;
						BackGroundColor[2] = BACKGROUND_ORANGE;
						strncpy(player3String, "       Player 3      >", sizeof(player1String) / sizeof(player1String[0]));
					}
				}
			}
			player3.xDir = -1;
			player3.direction = LEFT;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			if (CurrentState == State_Team_Options)
			{
				if (player3.isReady == Ready_False)
				{
					if (side[2] == A)
					{
						side[2] = B;
						ForeGroundColor[2] = FOREGROUND_GREEN_;
						BackGroundColor[2] = BACKGROUND_TEAL;
						strncpy(player3String, "<      Player 3       ", sizeof(player1String) / sizeof(player1String[0]));
					}
				}
			}
			player3.xDir = 1;
			player3.direction = RIGHT;
		}
		else
		{
			player3.xDir = 0;
		}
	}
	else if (ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTY)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player3.yDir = -1;
			player3.direction = UP;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			player3.yDir = 1;
			player3.direction = DOWN;
		}
		else
		{
			player3.yDir = 0;
		}
	}

	//right stick
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player3.shootingDirection = LEFT;
		}
		if (ev.caxis.value > DEADZONE)
		{
			player3.shootingDirection = RIGHT;
		}
	}
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTY)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player3.shootingDirection = UP;
		}
		if (ev.caxis.value > DEADZONE)
		{
			player3.shootingDirection = DOWN;
		}
	}

	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERRIGHT || ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERLEFT)
	{
		if (ev.caxis.value > DEADZONE)
		{
			if (CurrentState == State_Game)
			{
				if (!player3.bIsDead)
				{
					ShootingBullets(player3.bodyPosition, player3.shootingDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX),
						player3.teamColor, player3.shotColor, 10.0f, &player3.shootTimer);
					if (CheckTimerReached(&player3.shootTimer, 10.0f))
						PlayAudio(audio.shot);
				}
			}
		}
	}
}

void HandleController3JoyStick()
{
	//left stick
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTX)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			if (CurrentState == State_Team_Options)
			{
				if (player4.isReady == Ready_False)
				{
					if (side[3] == B)
					{
						side[3] = A;
						ForeGroundColor[3] = FOREGROUND_RED_;
						BackGroundColor[3] = BACKGROUND_ORANGE;
						strncpy(player4String, "       Player 4      >", sizeof(player1String) / sizeof(player1String[0]));
					}
				}
			}
			player4.xDir = -1;
			player4.direction = LEFT;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			if (CurrentState == State_Team_Options)
			{
				if (player4.isReady == Ready_False)
				{
					if (side[3] == A)
					{
						side[3] = B;
						ForeGroundColor[3] = FOREGROUND_RED_;
						BackGroundColor[3] = BACKGROUND_TEAL;
						strncpy(player4String, "<      Player 4       ", sizeof(player1String) / sizeof(player1String[0]));
					}
				}
			}
			player4.xDir = 1;
			player4.direction = RIGHT;
		}
		else
		{
			player4.xDir = 0;
		}
	}
	else if (ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTY)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player4.yDir = -1;
			player4.direction = UP;
		}
		else if (ev.caxis.value > DEADZONE)
		{
			player4.yDir = 1;
			player4.direction = DOWN;
		}
		else
		{
			player4.yDir = 0;
		}
	}

	//right stick
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player4.shootingDirection = LEFT;
		}
		if (ev.caxis.value > DEADZONE)
		{
			player4.shootingDirection = RIGHT;
		}
	}
	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTY)
	{
		if (ev.caxis.value < -DEADZONE)
		{
			player4.shootingDirection = UP;
		}
		if (ev.caxis.value > DEADZONE)
		{
			player4.shootingDirection = DOWN;
		}
	}

	if (ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERRIGHT || ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERLEFT)
	{
		if (ev.caxis.value > DEADZONE)
		{
			if (CurrentState == State_Game)
			{
				if (!player4.bIsDead)
				{
					ShootingBullets(player4.bodyPosition, player4.shootingDirection, Random_Range(SHOT_RANGE_MIN, SHOT_RANGE_MAX),
						player4.teamColor, player4.shotColor, 10.0f, &player4.shootTimer);
					if (CheckTimerReached(&player4.shootTimer, 10.0f))
						PlayAudio(audio.shot);
				}
			}
		}
	}
}

void HandleController1Buttons()
{
	if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_A)
	{
		switch (CurrentState)
		{
		case State_Team_Options:
			if (player2.isReady == Ready_True)
			{
				player2.isReady = Ready_False;
			}
			else
			{
				player2.isReady = Ready_True;
			}
			break;
		case State_Game:
			if (player2.teamColor == teamA.color)
			{
				UseUltimate(&teamA, &player2);
			}
			else
			{
				UseUltimate(&teamB, &player2);
			}
			break;
		}
	}
}

void HandleController2Buttons()
{
	if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_A)
	{
		switch (CurrentState)
		{
		case State_Team_Options:
			if (player3.isReady == Ready_True)
			{
				player3.isReady = Ready_False;
			}
			else
			{
				player3.isReady = Ready_True;
			}
			break;
		case State_Game:
			if (player3.teamColor == teamA.color)
			{
				UseUltimate(&teamA, &player3);
			}
			else
			{
				UseUltimate(&teamB, &player3);
			}
			break;
		}
	}
}

void HandleController3Buttons()
{
	if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_A)
	{
		switch (CurrentState)
		{
		case State_Team_Options:
			if (player4.isReady == Ready_True)
			{
				player4.isReady = Ready_False;
			}
			else
			{
				player4.isReady = Ready_True;
			}
			break;
		case State_Game:
			if (player4.teamColor == teamA.color)
			{
				UseUltimate(&teamA, &player4);
			}
			else
			{
				UseUltimate(&teamB, &player4);
			}
			break;
		}
	}
}

void HandleAllControllerInputs()
{
	while (SDL_PollEvent(&ev) != 0)
	{
		if (ev.type == SDL_CONTROLLERAXISMOTION)
		{
			//check if controller1
			if (ev.caxis.which == SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(joystick1)))
			{
				HandleController1JoyStick();
			}

			//check if controller2
			if (ev.caxis.which == SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(joystick2)))
			{
				HandleController2JoyStick();
			}

			//check if controller3
			if (ev.caxis.which == SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(joystick3)))
			{
				HandleController3JoyStick();
			}
		}
		if (ev.type == SDL_CONTROLLERBUTTONDOWN)
		{
			//check if controller1
			if (ev.caxis.which == SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(joystick1)))
			{
				HandleController1Buttons();
			}

			//check if controller2
			if (ev.caxis.which == SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(joystick2)))
			{
				HandleController2Buttons();
			}

			//check if controller3
			if (ev.caxis.which == SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(joystick3)))
			{
				HandleController3Buttons();
			}
		}
	}
}

void UnInitialiseControllers()
{
	joystick1 = NULL;
	joystick2 = NULL;
	joystick3 = NULL;
	SDL_Quit();
	numberOfControllers = 0;
}