#include "../Header_Files/Audio.h"

//========================================================================
//	author: David De Souza
//  load audio files from directory
//========================================================================
void GetAudioFiles()
{
	FMOD_System_CreateSound(audioSystem, "sounds/pop.ogg", FMOD_DEFAULT, 0, &audio.shot);
	FMOD_System_CreateSound(audioSystem, "sounds/menuBgm.ogg", FMOD_DEFAULT, 0, &audio.menuBgm);
	FMOD_System_CreateSound(audioSystem, "sounds/gameBgm.ogg", FMOD_DEFAULT, 0, &audio.gameBgm);
	FMOD_System_CreateSound(audioSystem, "sounds/splash.ogg", FMOD_DEFAULT, 0, &audio.splash);
	FMOD_System_CreateSound(audioSystem, "sounds/whislte.ogg", FMOD_DEFAULT, 0, &audio.whistle);
	FMOD_System_CreateSound(audioSystem, "sounds/drumRoll.ogg", FMOD_DEFAULT, 0, &audio.drumRoll);
}

void InitialiseAudio()
{
	FMOD_System_Create(&audioSystem);
	FMOD_System_Init(audioSystem, 32, FMOD_INIT_NORMAL, 0);
	GetAudioFiles();
}

void PlayAudio(FMOD_SOUND *_sound)
{
	if (_sound == audio.shot)
	{
		FMOD_Sound_SetMode(audio.shot, FMOD_LOOP_OFF);
		FMOD_System_PlaySound(audioSystem, _sound, channelGroup, 0, &channel.sfxBullets);
	}

	if (_sound == audio.menuBgm)
	{
		FMOD_Sound_SetMode(audio.menuBgm, FMOD_LOOP_NORMAL);
		FMOD_System_PlaySound(audioSystem, _sound, channelGroup, 0, &channel.bgm);
	}

	if (_sound == audio.gameBgm)
	{
		FMOD_Sound_SetMode(audio.gameBgm, FMOD_LOOP_NORMAL);
		FMOD_System_PlaySound(audioSystem, _sound, channelGroup, 0, &channel.bgm);
	}

	if (_sound == audio.splash)
	{
		FMOD_Sound_SetMode(audio.splash, FMOD_LOOP_OFF);
		FMOD_System_PlaySound(audioSystem, _sound, channelGroup, 0, &channel.sfx);
	}

	if (_sound == audio.whistle)
	{
		FMOD_Sound_SetMode(audio.whistle, FMOD_LOOP_OFF);
		FMOD_System_PlaySound(audioSystem, _sound, channelGroup, 0, &channel.sfx);
	}

	if (_sound == audio.drumRoll)
	{
		FMOD_Sound_SetMode(audio.drumRoll, FMOD_LOOP_OFF);
		FMOD_System_PlaySound(audioSystem, _sound, channelGroup, 0, &channel.bgm);
	}
}

void PauseAudio(FMOD_CHANNEL *_channel)
{
	FMOD_Channel_Stop(_channel);
}

void CloseAudio()
{
	FMOD_System_Close(audioSystem);
}