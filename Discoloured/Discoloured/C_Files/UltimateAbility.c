#include "../Header_Files/UltimateAbility.h"
#include "../Console/Console.h"
#include "../Header_Files/Paint/Bullets.h"
#include "../Libraries/Helpers.h"
#include "../Random/Random.h"
#include "../Header_Files/Audio.h"

//========================================================================
//	variables 
//========================================================================
int ultiBarAFlicker = 0;
int ultiBarBFlicker = 0;
float ultiBarAFlickerDeltaTime = 0.0f;
float ultiBarBFlickerDeltaTime = 0.0f;
Point f_up, f_down, f_left, f_right, f_UpLeft, f_UpRight, f_DownLeft, f_DownRight;

void ChargeUltimate(Team *_team)
{
	if (_team->ultimateCharge < 100)
	{
		_team->ultimateCharge += _team->numberOfKills / ULTIMATE_CHARGE_RATE;
		_team->bUltimateCharged = 0;
	}
	else
	{
		_team->bUltimateCharged = 1;
	}
}

void UseUltimate(Team *_team, PlayerComponent *_player)
{
	if (_player->bIsDead) 
		return;

	if (_team->bUltimateCharged)
	{
		_player->bUsedUltimate = 1;
		_team->blastDistance = 0;
		_team->bUltimateCharged = 0;
		_team->ultimateCharge = 0;
		f_up = _player->bodyPosition;
		f_down = _player->bodyPosition;
		f_left = _player->bodyPosition;
		f_right = _player->bodyPosition;
		f_UpLeft = _player->bodyPosition;
		f_UpRight = _player->bodyPosition;
		f_DownLeft = _player->bodyPosition;
		f_DownRight = _player->bodyPosition;
		KillEverythingInRadius_Player(_player);
		PlayAudio(audio.splash);
	}
}

void AIUseUltimate(Team *_team, AIComponent *_AI)
{
	if (_AI->bIsDead)
		return;

	if (_team->bUltimateCharged)
	{
		_AI->b_canUseUltimate = 1;
		_team->blastDistance = 0;
		_team->bUltimateCharged = 0;
		_team->ultimateCharge = 0;
		f_up = _AI->bodyPosition;
		f_down = _AI->bodyPosition;
		f_left = _AI->bodyPosition;
		f_right = _AI->bodyPosition;
		f_UpLeft = _AI->bodyPosition;
		f_UpRight = _AI->bodyPosition;
		f_DownLeft = _AI->bodyPosition;
		f_DownRight = _AI->bodyPosition;
		KillEverythingInRadius_AI(_AI);
		PlayAudio(audio.splash);
	}
}
void RenderUlitmateBarTeamA(Team *_team)
{
	if (_team->ultimateCharge > 0 && _team->ultimateCharge < 20)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition, 1, "Ultimate", _team->color);
	}
	else if (_team->ultimateCharge >= 20 && _team->ultimateCharge < 40)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 8, 1, " 40%", _team->color);
	}
	else if (_team->ultimateCharge >= 40 && _team->ultimateCharge < 60)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 8, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 12, 1, " 60%", _team->color);
	}
	else if (_team->ultimateCharge >= 60 && _team->ultimateCharge < 80)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 8, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 12, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 16, 1, " 80%", _team->color);
	}
	else if (_team->ultimateCharge >= 80 && _team->ultimateCharge < 100)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 8, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 12, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 16, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition + 20, 1, " 99%", _team->color);
	}
	else if (_team->ultimateCharge >= 100)
	{
		if (CheckTimerReached(&ultiBarAFlickerDeltaTime, 100.0f))
		{
			if (ultiBarAFlicker)
			{
				ultiBarAFlicker = 0;
			}
			else
			{
				ultiBarAFlicker = 1;
			}
		}
		if (ultiBarAFlicker)
		{
			Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition, 1, "Ultimate            100%", BACKGROUND_RED_ | FOREGROUND_BLACK);
		}
		else
		{
			Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition, 1, "Ultimate            100%", BACKGROUND_ORANGE | FOREGROUND_BLACK);
		}
	}
}

void RenderUlitmateBarTeamB(Team *_team)
{
	if (_team->ultimateCharge > 0 && _team->ultimateCharge < 20)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 7, 1, "Ultimate", _team->color);
	}
	else if (_team->ultimateCharge >= 20 && _team->ultimateCharge < 40)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 7, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 11, 1, "40% ", _team->color);
	}
	else if (_team->ultimateCharge >= 40 && _team->ultimateCharge < 60)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 7, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 11, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 15, 1, "60% ", _team->color);
	}
	else if (_team->ultimateCharge >= 60 && _team->ultimateCharge < 80)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 7, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 11, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 15, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 19, 1, "80% ", _team->color);
	}
	else if (_team->ultimateCharge >= 80 && _team->ultimateCharge < 100)
	{
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 7, 1, "Ultimate", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 11, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 15, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 19, 1, "    ", _team->color);
		Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 23, 1, "99% ", _team->color);
	}
	else if (_team->ultimateCharge >= 100)
	{
		if (CheckTimerReached(&ultiBarBFlickerDeltaTime, 100.0f))
		{
			if (ultiBarBFlicker)
			{
				ultiBarBFlicker = 0;
			}
			else
			{
				ultiBarBFlicker = 1;
			}
		}
		if (ultiBarBFlicker)
		{
			Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 23, 1, "100%            Ultimate", BACKGROUND_TEAL | FOREGROUND_BLACK);
		}
		else
		{
			Console_SetRenderBuffer_Colour_String(_team->ultimateBarPosition - 23, 1, "100%            Ultimate", BACKGROUND_RED_ | FOREGROUND_BLACK);
		}
	}
}

void RenderUltimateEffects(Team *_team, PlayerComponent *_player)
{
	int splatterChance;
	Point current;

	for (int x = _player->bodyPosition.x - ULTIMATE_BLASTRADIUS; x < _player->bodyPosition.x + ULTIMATE_BLASTRADIUS; x++)
	{
		for (int y = _player->bodyPosition.y - ULTIMATE_BLASTRADIUS; y < _player->bodyPosition.y + ULTIMATE_BLASTRADIUS; y++)
		{
			if (x > MAPSTARTPOSX && x < MAPSIZEX && y > MAPSTARTPOSY && y < MAPSIZEY)
			{
				if (map[x][y].c != 'W')
				{
					current.x = x;
					current.y = y;
					if (GetDistanceBetweenPoints(_player->bodyPosition, current) <= (double)ULTIMATE_BLASTRADIUS)
					{
						splatterChance = Random_Range(0, 1);
						if (splatterChance)
							map[x][y].color = _player->shotColor;
					}
				}
			}
		}
	}
	
	_player->bUsedUltimate = 0;
}

void RenderAIUltimateEffects(Team *_team, AIComponent *_AI)
{
	int splatterChance;
	Point current;

	for (int x = _AI->bodyPosition.x - ULTIMATE_BLASTRADIUS; x < _AI->bodyPosition.x + ULTIMATE_BLASTRADIUS; x++)
	{
		for (int y = _AI->bodyPosition.y - ULTIMATE_BLASTRADIUS; y < _AI->bodyPosition.y + ULTIMATE_BLASTRADIUS; y++)
		{
			if (x > MAPSTARTPOSX && x < MAPSIZEX && y > MAPSTARTPOSY && y < MAPSIZEY)
			{
				if (map[x][y].c != 'W')
				{
					current.x = x;
					current.y = y;
					if (GetDistanceBetweenPoints(_AI->bodyPosition, current) <= (double)ULTIMATE_BLASTRADIUS)
					{
						splatterChance = Random_Range(0, 1);
						if (splatterChance)
							map[x][y].color = _AI->shotColor;
					}
				}
			}
		}
	}

	_AI->b_canUseUltimate = 0;
}

void RenderUltimateBasedOnTeam()
{
	if (player1.bUsedUltimate)
	{
		if (player1.teamColor == teamA.color)
		{
			RenderUltimateEffects(&teamA, &player1);
		}
		else
		{
			RenderUltimateEffects(&teamB, &player1);
		}
	}
	if (player2.bUsedUltimate)
	{
		if (player2.teamColor == teamA.color)
		{
			RenderUltimateEffects(&teamA, &player2);
		}
		else
		{
			RenderUltimateEffects(&teamB, &player2);
		}
	}
	if (player3.bUsedUltimate)
	{
		if (player3.teamColor == teamA.color)
		{
			RenderUltimateEffects(&teamA, &player3);
		}
		else
		{
			RenderUltimateEffects(&teamB, &player3);
		}
	}
	if (player4.bUsedUltimate)
	{
		if (player4.teamColor == teamA.color)
		{
			RenderUltimateEffects(&teamA, &player4);
		}
		else
		{
			RenderUltimateEffects(&teamB, &player4);
		}
	}
	for (int i = 0; i < MAXNUMBEROFAI; i++)
	{
		if (AI[i].b_isActive)
		{
			if (AI[i].b_canUseUltimate) 
			{
				if (AI[i].teamColor == teamA.color)
				{
					RenderAIUltimateEffects(&teamA, &AI[i]);
				}
				else if (AI[i].teamColor == teamB.color)
				{
					RenderAIUltimateEffects(&teamB, &AI[i]);
				}
			}
			
		}
	}
}

void KillEverythingInRadius_Player(PlayerComponent *_player)
{
	int kills = 0;

	for (int i = 0; i < MAXNUMBEROFAI; i++)
	{
		if (AI[i].b_isActive && !AI[i].bIsDead && !AI[i].bIsInvulnerable)
		{
			if (GetDistanceBetweenPoints(_player->bodyPosition, AI[i].bodyPosition) <= ULTIMATE_BLASTRADIUS &&
				AI[i].teamColor != _player->teamColor)
			{
				AI[i].bIsDead = 1;
				kills++;
			}
		}
	}
	if (_player != &player1)
	{
		if (!_player->bIsDead && !_player->bIsInvulnerable)
		{
			if (_player->teamColor != player1.teamColor && !player1.bIsDead && !player1.bIsInvulnerable && player1.bIsActive)
			{
				player1.bIsDead = 1;
				kills++;
			}
		}
	}
	if (_player != &player2)
	{
		if (!_player->bIsDead && !_player->bIsInvulnerable)
		{
			if (_player->teamColor != player2.teamColor && !player2.bIsDead && !player2.bIsInvulnerable && player2.bIsActive)
			{
				player2.bIsDead = 1;
				kills++;
			}
		}
	}
	if (_player != &player3)
	{
		if (!_player->bIsDead && !_player->bIsInvulnerable)
		{
			if (_player->teamColor != player3.teamColor && !player3.bIsDead && !player3.bIsInvulnerable && player3.bIsActive)
			{
				player3.bIsDead = 1;
				kills++;
			}
		}
	}
	if (_player != &player4)
	{
		if (!_player->bIsDead && !_player->bIsInvulnerable)
		{
			if (_player->teamColor != player4.teamColor && !player4.bIsDead && !player4.bIsInvulnerable && player4.bIsActive)
			{
				player4.bIsDead = 1;
				kills++;
			}
		}
	}

	if (_player->teamColor == teamA.color)
	{
		teamA.numberOfKills += kills;
	}
	else
	{
		teamB.numberOfKills += kills;
	}
}

void KillEverythingInRadius_AI(AIComponent *_AI)
{
	int kills = 0;
	for (int i = 0; i < MAXNUMBEROFAI; i++)
	{
		if (!AI[i].b_isActive)
		{
			continue;
		}

		if (AI[i].bIsDead || AI[i].bIsInvulnerable)
			continue;

		if (GetDistanceBetweenPoints(AI[i].bodyPosition, _AI->bodyPosition) <= ULTIMATE_BLASTRADIUS &&
			AI[i].teamColor != _AI->teamColor)
		{
			AI[i].bIsDead = 1;
			kills++;
		}
	}

	if (GetDistanceBetweenPoints(player1.bodyPosition, _AI->bodyPosition) <= ULTIMATE_BLASTRADIUS &&
		player1.teamColor != _AI->teamColor)
	{
		if (!player1.bIsDead && !player1.bIsInvulnerable)
		{
			player1.bIsDead = 1;
			kills++;
		}
	}
	if (GetDistanceBetweenPoints(player2.bodyPosition, _AI->bodyPosition) <= ULTIMATE_BLASTRADIUS &&
		player2.teamColor != _AI->teamColor)
	{
		if (!player2.bIsDead && !player2.bIsInvulnerable)
		{
			player2.bIsDead = 1;
			kills++;
		}
	}
	if (GetDistanceBetweenPoints(player3.bodyPosition, _AI->bodyPosition) <= ULTIMATE_BLASTRADIUS &&
		player3.teamColor != _AI->teamColor)
	{
		if (!player3.bIsDead && !player3.bIsInvulnerable)
		{
			player3.bIsDead = 1;
			kills++;
		}
	}
	if (GetDistanceBetweenPoints(player4.bodyPosition, _AI->bodyPosition) <= ULTIMATE_BLASTRADIUS &&
		player4.teamColor != _AI->teamColor)
	{
		if (!player4.bIsDead && !player4.bIsInvulnerable)
		{
			player4.bIsDead = 1;
			kills++;
		}
	}
	if (_AI->teamColor == teamA.color)
	{
		teamA.numberOfKills += kills;
	}
	else
	{
		teamB.numberOfKills += kills;
	}
}