#include "../../Header_Files/Paint/Bullets.h"
#include "../../Header_Files/Globals.h"
#include "../../Header_Files/DeltaTime.h"
#include "../../Libraries/GameTime.h"
#include "../../Console/Console.h"
#include "../../Libraries/Collision.h"
#include "../../Random/Random.h"
#include "../../Header_Files/SpawnManager.h"

#define MAX_NUMBER_OF_BULLETS 300
#define BULLET_SPEED 0.08f
/***************************************************************************************
									Local Variables
***************************************************************************************/
Bullet g_bulletList[MAX_NUMBER_OF_BULLETS];

/***************************************************************************************
									   Functions
***************************************************************************************/

void InitialiseBullets(char _rep)
{
	for (int i = 0; i < MAX_NUMBER_OF_BULLETS; ++i)
	{
		// pointer that points to memory address of i element in array to modify it
		Bullet *bullet = g_bulletList + i;

		bullet->pos = SetVector(0, 0);
		bullet->maxDisplacedPos = SetVector(0, 0);
		bullet->dir = SetVector(0, 0);
		bullet->directionInEnum = DEFAULT;

		bullet->isActive = 0;
		bullet->charRepresentation = _rep;
		bullet->speed = BULLET_SPEED;
		bullet->euler = 0.0f;
		bullet->deltaTime = 0.0f;
		bullet->color = BACKGROUND_WHITE_ | FOREGROUND_WHITE_;
	}
}

Vector2 DetermineShootingDirection(Direction _direction)
{
	Vector2 direction = SetVector(0, 0);

	switch (_direction)
	{
	case UP:
		direction = SetVector(0, -1);
		break;
	case DOWN:
		direction = SetVector(0, 1);
		break;
	case LEFT:
		direction = SetVector(-1, 0);
		break;
	case RIGHT:
		direction = SetVector(1, 0);
		break;
	case TOPLEFT:
		direction = SetVector(-1, -1);
		break;
	case TOPRIGHT:
		direction = SetVector(1, -1);
		break;
	case BOTTOMLEFT:
		direction = SetVector(-1, 1);
		break;
	case BOTTOMRIGHT:
		direction = SetVector(1, 1);
		break;
	case DEFAULT:
		direction = SetVector(0, 0);
		break;
	}

	return direction;
}

int HasReachedMaxDisplacement(Point _bulletPosition, Point _displacedPos)
{
	if (_bulletPosition.x != _displacedPos.x
		|| _bulletPosition.y != _displacedPos.y)
	{
		return 0;
	}

	return 1;
}

void ShootingBullets(Point _startShootingPos, Direction _shotDir, int _maxDisplacement, int _teamColor, 
				     int _color, float _timeInterval, float*_shotTime)
{
	if (CheckTimerReached(_shotTime, _timeInterval))
	{
		// At every timeInterval, set a bullet to be active
		for (int i = 0; i < MAX_NUMBER_OF_BULLETS; ++i)
		{
			Bullet *bullet = g_bulletList + i;

			if (bullet->isActive)
				continue;

			bullet->isActive = 1;
			bullet->dir = DetermineShootingDirection(_shotDir);
			bullet->directionInEnum = _shotDir;
			bullet->pos = _startShootingPos;
			bullet->teamColor = _teamColor;
			bullet->color = _color;
			bullet->maxDisplacedPos = SetVector(bullet->pos.x + bullet->dir.x * _maxDisplacement,
				bullet->pos.y + bullet->dir.y * _maxDisplacement);
			break;
		}
	}
}

int CheckCollisionWithBullets(Bullet *_bullet, Point _targetPos)
{
	if (CheckCollisionAhead(_bullet->pos, _bullet->directionInEnum, _targetPos))
	{
		// Check if bullet hit AIs
		for (int i = 0; i < MAXNUMBEROFAI; ++i)
		{
			if (!AI[i].b_isActive)
				continue;
			if (AI[i].bIsDead || AI[i].bIsInvulnerable)
				continue;

			if (CheckPointToBoxCollider(_targetPos, AI[i].collider))
			{
				// Check if AI is on same team as the user that shot the bullet
				if (AI[i].teamColor == _bullet->teamColor)
					return 0;

				// Not same team, then AI will be "killed"
				AI[i].bIsDead = 1;

				if (AI[i].teamColor == teamA.color)
					teamB.numberOfKills++;
				else
					teamA.numberOfKills++;

				ResetBullets(_bullet);
				return 1;
			}
		}

		// Check if bullet has collided with any other players
		if (CheckPointToBoxCollider(_targetPos, player1.collider))
		{
			if (player1.teamColor == _bullet->teamColor || player1.bIsDead || player1.bIsInvulnerable)
				return 0;

			// Not same team, "kill" player
			player1.bIsDead = 1;

			if (player1.teamColor == teamA.color)
				teamB.numberOfKills++;
			else
				teamA.numberOfKills++;

			ResetBullets(_bullet);
			return 1;
		}
		else if (CheckPointToBoxCollider(_targetPos, player2.collider))
		{
			if (player2.teamColor == _bullet->teamColor || player2.bIsDead || player2.bIsInvulnerable)
				return 0;

			// Not same team, "kill" player
			player2.bIsDead = 1;

			if (player2.teamColor == teamA.color)
				teamB.numberOfKills++;
			else
				teamA.numberOfKills++;

			ResetBullets(_bullet);
			return 1;
		}
		else if (CheckPointToBoxCollider(_targetPos, player3.collider))
		{
			if (player3.teamColor == _bullet->teamColor || player3.bIsDead || player3.bIsInvulnerable)
				return 0;

			// Not same team, "kill" player
			player3.bIsDead = 1;

			if (player3.teamColor == teamA.color)
				teamB.numberOfKills++;
			else
				teamA.numberOfKills++;

			ResetBullets(_bullet);
			return 1;
		}
		else if (CheckPointToBoxCollider(_targetPos, player4.collider))
		{
			if (player4.teamColor == _bullet->teamColor || player4.bIsDead || player4.bIsInvulnerable)
				return 0;

			// Not same team, "kill" player
			player4.bIsDead = 1;

			if (player4.teamColor == teamA.color)
				teamB.numberOfKills++;
			else
				teamA.numberOfKills++;

			ResetBullets(_bullet);
			return 1;
		}
	}

	return 0;
}

void SplatterShot(Bullet *_bullet)
{
	int _up, _down, _left, _right;

	if (_bullet->directionInEnum == LEFT || _bullet->directionInEnum == RIGHT)
	{
		if (_up = Random_Range(0, SHOT_RANGE_SPREAD))
		{
			if (map[_bullet->pos.x][_bullet->pos.y - _up].c != 'W')
			{
				map[_bullet->pos.x][_bullet->pos.y - _up].color = _bullet->color;
			}
		}
		if (_down = Random_Range(0, SHOT_RANGE_SPREAD))
		{
			if (map[_bullet->pos.x][_bullet->pos.y + _down].c != 'W')
			{
				map[_bullet->pos.x][_bullet->pos.y + _down].color = _bullet->color;
			}
		}
	}
	else if (_bullet->directionInEnum == UP || _bullet->directionInEnum == DOWN)
	{
		if (_left = Random_Range(0, SHOT_RANGE_SPREAD))
		{
			if (map[_bullet->pos.x - _left][_bullet->pos.y].c != 'W')
			{
				map[_bullet->pos.x - _left][_bullet->pos.y].color = _bullet->color;
			}
		}
		if (_right = Random_Range(0, SHOT_RANGE_SPREAD))
		{
			if (map[_bullet->pos.x + _right][_bullet->pos.y].c != 'W')
			{
				map[_bullet->pos.x + _right][_bullet->pos.y].color = _bullet->color;
			}
		}
	}
}

void UpdateBulletsMovement()
{
	for (int i = 0; i < MAX_NUMBER_OF_BULLETS; ++i)
	{
		Bullet *bullet = g_bulletList + i;

		if (!bullet->isActive)
			continue;

		int posX = 0;
		int posY = 0;

		// Apply movement update to each bullet that is spawned in scene
		if (!HasReachedMaxDisplacement(bullet->pos, bullet->maxDisplacedPos))
		{
			if (CheckEuler(&bullet->euler, bullet->speed))
			{
				posX = bullet->pos.x + bullet->dir.x;
				posY = bullet->pos.y + bullet->dir.y;

				if (CheckIfAtBoundary(posX, posY))
				{
					ResetBullets(bullet);
					continue;
				}

				// Check if there is any collision
				// If there is no collision, move the bullet to new position
				if (!CheckCollisionWithBullets(bullet, map[posX][posY].position))
				{
					bullet->pos = SetVector(posX, posY);

					if (map[posX][posY].c != 'W')
					{
						if (Random_Range(0, 1))
						{
							// Check again for if bullet goes out of bounds
							if (CheckIfAtBoundary(bullet->pos.x, bullet->pos.y))
							{
								ResetBullets(bullet);
								continue;
							}
							map[bullet->pos.x][bullet->pos.y].color = bullet->color;
						}
						SplatterShot(bullet);
					}
				}
			}
			continue;
		}

		// When max displacement is reached, "unrender" bullet by moving it out of screen
		ResetBullets(bullet);
	}
}

void RenderBullets()
{
	for (int i = 0; i < MAX_NUMBER_OF_BULLETS; ++i)
	{
		Bullet *bullet = g_bulletList + i;

		if (!bullet->isActive)
			continue;

		if (bullet->teamColor == teamA.color)
		{
			Console_SetRenderBuffer_Colour_Char(bullet->pos.x,
				bullet->pos.y,
				bullet->charRepresentation,
				BACKGROUND_WHITE_ | FOREGROUND_ORANGE);
		}
		else
		{
			Console_SetRenderBuffer_Colour_Char(bullet->pos.x,
				bullet->pos.y,
				bullet->charRepresentation,
				BACKGROUND_WHITE_ | FOREGROUND_TEAL);
		}
	}
}

void ResetBullets(Bullet *_bullet)
{
	_bullet->deltaTime = 0.0f;
	_bullet->isActive = 0;
	_bullet->pos.x = 0;
	_bullet->pos.y = 0;
	_bullet->color = BACKGROUND_WHITE_ | FOREGROUND_WHITE_;
}
