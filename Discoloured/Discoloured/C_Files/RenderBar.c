#include "../Header_Files/RenderBar.h"
#include "../Console/Console.h"

/***************************************************************************************
									Local Variables
***************************************************************************************/

/***************************************************************************************
									   Functions
***************************************************************************************/

void InitialiseRenderBar(RenderBar* _renderBar, int minPointX, int minPointY, int maxPointX, int maxPointY, int color)
{
	_renderBar->minPoint.x = minPointX;
	_renderBar->minPoint.y = minPointY;

	_renderBar->currPoint.x = minPointX;
	_renderBar->currPoint.y = minPointY;

	_renderBar->maxPoint.x = maxPointX;
	_renderBar->maxPoint.y = maxPointY;

	_renderBar->color = color;

	_renderBar->toIncrease = 0;
}

void Updating_RenderBar(RenderBar *_renderBar, const Score _score, const float _maxPercentage)
{
	// to read each value only once, as the values are in floats
	if (_renderBar->toIncrease == (int)_score.percentageCovered)
		return;

	_renderBar->toIncrease = (int)_score.percentageCovered;

	float barLevel = (_renderBar->minPoint.y - _renderBar->maxPoint.y) * (_maxPercentage / 100.0f);
	float percentageToIncrease = _maxPercentage / barLevel;

	// skip the 0 values as it will always be true
	if (_score.percentageCovered < 1.0f)
		return;

	if ((int)_score.percentageCovered % (int)percentageToIncrease == 0)
	{
		_renderBar->currPoint.y--;
	}
}

void Rendering_RenderBar(RenderBar _renderBar)
{
	for (int i = _renderBar.currPoint.y; i <= _renderBar.minPoint.y; ++i)
	{
		for (int j = _renderBar.minPoint.x; j <= _renderBar.maxPoint.x; ++j)
		{
			Console_SetRenderBuffer_Colour_Char(j, i, ' ', _renderBar.color);
		}
	}
}