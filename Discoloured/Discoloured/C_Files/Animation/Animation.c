#include "../../Header_Files/Animation/Animations.h"
#include "../../Libraries/GameTime.h"
#include "../../Clock/Clock.h"

/***************************************************************************************
									Local Variables
***************************************************************************************/
const float g_blinkingInterval = 0.05f;

/***************************************************************************************
									   Functions
***************************************************************************************/
void BlinkingAnimation(int* _activeness, float* _elapsedTime)
{
	// Creates a "blinking" effect by setting render to render/ unrender accordingly
	if (CheckTimerReached(_elapsedTime, ConvertFromSeconds(g_blinkingInterval)))
	{
		*_activeness = !*_activeness;
		*_elapsedTime = 0.0f;
	}
}
