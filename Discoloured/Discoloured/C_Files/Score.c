#include "../Header_Files/Score.h"
#include "../Header_Files/Globals.h"

/***************************************************************************************
									Local Variables
***************************************************************************************/
Team g_currentWinner;

/***************************************************************************************
									   Functions
***************************************************************************************/

void InitialiseScore(Score* _scoreToInit, Team _team, float _possibleArea)
{
	_scoreToInit->team = _team;
	_scoreToInit->pixelsPossibleToCover = _possibleArea;
	_scoreToInit->pixelsCovered = 0;
	_scoreToInit->percentageCovered = 0.0f;
}

void CalculateColorPercentage(Score* _score, int _mapRow, int _mapCol)
{
	// Checks if the map has the color or shotcolor of the team that the score object is calculating for
	if (_score->team.color == map[_mapRow][_mapCol].color || _score->team.shotColor == map[_mapRow][_mapCol].color)
	{
		++(_score->pixelsCovered);
	}

	/* Calculates % of map that is covered by color as of current frame */
	_score->percentageCovered = _score->pixelsCovered / _score->pixelsPossibleToCover * 100.0f;
}

void CalculateTotalColorPercentage(Score* _score, int _mapRows, int _mapCols)
{
	_score->pixelsCovered = 0;
	for (int i = 0; i < _mapRows; ++i)
	{
		for (int j = 0; j < _mapCols; ++j)
		{
			if (_score->team.color == map[i][j].color || _score->team.shotColor == map[i][j].color)
			{
				++(_score->pixelsCovered);
			}
		}
	}

	/* Returns % of map that is covered by color to be search */
	_score->percentageCovered = _score->pixelsCovered / _score->pixelsPossibleToCover * 100.0f;
}

void ResetScore(Score* _score)
{
	_score->percentageCovered = 0.0f;
}


void SetCurrentWinner(Score _winningScore)
{
	g_currentWinner = _winningScore.team;
}

Team GetCurrentWinner()
{
	return g_currentWinner;
}